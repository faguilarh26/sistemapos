<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(3);
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox ibox-default">
            <div class="ibox-head">
                <div class="ibox-title">MANTENIMIENTO PAQUETES PREMIUM</div>
                <div class="ibox-tools">
                    <?php if ($_SESSION['permisosMod']['w']) { ?>
                        <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
                    <?php } ?>
                </div>
            </div>
            <div class="ibox-body">
                <table id="tabla_paquete_premium" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Paquete</th>
                            <th>Precio</th>
                            <th>Fecha Registro</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Paquete</th>
                            <th>Precio</th>
                            <th>Fecha Registro</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro paquete premium</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <label class="col-lg-12" for="inputName">PAQUETE PREMIUM</label>
                    <div class="col-lg-12">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" class="form-control" id="txt_paquete_premium" />
                        </div>
                    </div>

                </div>
                <div class="form-group row">
                    <label class="col-12" for="inputName">PRECIO</label>
                    <div class="col-12">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" class="form-control" id="txt_precio"  />
                        </div>
                    </div>

                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Registrar_Premium()" style="background: #854c35;border: none;">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar paquetes premium</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group row">
                    <input type="text" id="txt_idpremium" hidden>
                    <label class="col-lg-12" for="inputName">PAQUETE PREMIUM</label>
                    <div class="col-lg-12">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" class="form-control" id="txt_premium_actual" hidden />
                            <input type="text" class="form-control" id="txt_premium_nuevo" />
                        </div>
                    </div>

                </div>
                <div class="form-group row">
                    <label class="col-12" for="inputName">PRECIO</label>
                    <div class="col-12">
                        <div class="input-group" style="width: 100%;">
                            <input type="text" class="form-control" id="txt_precio_premium_editar"  />
                        </div>
                    </div>

                </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Editar_Paquete_Premium()" style="background: #854c35;border: none;">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../js/console_plan.js?rev=<?php echo time(); ?>"></script>
<script>
    $(document).ready(function() {
        // $('#tabla_rol').DataTable();
        $('.js-example-basic-single').select2();
        listar_premium();



    });
    // $('#modal_registro').on('shown.bs.modal', function() {
    //     $('#txt_producto').trigger('focus')
    // })
</script>