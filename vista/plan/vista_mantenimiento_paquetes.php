<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(3);
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox ibox-default">
            <div class="ibox-head">
                <div class="ibox-title">MANTENIMIENTO DE PAQUETES</div>
                <div class="ibox-tools">
                    <?php if ($_SESSION['permisosMod']['w']) { ?>
                        <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
                    <?php } ?>
                </div>
            </div>
            <div class="ibox-body">
                <div class="form-group row">
                    <?php
                    date_default_timezone_set('America/Lima');
                    $fecha_actual = date("Y-m-d");
                    $hora_actual = date("H:i:s");
                    ?>


                    <div class="col-lg-3 ">
                        <label class="col-lg-12" for="inputName">PRODUCTO</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cmb_producto_listar">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3 ">
                        <label class="col-lg-12" for="inputName">PLAY</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_play_listar">

                                </select>
                            </div>

                        </div>
                    </div>

                    <div class="col-lg-3 ">
                        <label class="col-lg-12" for="inputName">PLAN</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_plan_listar">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-3">
                        <label for=""><br></label>
                        <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                            <button class="btn btn-danger" onclick="listar_paquetes()">Buscar</button>
                        </div>


                    </div>
                    <div class="col-lg-12">
                        <strong>
                            <hr>
                        </strong>
                    </div>
                </div>
                <table id="tabla_plan_paquetes" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Play</th>
                            <th>Plan</th>
                            <th>Paquete</th>
                            <th>Precio</th>

                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Producto</th>
                            <th>Play</th>
                            <th>Plan</th>
                            <th>Paquete</th>
                            <th>Precio</th>

                            <th>Acci&oacute;n</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog " role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de paquete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 ">
                        <label class="col-lg-12" for="inputName">PRODUCTO</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cmb_producto">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <label class="col-lg-12" for="inputName">PLAY</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_play">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <label class="col-lg-12" for="inputName">PLAN</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_plan">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <label class="col-lg-12" for="inputName">PAQUETE</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <input type="text" class="form-control" id="txt_paquete" />
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <label class="col-lg-12" for="inputName">PRECIO</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <input type="text" class="form-control" id="txt_precio" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Registrar_Paquete()" style="background: #854c35;border: none;">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar paquete</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-lg-12 ">
                        <input type="text" id="txtidpaquete" hidden>
                        <label class="col-lg-12" for="inputName">PRODUCTO</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cmb_producto_editar">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <label class="col-lg-12" for="inputName">PLAY</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_play_editar">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12 ">
                        <label class="col-lg-12" for="inputName">PLAN</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <select class="form-control inp" style="width: 100%;" id="cbm_plan_editar">

                                </select>
                            </div>

                        </div>
                    </div>
                    <div class="col-lg-12">
                        <label class="col-lg-12" for="inputName">PAQUETE</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <input type="text" class="form-control" id="txt_paquete_actual_editar" hidden/>
                                <input type="text" class="form-control" id="txt_paquete_nuevo_editar" />
                            </div>
                        </div>

                    </div>
                    <div class="col-lg-12">
                        <label class="col-lg-12" for="inputName">PRECIO</label>
                        <div class="col-lg-12">
                            <div class="input-group" style="width: 100%;">
                                <input type="text" class="form-control" id="txt_precio_editar" />
                            </div>
                        </div>

                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Editar_Paquetes()" style="background: #854c35;border: none;">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../js/console_plan.js?rev=<?php echo time(); ?>"></script>
<script>
    $(document).ready(function() {
        // $('#tabla_rol').DataTable();
        $('.js-example-basic-single').select2();
        // listar_plan_internet();
        listar_producto();
        listar_play_combo();
        
        
        
        listar_plan_internet_combo();
        // listar_paquetes();
        



    });
    // $('#modal_registro').on('shown.bs.modal', function() {
    //     $('#txt_producto').trigger('focus')
    // })
</script>