<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(3);
?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox ibox-default">
            <div class="ibox-head">
                <div class="ibox-title">MANTENIMIENTO PLAY</div>
                <div class="ibox-tools">
                    <?php if ($_SESSION['permisosMod']['w']) { ?>
                        <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
                    <?php } ?>
                </div>
            </div>
            <div class="ibox-body">
                <table id="tabla_play" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Play</th>
                            <th>Fecha Registro</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Play</th>
                            <th>Fecha Registro</th>
                            <th>Estatus</th>
                            <th>Acci&oacute;n</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Registro de play</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <label for="">Play</label>
                <input type="text" class="form-control" id="txt_play">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Registrar_Play()" style="background: #854c35;border: none;">Guardar</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar play</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="col-lg-12">
                    <input type="text" id="txtidplay" hidden>
                    <label for="">Play</label>
                    <input type="text" id="txt_play_actual_editar" hidden>
                    <input type="text" class="form-control" id="txt_play_nuevo_editar">
                </div>
                <div class="col-lg-12">
                    <label for="">Estatus</label>
                    <select class="js-example-basic-single" style="width: 100%;" id="cbm_estatus">
                        <option value="ACTIVO">ACTIVO</option>
                        <option value="INACTIVO">INACTIVO</option>
                    </select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary" onclick="Editar_Play()" style="background: #854c35;border: none;">Actualizar</button>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" src="../../js/console_plan.js?rev=<?php echo time(); ?>"></script>
<script>
    $(document).ready(function() {
        // $('#tabla_rol').DataTable();
        $('.js-example-basic-single').select2();
        listar_play();

       

    });
    // $('#modal_registro').on('shown.bs.modal', function() {
    //     $('#txt_producto').trigger('focus')
    // })
</script>