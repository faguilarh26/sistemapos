<?php
session_start();
if (!isset($_SESSION['S_IDUSUARIO'])) {
    header('Location: login.php');
}

require '../modelo/modelo_conexion_pdo.php';

getPermisosModuloGeneral();

$idusuario = $_SESSION['S_IDUSUARIO']

?>



<!DOCTYPE html>
<html lang="es">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width initial-scale=1.0">
    <title>FG TELECOMUNICACIONES</title>
    <!-- GLOBAL MAINLY STYLES-->
    <link href="plantilla/assets/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet" />
    <link href="plantilla/assets/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet" />
    <link href="plantilla/assets/vendors/themify-icons/css/themify-icons.css" rel="stylesheet" />
    <!-- PLUGINS STYLES-->
    <link href="plantilla/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.css" rel="stylesheet" />
    <!-- THEME STYLES-->
    <link href="plantilla/assets/css/main.min.css" rel="stylesheet" />
    <link href="plantilla/assets/css/estilos.css" rel="stylesheet" />

    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/dt/dt-1.11.3/b-2.0.1/b-html5-2.0.1/r-2.2.9/sl-1.3.3/datatables.min.css" />
    <link href="plantilla/assets/vendors/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="plantilla/assets/vendors/select2/dist/css/select2.css" rel="stylesheet" />
    <link href="https://gitcdn.github.io/bootstrap-toggle/2.2.2/css/bootstrap-toggle.min.css" rel="stylesheet">
    <!-- <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" /> -->



    <!-- PAGE LEVEL STYLES-->
</head>

<body class="fixed-navbar">
    <div class="page-wrapper">
        <!-- START HEADER-->
        <header class="header">
            <div class="page-brand">
                <a class="link" href="index.php" id="titulologo">
                    <img class="logo-index" src="img/logo2.png" width="37" height="45" />
                    <span class="brand">
                        <span class="brand-tip">TELECOMUNICACIONES</span>
                    </span>

                </a>
            </div>
            <div class="flexbox flex-1">
                <!-- START TOP-LEFT TOOLBAR-->
                <input type="text" class="form-control" id="txt_idusuario_general" value="<?= $idusuario ?>" hidden />
                <ul class="nav navbar-toolbar">
                    <li>
                        <a class="nav-link sidebar-toggler js-sidebar-toggler"><i class="ti-menu"></i></a>
                    </li>
                    <!-- <li>
                        <form class="navbar-search" action="javascript:;">
                            <div class="rel">
                                <span class="search-icon"><i class="ti-search"></i></span>
                                <input class="form-control" placeholder="Search here...">
                            </div>
                        </form>
                    </li> -->
                </ul>
                <!-- END TOP-LEFT TOOLBAR-->
                <!-- START TOP-RIGHT TOOLBAR-->
                <ul class="nav navbar-toolbar">
                    <!-- <li class="dropdown dropdown-inbox">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-envelope-o"></i>
                            <span class="badge badge-primary envelope-badge">9</span>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                            <li class="dropdown-menu-header">
                                <div>
                                    <span><strong>9 New</strong> Messages</span>
                                    <a class="pull-right" href="mailbox.html">view all</a>
                                </div>
                            </li>
                            <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                                <div>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <img src="plantilla/assets/img/users/u1.jpg" />
                                            </div>
                                            <div class="media-body">
                                                <div class="font-strong"> </div>Jeanne Gonzalez<small class="text-muted float-right">Just now</small>
                                                <div class="font-13">Your proposal interested me.</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <img src="plantilla/assets/img/users/u2.jpg" />
                                            </div>
                                            <div class="media-body">
                                                <div class="font-strong"></div>Becky Brooks<small class="text-muted float-right">18 mins</small>
                                                <div class="font-13">Lorem Ipsum is simply.</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <img src="plantilla/assets/img/users/u3.jpg" />
                                            </div>
                                            <div class="media-body">
                                                <div class="font-strong"></div>Frank Cruz<small class="text-muted float-right">18 mins</small>
                                                <div class="font-13">Lorem Ipsum is simply.</div>
                                            </div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <img src="plantilla/assets/img/users/u4.jpg" />
                                            </div>
                                            <div class="media-body">
                                                <div class="font-strong"></div>Rose Pearson<small class="text-muted float-right">3 hrs</small>
                                                <div class="font-13">Lorem Ipsum is simply.</div>
                                            </div>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li>
                    <li class="dropdown dropdown-notification">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown"><i class="fa fa-bell-o rel"><span class="notify-signal"></span></i></a>
                        <ul class="dropdown-menu dropdown-menu-right dropdown-menu-media">
                            <li class="dropdown-menu-header">
                                <div>
                                    <span><strong>5 New</strong> Notifications</span>
                                    <a class="pull-right" href="javascript:;">view all</a>
                                </div>
                            </li>
                            <li class="list-group list-group-divider scroller" data-height="240px" data-color="#71808f">
                                <div>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-success badge-big"><i class="fa fa-check"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">4 task compiled</div><small class="text-muted">22 mins</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-default badge-big"><i class="fa fa-shopping-basket"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">You have 12 new orders</div><small class="text-muted">40 mins</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-danger badge-big"><i class="fa fa-bolt"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">Server #7 rebooted</div><small class="text-muted">2 hrs</small></div>
                                        </div>
                                    </a>
                                    <a class="list-group-item">
                                        <div class="media">
                                            <div class="media-img">
                                                <span class="badge badge-success badge-big"><i class="fa fa-user"></i></span>
                                            </div>
                                            <div class="media-body">
                                                <div class="font-13">New user registered</div><small class="text-muted">2 hrs</small></div>
                                        </div>
                                    </a>
                                </div>
                            </li>
                        </ul>
                    </li> -->
                    <li class="dropdown dropdown-user">
                        <a class="nav-link dropdown-toggle link" data-toggle="dropdown">
                            <img src="plantilla/assets/img/admin-avatar.png" />
                            <span></span><?php
                                            echo $_SESSION['S_USER']; ?><i class="fa fa-angle-down m-l-5"></i></a>
                        <ul class="dropdown-menu dropdown-menu-right">
                            <a class="dropdown-item" onclick="cargar_contenido('contenido_principal','usuario/vista_profile_usuario.php')"> <i class="fa fa-user"></i>Profile</a>
                            <a class="dropdown-item" href="profile.html"><i class="fa fa-cog"></i>Settings</a>
                            <a class="dropdown-item" href="javascript:;"><i class="fa fa-support"></i>Support</a>
                            <li class="dropdown-divider"></li>
                            <a class="dropdown-item" href="../controlador/usuario/controlador_cerrar_sesion.php"><i class="fa fa-power-off"></i>Logout</a>
                        </ul>
                    </li>
                </ul>
                <!-- END TOP-RIGHT TOOLBAR-->
            </div>
        </header>
        <!-- END HEADER-->
        <!-- START SIDEBAR-->
        <nav class="page-sidebar" id="sidebar">
            <div id="sidebar-collapse">
                <div class="admin-block d-flex">
                    <div>
                        <img src="plantilla/assets/img/admin-avatar.png" width="45px" />
                    </div>
                    <div class="admin-info">
                        <div class="font-strong"><?php
                                                    echo $_SESSION['S_PERSONA']; ?></div><small><?php
                                                        echo $_SESSION['S_ROL']; ?></small>
                    </div>
                </div>
                <ul class="side-menu metismenu">
                    <li>
                        <a class="active" onclick="cargar_contenido('contenido_principal','dashboard/vista_dashboard.php')"><i class="sidebar-item-icon fa fa-th-large"></i>
                            <span class="nav-label">Inicio</span>
                        </a>
                    </li>




                    <li class="heading">MENU DE OPCIONES</li>

                    <!-- seccion de roles -->

                    <?php if (!empty($_SESSION['permisos'][1]['r'])) { ?>
                        <li>
                            <a href="javascript:cargar_contenido('contenido_principal','rol/vista_mantenimiento_rol.php')"><i class="sidebar-item-icon fa fa-tasks"></i>
                                <span class="nav-label">Rol </span><i class="fa fa-angle-left arrow"></i></a>
                            <!-- <ul class="nav-2-level collapse">
                            <li>
                                <a href="mailbox.html">Inbox</a>
                            </li>
                            <li>
                                <a href="mail_view.html">Mail view</a>
                            </li>
                            <li>
                                <a href="mail_compose.html">Compose mail</a>
                            </li>
                        </ul> -->
                        </li>
                    <?php } ?>

                    <!-- seccion de trabajadores -->
                    <?php if (!empty($_SESSION['permisos'][2]['r'])) { ?>
                        <li>
                            <a href="javascript:cargar_contenido('contenido_principal','persona/vista_mantenimiento_persona.php')"><i class="sidebar-item-icon fa fa-id-badge"></i>
                                <span class="nav-label">Trabajadores</span><i class="fa fa-angle-left arrow"></i></a>

                        </li>
                    <?php } ?>
                    <!-- seccion de productos -->
                    <?php if (!empty($_SESSION['permisos'][3]['r'])) { ?>
                        <li>
                            <a href="javascript:cargar_contenido('contenido_principal','producto/vista_mantenimiento_producto.php')"><i class="sidebar-item-icon fa fa-id-badge"></i>
                                <span class="nav-label">Productos</span><i class="fa fa-angle-left arrow"></i></a>

                        </li>
                    <?php } ?>
                    <!-- seccion de plan -->


                    <!-- seccion de usuarios -->
                    <?php if (!empty($_SESSION['permisos'][4]['r'])) { ?>
                        <li>
                            <a href="javascript:cargar_contenido('contenido_principal','usuario/vista_mantenimiento_usuario.php')"><i class="sidebar-item-icon fa fa-id-badge"></i>
                                <span class="nav-label">Usuarios</span><i class="fa fa-angle-left arrow"></i></a>

                        </li>
                    <?php } ?>
                    <!-- seccion de clientes -->
                    <?php if (!empty($_SESSION['permisos'][5]['r'])) { ?>
                        <!-- <li> 
                        <a href="javascript:cargar_contenido('contenido_principal','cliente/vista_mantenimiento_cliente.php')"><i class="sidebar-item-icon fa fa-user"></i>
                            <span class="nav-label">Clientes</span><i class="fa fa-angle-left arrow"></i></a>
                        
                    </li> -->
                        <li>
                            <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
                                <span class="nav-label">Plan</span><i class="fa fa-angle-left arrow"></i></a>
                            <ul class="nav-2-level collapse">
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_play.php')">Play</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_paquetes.php')">Paquetes</a>
                                </li>
                                <!-- <li>
                                <a href="javascript:cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta_rechazada.php')">Plan cable</a>
                            </li> -->
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_plan_internet.php')">Plan internet</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta_rechazada.php')">Tipo de venta</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_decoadicional1.php')">Decos adicionales 1</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_decoadicional2.php')">Decos adicionales 2</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','plan/vista_mantenimiento_premium.php')">Paquetes Premium</a>
                                </li>
                                <!-- <li>
                                <a href="mail_compose.html">Compose mail</a>
                            </li> -->
                            </ul>

                        </li>
                    <?php } ?>
                    <!-- seccion de la preventa -->
                    <?php if (!empty($_SESSION['permisos'][6]['r'])) { ?>
                        <li>
                            <a href="javascript:;"><i class="sidebar-item-icon fa fa-shopping-cart"></i>
                                <span class="nav-label">Ventas</span><i class="fa fa-angle-left arrow"></i></a>
                            <ul class="nav-2-level collapse">
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Venta</a>
                                </li>
                                <li>
                                    <a href="javascript:cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta_rechazada.php')">Ventas Rechazadas</a>
                                </li>
                                <!-- <li>
                                <a href="mail_compose.html">Compose mail</a>
                            </li> -->
                            </ul>

                        </li>
                    <?php } ?>
                    <!-- seccion de los reportes -->





                </ul>
            </div>
        </nav>
        <!-- END SIDEBAR-->
        <div class="content-wrapper">
            <!-- START PAGE CONTENT-->
            <div class="page-content fade-in-up">
                <div id="contenido_principal">
                    <div class="row">
                        <div class="col-lg-3 col-md-6">
                            <div class="ibox bg-success color-white widget-stat">
                                <div class="ibox-body">
                                    <h2 class="m-b-5 font-strong">201</h2>
                                    <div class="m-b-5">NEW ORDERS</div><i class="ti-shopping-cart widget-stat-icon"></i>
                                    <div><i class="fa fa-level-up m-r-5"></i><small>25% higher</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="ibox bg-info color-white widget-stat">
                                <div class="ibox-body">
                                    <h2 class="m-b-5 font-strong">1250</h2>
                                    <div class="m-b-5">UNIQUE VIEWS</div><i class="ti-bar-chart widget-stat-icon"></i>
                                    <div><i class="fa fa-level-up m-r-5"></i><small>17% higher</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="ibox bg-warning color-white widget-stat">
                                <div class="ibox-body">
                                    <h2 class="m-b-5 font-strong">$1570</h2>
                                    <div class="m-b-5">TOTAL INCOME</div><i class="fa fa-money widget-stat-icon"></i>
                                    <div><i class="fa fa-level-up m-r-5"></i><small>22% higher</small></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6">
                            <div class="ibox bg-danger color-white widget-stat">
                                <div class="ibox-body">
                                    <h2 class="m-b-5 font-strong">108</h2>
                                    <div class="m-b-5">NEW USERS</div><i class="ti-user widget-stat-icon"></i>
                                    <div><i class="fa fa-level-down m-r-5"></i><small>-12% Lower</small></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <!-- END PAGE CONTENT-->
            <footer class="page-footer">
                <div class="font-13">2021 © <b>FG Telecomunicaciones</b> - All rights reserved.</div>
                <!-- <a class="px-4" href="http://themeforest.net/item/adminca-responsive-bootstrap-4-3-angular-4-admin-dashboard-template/20912589" target="_blank">BUY PREMIUM</a> -->
                <div class="to-top"><i class="fa fa-angle-double-up"></i></div>
            </footer>
        </div>
    </div>

    <!-- BEGIN PAGA BACKDROPS-->
    <div class="sidenav-backdrop backdrop"></div>
    <div class="preloader-backdrop">
        <div class="page-preloader">Loading</div>
    </div>
    <!-- END PAGA BACKDROPS-->
    <!-- CORE PLUGINS-->
    <script src="plantilla/assets/vendors/jquery/dist/jquery.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/popper.js/dist/umd/popper.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/bootstrap/dist/js/bootstrap.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/metisMenu/dist/metisMenu.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
    <!-- PAGE LEVEL PLUGINS-->
    <script src="plantilla/assets/vendors/chart.js/dist/Chart.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/jvectormap/jquery-jvectormap-2.0.3.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/jvectormap/jquery-jvectormap-world-mill-en.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/jvectormap/jquery-jvectormap-us-aea-en.js" type="text/javascript"></script>
    <!-- <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.19.3/jquery.validate.min.js" type="text/javascript"></script> -->
    <!-- CORE SCRIPTS-->
    <script src="plantilla/assets/js/app.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="https://cdn.datatables.net/v/dt/dt-1.11.3/b-2.0.1/b-html5-2.0.1/r-2.2.9/sl-1.3.3/datatables.min.js"></script>
    <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

    <script src="plantilla/assets/vendors/select2/dist/js/select2.min.js" type="text/javascript"></script>
    <script src="plantilla/assets/vendors/select2/dist/js/select2.full.min.js" type="text/javascript"></script>
    <!-- <script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script> -->
    <script src="https://gitcdn.github.io/bootstrap-toggle/2.2.2/js/bootstrap-toggle.min.js"></script>
    <script src="plantilla/assets/js/push.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="../js/console_ubigeo.js"></script>


    <!-- PAGE LEVEL SCRIPTS-->
    <!-- <script src="plantilla/assets/js/scripts/dashboard_1_demo.js" type="text/javascript"></script> -->


    <script>
        function cargar_contenido(contenedor, contenido) {
            $("#" + contenedor).load(contenido);
        }

        var idioma_espanol = {
            select: {
                rows: "%d fila seleccionada"
            },
            "sProcessing": "Procesando...",
            "sLengthMenu": "Mostrar _MENU_ registros",
            "sZeroRecords": "No se encontraron resultados",
            "sEmptyTable": "Ning&uacute;n dato disponible en esta tabla",
            "sInfo": "Registros del (_START_ al _END_) total de _TOTAL_ registros",
            "sInfoEmpty": "Registros del (0 al 0) total de 0 registros",
            "sInfoFiltered": "(filtrado de un total de _MAX_ registros)",
            "sInfoPostFix": "",
            "sSearch": "Buscar:",
            "sUrl": "",
            "sInfoThousands": ",",
            "sLoadingRecords": "<b>No se encontraron datos</b>",
            "oPaginate": {
                "sFirst": "Primero",
                "sLast": "Último",
                "sNext": "Siguiente",
                "sPrevious": "Anterior"
            },
            "oAria": {
                "sSortAscending": ": Activar para ordenar la columna de manera ascendente",
                "sSortDescending": ": Activar para ordenar la columna de manera descendente"
            }
        }


        function sololetras(e) {
            key = e.keyCode || e.which;

            teclado = String.fromCharCode(key).toLowerCase();

            letras = "qwertyuiopasdfghjklñzxcvbnmáéíóú ";

            especiales = "8-37-38-46-164";

            teclado_especial = false;

            for (var i in especiales) {
                if (key == especiales[i]) {
                    teclado_especial = true;
                    break;
                }
            }

            if (letras.indexOf(teclado) == -1 && !teclado_especial) {
                return false;
            }
        }


        function soloNumeros(e) {
            tecla = (document.all) ? e.keyCode : e.which;
            if (tecla == 8) {
                return true;
            }
            // Patron de entrada, en este caso solo acepta numeros
            patron = /[0-9]/;
            tecla_final = String.fromCharCode(tecla);
            return patron.test(tecla_final);
        }

        // setInterval(ver,10000);
    </script>
</body>

</html>