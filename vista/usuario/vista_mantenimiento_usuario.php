<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(4);
?>
<div class="row">
  <div class="col-md-12">
    <div class="ibox ibox-default">
      <div class="ibox-head">
        <div class="ibox-title">MANTENIMIENTO USUARIOS</div>
        <div class="ibox-tools">
          <?php if ($_SESSION['permisosMod']['w']) { ?>
            <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
          <?php } ?>
        </div>
      </div>
      <div class="ibox-body">
        <table id="tabla_usuario" class="display" style="width:100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Usuario</th>
              <th>Trabajador</th>
              <th>Rol</th>
              <th>Email</th>
              <!-- <th style="text-align: center;">Imagen</th> -->
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </thead>
          <tbody>
          </tbody>

          <tfoot>
            <tr>
              <th>#</th>
              <th>Usuario</th>
              <th>Trabajador</th>
              <th>Rol</th>
              <th>Email</th>
              <!-- <th style="text-align: center;">Imagen</th> -->
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Registro de Usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="col-lg-6">

            <label for="">Usuario</label>
            <input type="text" class="form-control" id="txt_usu">

          </div>
          <div class="col-lg-6">
            <label for="">Contrase&ntilde;a</label>
            <input type="password" class="form-control" id="txt_con">
          </div>
          <div class="col-lg-6">
            <label for="">Trabajador</label>
            <select class="js-example-basic-single" name="state" style="width: 100%;" id="cbm_trabajador">

            </select>
          </div>
          <div class="col-lg-6">
            <label for="">Email</label>
            <input type="text" class="form-control" id="txt_email">
          </div>

          <div class="col-lg-6">
            <label for="">Rol</label>
            <select class="js-example-basic-single" style="width: 100%;" id="cbm_rol">

            </select>
          </div>
          <!-- <div class="col-lg-6">
            <label for="">Subir imagen</label>
            <input type="file" class="form-control-file" id="imagen" accept="image/*">
          </div> -->




          <div class="col-lg-12"><br>
            <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;"></div>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Registrar_Usuario()" style="background: #854c35;border: none;">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal editar-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar usuario</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">

          <!--  -->
          <div class="col-lg-6">
            <input type="text" id="txtidusuario" hidden>
            <label for="">Usuario</label>
            <input type="text" class="form-control" id="txt_usu_editar_actual" disabled>

          </div>
          <div class="col-lg-6">
            <label for="">Trabajador</label>
            <select class="js-example-basic-single" style="width: 100%;" id="cbm_trabajador_editar">

            </select>
          </div>
          <div class="col-lg-6">
            <label for="">Email</label>
            <input type="text" class="form-control" id="txt_email_editar_nuevo">
          </div>

          <div class="col-lg-6">
            <label for="">Rol</label>
            <select class="js-example-basic-single" style="width: 100%;" id="cbm_rol_editar">

            </select>
          </div>
          <div class="col-lg-4">
            <label for="">Estatus</label>
            <select class="js-example-basic-single" style="width: 100%;" id="cbm_estatus">
              <option value="ACTIVO">ACTIVO</option>
              <option value="INACTIVO">INACTIVO</option>
            </select>
          </div>
          <!-- <div class="col-lg-6">
            <label for="">Subir imagen</label>
            <input type="file" class="form-control-file" id="imagen_editar" accept="image/*">
          </div> -->




          <div class="col-lg-12"><br>
            <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;"></div>
          </div>
          <!-- <div class="col-lg-12">
            <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;"></div>
          </div> -->

          <div class="col-lg-12"><br>
            <div class="alert alert-danger alert-bordered" id="div_error_editar" style="display: none;"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Editar_Usuario()" style="background: #854c35;border: none;">Actualizar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal permisos -->
<div class="modal fade" id="modal_permisos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">

        <h5 class="modal-title" id="myModalLabel">Asignacion de permisos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>


      </div>

      <!-- Modal Body -->
      <div class="modal-body">
        <div class="col-md-12">
          <div class="tile">
            <form action="" id="formPermisos" name="formPermisos" onsubmit="registrarPermisos(event)">
              <!-- <input type="hidden" id="idrol" name="idrol" value="2" required=""> -->
              <div class="table-responsive">
                <div class="form-group col-lg-12"  >

                  <label for="inputName">Rol Usuario</label>
                  <input type="text" name="idrol" id="txtidrol_permiso" value="">
                </div>
                <table class="table" id="tabla_permisos">
                  <style>
                    table.dataTable.no-footer {
                      border-bottom-color: #fff;
                    }
                  </style>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>IdPermiso</th>
                      <th>Módulo</th>
                      <th>Ver</th>
                      <th>Crear</th>
                      <th>Actualizar</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot hidden></tfoot>
                </table>
              </div>

              <div class="text-center">
                <button class="btn btn-success" type="submit" style="background: #854c35;border: none;"><i class="fa fa-fw fa-lg fa-check-circle" aria-hidden="true"></i> Guardar</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="app-menu__icon fas fa-sign-out-alt" aria-hidden="true"></i> Salir</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Footer -->
      <!-- <div class="modal-footer text-center">
         <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle" aria-hidden="true"></i> Guardar</button>
                <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="app-menu__icon fas fa-sign-out-alt" aria-hidden="true"></i> Salir</button>
              
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript" src="../../js/console_usuario.js?rev=<?php echo time(); ?>"></script>
<script>
  $(document).ready(function() {
    // $('#tabla_persona').DataTable();
    $('.js-example-basic-single').select2();
    listar_usuario();
    Listar_persona_combo();
    Listar_rol_combo();

    function listar_usuario() {
      t_usuario = $("#tabla_usuario").DataTable({
        ordering: true,
        pageLength: 10,
        destroy: true,
        async: false,
        responsive: true,
        autoWidth: false,
        ajax: {
          method: "POST",
          url: "../../controlador/usuario/controlador_usuario_listar.php",
        },
        columns: [{
            defaultContent: ""
          },
          {
            data: "usuario_nombre"
          },
          {
            data: "persona"
          },
          {
            data: "rol_nombre"
          },
          {
            data: "usuario_email"
          },
          // { data: "usuario_imagen",
          // render: function (data, type, row) {
          //   return '<img src="../'+data+'" class="img-circle m-r-10" style="width:28px;">';
          // } },
          {
            data: "usuario_estatus",
            render: function(data, type, row) {
              if (data == "ACTIVO") {
                return (
                  "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" +
                  data +
                  "</span>"
                );
              } else {
                return (
                  "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" +
                  data +
                  "</span>"
                );
              }
            }
          },
          {
            defaultContent: "<button style='font-size:13px;' type='button' class='permisos btn btn-secondary '><i class='fa fa-key'> </i></button>&nbsp<?php if ($_SESSION['permisosMod']['u']) { ?><button class='editar btn btn-primary'><i class='fa fa-edit'></i></button><?php } ?>",
          },
        ],
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          // $($(nRow).find("td")[3]).css("text-align", "center");
          $($(nRow).find("td")[4]).css("text-align", "left");
          // $($(nRow).find("td")[5]).css("text-align","center");
        },
        language: idioma_espanol,
        select: true,
      });
      t_usuario.on("draw.dt", function() {
        var PageInfo = $("#tabla_usuario").DataTable().page.info();
        t_usuario
          .column(0, {
            page: "current"
          })
          .nodes()
          .each(function(cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
          });
      });
    }

    listar_usuario();

  });
  $('#modal_registro').on('shown.bs.modal', function() {
    $('#txt_usu').trigger('focus')
  })

  //   document.getElementById("imagen").addEventListener("change", () => {
  //      var fileName = document.getElementById("imagen").value; 
  //      var idxDot = fileName.lastIndexOf(".") + 1; 
  //      var extFile = fileName.substr(idxDot, fileName.length).toLowerCase(); 
  //      if (extFile=="jpg" || extFile=="jpeg" || extFile=="png"){ 
  //       //TO DO 
  //      }else{ 
  //       Swal.fire("MENSAJE DE ADVERTENCIA","SOLO SE ACEPTAN IMAGENES - USTED SUBIO UN ARCHIVO CON EXTENSION"+extFile,"warning"); 
  //       document.getElementById("imagen").value="";
  //      } 
  // });
</script>