<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(1);
?>
<div class="row">
  <div class="col-md-12">
    <div class="ibox ibox-default">
      <div class="ibox-head">
        <div class="ibox-title">MANTENIMIENTO ROL</div>
        <div class="ibox-tools">
          <?php if ($_SESSION['permisosMod']['w']) { ?>
            <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
          <?php } ?>
        </div>
      </div>
      <div class="ibox-body">
        <table id="tabla_rol" class="display" style="width:100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Rol</th>
              <th>Fecha Registro</th>
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </thead>
          <tbody>
          </tbody>

          <tfoot>
            <tr>
              <th>#</th>
              <th>Rol</th>
              <th>Fecha Registro</th>
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Registro de rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <label for="">Rol</label>
        <input type="text" class="form-control" id="txt_rol">
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Registrar_Rol()" style="background: #854c35;border: none;">Guardar</button>
      </div>
    </div>
  </div>
</div>
<!-- Modal editar-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar rol</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="col-lg-12">
          <input type="text" id="txtidrol" hidden>
          <label for="">Rol</label>
          <input type="text" id="txt_rol_actual_editar" hidden>
          <input type="text" class="form-control" id="txt_rol_nuevo_editar">
        </div>
        <div class="col-lg-12">
          <label for="">Estatus</label>
          <select class="js-example-basic-single" style="width: 100%;" id="cbm_estatus">
            <option value="ACTIVO">ACTIVO</option>
            <option value="INACTIVO">INACTIVO</option>
          </select>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Editar_Rol()" style="background: #854c35;border: none;">Actualizar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal permisos -->
<div class="modal fade" id="modal_permisos" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg">
    <div class="modal-content">
      <!-- Modal Header -->
      <div class="modal-header">

        <h5 class="modal-title" id="myModalLabel">Asignacion de permisos</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>


      </div>

      <!-- Modal Body -->
      <div class="modal-body">
        <div class="col-md-12">
          <div class="tile">
            <form action="" id="formPermisos" name="formPermisos" onsubmit="registrarPermisos(event)">
              <!-- <input type="hidden" id="idrol" name="idrol" value="2" required=""> -->
              <div class="table-responsive">
                <div class="form-group col-lg-12" hidden >

                  <label for="inputName">Rol Usuario</label>
                  <input type="text" name="idrol" id="txtidrol_permiso" value="">
                </div>
                <table class="table" id="tabla_permisos">
                  <style>
                    table.dataTable.no-footer {
                      border-bottom-color: #fff;
                    }
                  </style>
                  <thead>
                    <tr>
                      <th>#</th>
                      <th>IdPermiso</th>
                      <th>Módulo</th>
                      <th>Ver</th>
                      <th>Crear</th>
                      <th>Actualizar</th>
                      <th>Eliminar</th>
                    </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot hidden></tfoot>
                </table>
              </div>

              <div class="text-center">
                <button class="btn btn-success" type="submit" style="background: #854c35;border: none;"><i class="fa fa-fw fa-lg fa-check-circle" aria-hidden="true"></i> Guardar</button>
                <button class="btn btn-secondary" type="button" data-dismiss="modal"><i class="app-menu__icon fas fa-sign-out-alt" aria-hidden="true"></i> Salir</button>
              </div>
            </form>
          </div>
        </div>
      </div>

      <!-- Modal Footer -->
      <!-- <div class="modal-footer text-center">
         <button class="btn btn-success" type="submit"><i class="fa fa-fw fa-lg fa-check-circle" aria-hidden="true"></i> Guardar</button>
                <button class="btn btn-danger" type="button" data-dismiss="modal"><i class="app-menu__icon fas fa-sign-out-alt" aria-hidden="true"></i> Salir</button>
              
      </div> -->
    </div>
  </div>
</div>

<script type="text/javascript" src="../../js/console_rol.js?rev=<?php echo time(); ?>"></script>
<script>
  $(document).ready(function() {
    // $('#tabla_rol').DataTable();
    $('.js-example-basic-single').select2();
    listar_rol();

    function listar_rol() {

      t_rol = $("#tabla_rol").DataTable({
        "ordering": true,
        "pageLength": 10,
        "destroy": true,
        "async": false,
        "responsive": true,
        "autoWidth": false,
        "ajax": {
          "method": "POST",
          "url": "../../controlador/rol/controlador_rol_listar.php",
        },
        "columns": [{
            "defaultContent": ""
          },
          {
            "data": "rol_nombre"
          },
          {
            "data": "rol_feregistro"
          },
          {
            "data": "rol_estatus",
            render: function(data, type, row) {
              if (data == 'ACTIVO') {
                return "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + data + "</span>";
              } else {
                return "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" + data + "</span>";
              }
            }
          },
          {
            "defaultContent": "<?php if($_SESSION['permisosMod']['u']){ ?><button style='font-size:13px;' type='button' class='permisos btn btn-secondary '><i class='fa fa-key'> </i></button>&nbsp<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button><?php } ?>&nbsp<?php if($_SESSION['permisosMod']['d']){ ?><button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button><?php } ?> "
          },

        ],
        "fnRowCallback": function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $($(nRow).find("td")[4]).css('text-align', 'left');
        },
        "language": idioma_espanol,
        select: true
      });
      t_rol.on('draw.dt', function() {
        var PageInfo = $('#tabla_rol').DataTable().page.info();
        t_rol.column(0, {
          page: 'current'
        }).nodes().each(function(cell, i) {
          cell.innerHTML = i + 1 + PageInfo.start;
        });
      });

    }

    listar_rol();

  });
  $('#modal_registro').on('shown.bs.modal', function() {
    $('#txt_rol').trigger('focus')
  })

  // $('#btnObtener').on('click', function() {
  //   let dt = $('#tabla_permisos').DataTable();
  //   let checkeds = dt.data().toArray().filter((data) => data.checked);
  //   console.log(checkeds);
  // });
</script>