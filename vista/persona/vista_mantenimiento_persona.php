<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(2);
?>

<div class="row">
  <div class="col-md-12">
    <div class="ibox ibox-default">
      <div class="ibox-head">
        <div class="ibox-title">MANTENIMIENTO TRABAJADORES</div>
        <div class="ibox-tools">
          <?php if ($_SESSION['permisosMod']['w']) { ?>
            <button class="btn btn-danger" onclick="AbrirModal()">Nuevo Registro</button>
          <?php } ?>
        </div>
      </div>
      <div class="ibox-body">
        <table id="tabla_persona" class="display" style="width:100%">
          <thead>
            <tr>
              <th>#</th>
              <th>Persona</th>
              <th>Nro documento</th>
              <th style="text-align: center;">Tipo documento</th>
              <th style="text-align: center;">Sexo</th>
              <th>Telefono</th>
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </thead>
          <tbody>
          </tbody>

          <tfoot>
            <tr>
              <th>#</th>
              <th>Persona</th>
              <th>Nro documento</th>
              <th style="text-align: center;">Tipo documento</th>
              <th style="text-align: center;">Sexo</th>
              <th>Telefono</th>
              <th>Estatus</th>
              <th>Acci&oacute;n</th>
            </tr>
          </tfoot>
        </table>
      </div>
    </div>
  </div>
</div>

<!-- Modal registro-->
<div class="modal fade" id="modal_registro" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Registro de persona</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-lg-6">
            <label for="">Nombre</label>
            <input type="text" class="form-control" id="txtnombre" onkeypress="return sololetras(event)">

          </div>
          <div class="form-group col-lg-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" id="txtapepat" onkeypress="return sololetras(event)">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" id="txtapemat" onkeypress="return sololetras(event)">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Tipo Documento</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="DNI">DNI</option>
                <option value="RUC">RUC</option>
                <option value="PASAPORTE">CARNET DE EXTRANJERIA</option>
              </select>
            </div>
          </div>
          <div class="form-group col-lg-6">
            <label for="">Nro Documento</label>
            <input type="text" class="form-control" id="txtnro" onkeypress="return soloNumeros(event)">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Sexo</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_sexo">
                <option value="MASCULINO">MASCULINO</option>
                <option value="FEMENINO">FEMENINO</option>
              </select>
            </div>
          </div>
          <?php
          date_default_timezone_set('America/Lima');
          $fecha_actual = date("Y-m-d");
          $hora_actual = date("H:i:s");
          ?>



          <div class="form-group col-lg-6">
            <label for="">Fecha de nacimiento</label>
            <input id="txt_fecnacimiento" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de contratacion</label>
            <input type="date" class="form-control" id="txt_fecontratacion" value="<?= $fecha_actual ?>" min="<?= $fecha_actual ?>" max="<?= $fecha_actual ?>" />
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de ingreso</label>
            <input id="txt_fecingreso" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>

          <!-- <div class="form-group col-lg-6">
            <label for="">Fecha de salida</label>
            <input id="txt_fecsalida" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Motivo Salida</label>
            <input type="text" class="form-control" id="txtmotivosalida" onkeypress="return sololetras(event)">
          </div> -->

          <!-- <div class="form-group col-lg-6">
            <label for="">Fecha de inicio full time</label>
            <input id="txt_fecfulltime" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div> -->
          <div class="form-group col-lg-6">
            <label for="">Nro Telefono</label>
            <input type="text" class="form-control" id="txttelefono" onkeypress="return soloNumeros(event)">
          </div>

          <div class="col-lg-6">
            <label for="">Banco</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_banco">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="INTERBANK">INTERBANK</option>
                <option value="BCP">BCP</option>
                <option value="BANCO DE LA NACION">BANCO DE LA NACION</option>
              </select>
            </div>
          </div>

          <div class="form-group col-lg-6">
            <label for="">Nro Cuenta de ahorros</label>
            <input type="text" class="form-control" id="txtnrocuentaahorro" onkeypress="return soloNumeros(event)">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Codigo interbancario</label>
            <input type="text" class="form-control" id="txtinterbancario" onkeypress="return soloNumeros(event)">
          </div>

          <div class="col-lg-6">
            <label for="">Tipo de pago</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_tipopago">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="RECIBO POR HONORARIOS">RECIBO POR HONORARIOS</option>
              </select>
            </div>
          </div>

          <div class="col-lg-6">
            <label for="">Esquema de pago</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_esquemapago">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="MENSUAL">MENSUAL</option>
              </select>
            </div>
          </div>
          <div class="form-group col-lg-6">
            <label for="">RUC</label>
            <input type="text" class="form-control" id="txtruc" onkeypress="return soloNumeros(event)" maxlength="11">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Clave sol 1</label>
            <input type="text" class="form-control" id="txt_clavesol1">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Clave sol 2</label>
            <input type="text" class="form-control" id="txt_clavesol2">
          </div>

          <div class="col-lg-6">
            <label for="">Email</label>
            <input type="text" class="form-control" id="txt_email">
          </div>

          <div class="col-lg-6">
            <label for="">Rol</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_rol">

              </select>
            </div>
          </div>
          <div id="persresponsable" class="col-lg-6" style="display: none;">
            <label for="">Persona Responsable</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_responsable">

              </select>
            </div>
          </div>
          <div class="col-lg-6">
            <label for="">Zona</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_zona">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="CALL CENTER">CALL CENTER</option>


              </select>
            </div>
          </div>


          <div class="col-lg-12"><br>
            <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;"></div>
          </div>
        </div>

      </div>

      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Registrar_Persona_Prueba()" style="background: #854c35;border: none;">Guardar</button>
      </div>
    </div>
  </div>
</div>

<!-- Modal editar trabajador-->
<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog modal-lg" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar persona</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">


        <div class="row">
          <div class="form-group col-lg-6">
            <input type="text" id="txtidpersona" hidden>
            <input type="text" id="txtidpersona_pago" hidden>
            <input type="text" id="txtidusuario" hidden>
            <label for="">Nombre</label>
            <input type="text" class="form-control" id="txtnombre_editar" onkeypress="return sololetras(event)">

          </div>
          <div class="form-group col-lg-6">
            <label for="">Apellido Paterno</label>
            <input type="text" class="form-control" id="txtapepat_editar" onkeypress="return sololetras(event)">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Apellido Materno</label>
            <input type="text" class="form-control" id="txtapemat_editar" onkeypress="return sololetras(event)">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Tipo Documento</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento_editar">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="DNI">DNI</option>
                <option value="RUC">RUC</option>
                <option value="PASAPORTE">CARNET DE EXTRANJERIA</option>
              </select>
            </div>
          </div>
          <div class="form-group col-lg-6">
            <label for="">Nro Documento</label>
            <input type="text" class="form-control" id="txtnro_editar_actual" onkeypress="return soloNumeros(event)" hidden>
            <input type="text" class="form-control" id="txtnro_editar_nuevo" onkeypress="return soloNumeros(event)">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Sexo</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_sexo_editar">
                <option value="MASCULINO">MASCULINO</option>
                <option value="FEMENINO">FEMENINO</option>
              </select>
            </div>
          </div>




          <div class="form-group col-lg-6">
            <label for="">Fecha de nacimiento</label>
            <input id="txt_fecnacimiento_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de contratacion</label>
            <input type="date" class="form-control" id="txt_fecontratacion_editar" disabled />
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de ingreso</label>
            <input id="txt_fecingreso_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de salida</label>
            <input id="txt_fecsalida_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Motivo Salida</label>
            <input type="text" class="form-control" id="txtmotivosalida_editar" onkeypress="return sololetras(event)">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Fecha de inicio full time</label>
            <input id="txt_fecfulltime_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Nro Telefono</label>
            <input type="text" class="form-control" id="txttelefono_editar" onkeypress="return soloNumeros(event)">
          </div>

          <div class="col-lg-6">
            <label for="">Banco</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_banco_editar">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="INTERBANK">INTERBANK</option>
                <option value="BCP">BCP</option>
                <option value="BANCO DE LA NACION">BANCO DE LA NACION</option>
              </select>
            </div>
          </div>

          <div class="form-group col-lg-6">
            <label for="">Nro Cuenta de ahorros</label>
            <input type="text" class="form-control" id="txtnrocuentaahorro_editar" onkeypress="return soloNumeros(event)">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Codigo interbancario</label>
            <input type="text" class="form-control" id="txtinterbancario_editar" onkeypress="return soloNumeros(event)">
          </div>

          <div class="col-lg-6">
            <label for="">Tipo de pago</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_tipopago_editar">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="RECIBO POR HONORARIOS">RECIBO POR HONORARIOS</option>
              </select>
            </div>
          </div>

          <div class="col-lg-6">
            <label for="">Esquema de pago</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_esquemapago_editar">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="MENSUAL">MENSUAL</option>
              </select>
            </div>
          </div>
          <div class="form-group col-lg-6">
            <label for="">RUC</label>
            <input type="text" class="form-control" id="txtruc_editar" onkeypress="return soloNumeros(event)" maxlength="11">
          </div>

          <div class="form-group col-lg-6">
            <label for="">Clave sol 1</label>
            <input type="text" class="form-control" id="txt_clavesol1_editar_actual" hidden>
            <input type="text" class="form-control" id="txt_clavesol1_editar_nuevo">
          </div>
          <div class="form-group col-lg-6">
            <label for="">Clave sol 2</label>
            <input type="text" class="form-control" id="txt_clavesol2_editar_actual" hidden>
            <input type="text" class="form-control" id="txt_clavesol2_editar_nuevo">
          </div>

          <div class="col-lg-6">
            <label for="">Email</label>
            <input type="text" class="form-control" id="txt_email_editar_actual" hidden>
            <input type="text" class="form-control" id="txt_email_editar_nuevo">
          </div>

          <div class="col-lg-6">
            <label for="">Rol</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_rol_editar">

              </select>
            </div>
          </div>
          <div id="persresponsable" class="col-lg-6" style="display: none;">
            <label for="">Persona Responsable</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_responsable_editar">

              </select>
            </div>
          </div>
          <div class="col-lg-6">
            <label for="">Zona</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_zona_editar">
                <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                <option value="CALL CENTER">CALL CENTER</option>


              </select>
            </div>
          </div>
          <div class="col-lg-6">
            <label for="">Estado Final</label>
            <div class="input-group" style="width: 100%;">
              <select class="form-control inp" style="width: 100%;" id="cbm_estatus">
                <option value="ACTIVO">ACTIVO</option>
                <option value="INACTIVO">INACTIVO</option>
              </select>
            </div>
          </div>


          <div class="col-lg-12"><br>
            <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;"></div>
          </div>
        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="Editar_Persona()" style="background: #854c35;border: none;">Actualizar</button>
      </div>
    </div>
  </div>
</div>


<!-- Modal editar contraseña-->
<div class="modal fade" id="modal_editar_contra" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLongTitle">Editar Contraseña</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <div class="row">
          <div class="form-group col-lg-12">
            <input type="text" id="txtidpersona_contra" hidden>
            <input type="text" id="txtusuarioid" hidden>
            <input type="text" id="txtcontrbd" hidden>
            <label for="">Contrase&ntilde;a actual</label>
            <input type="text" class="form-control" id="txt_contra_actual">

          </div>
          <div class="form-group col-lg-12">
            <label for="">Nueva contrase&ntilde;a</label>
            <input type="text" class="form-control" id="txt_contra_nueva">
          </div>
          <div class="form-group col-lg-12">
            <label for="">Repetircontrase&ntilde;a</label>
            <input type="text" class="form-control" id="txt_repetir_contra">
          </div>


        </div>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
        <button type="button" class="btn btn-primary" onclick="editarContra()" style="background: #854c35;border: none;">Actualizar</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript" src="../../js/console_persona.js?rev=<?php echo time(); ?>"></script>
<script>
  $(document).ready(function() {
    // $('#tabla_persona').DataTable();
    $('.js-example-basic-single').select2();

    listar_persona();
    Listar_rol_combo();
    Listar_personaCargo_combo();

    function listar_persona() {
      t_persona = $("#tabla_persona").DataTable({
        ordering: true,
        pageLength: 10,
        destroy: true,
        async: false,
        responsive: true,
        autoWidth: false,
        ajax: {
          method: "POST",
          url: "../../controlador/persona/controlador_persona_listar.php",
        },
        columns: [{
            defaultContent: ""
          },
          {
            data: "persona"
          },
          {
            data: "persona_nrodocumento"
          },
          {
            data: "persona_tipodocumento"
          },
          {
            data: "persona_sexo",
            render: function(data, type, row) {
              if (data == "MASCULINO") {
                return "<i class='fa fa-male' aria-hidden='true'></i>";
              } else {
                return "<i class='fa fa-female' aria-hidden='true'></i>";
              }
            },
          },
          {
            data: "persona_telefono"
          },
          {
            data: "persona_estatus",
            render: function(data, type, row) {
              if (data == "ACTIVO") {
                return (
                  "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" +
                  data +
                  "</span>"
                );
              } else {
                return (
                  "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" +
                  data +
                  "</span>"
                );
              }
            },
          },
          {
            defaultContent: "<?php if ($_SESSION['permisosMod']['u']) { ?><button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='editar_password btn btn-warning'><i class='fa fa-unlock'></i></button>&nbsp<button class='resetear_password btn btn-danger'><i class='fa fa-unlock-alt'></i></button><?php } ?>&nbsp<?php if ($_SESSION['permisosMod']['d']) { ?><button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button><?php } ?> ",
          },
        ],
        fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
          $($(nRow).find("td")[3]).css("text-align", "center");
          $($(nRow).find("td")[4]).css("text-align", "center");
        },
        language: idioma_espanol,
        select: true,
      });
      t_persona.on("draw.dt", function() {
        var PageInfo = $("#tabla_persona").DataTable().page.info();
        t_persona
          .column(0, {
            page: "current"
          })
          .nodes()
          .each(function(cell, i) {
            cell.innerHTML = i + 1 + PageInfo.start;
          });
      });
    }
    listar_persona();
  });
  $('#modal_registro').on('shown.bs.modal', function() {
    $('#txtnombre').trigger('focus')
  })

  $("#cbm_tdocumento").change(function() {
    var tipdoc = $("#cbm_tdocumento").val();
    if (tipdoc == "DNI") {
      $("#txtnro").attr("maxlength", "8");
      $("#txtnro").attr("onkeypress", "return soloNumeros(event)");


    } else if (tipdoc == "RUC") {
      $("#txtnro").attr("maxlength", "11");
      $("#txtnro").attr("onkeypress", "return soloNumeros(event)");
    } else {
      $("#txtnro").attr("maxlength", "12");
      $("#txtnro").attr("onkeypress", "");
    }
  })
  $("#cbm_tdocumento_editar").change(function() {
    var tipdoc = $("#cbm_tdocumento_editar").val();
    if (tipdoc == "DNI") {
      $("#txtnro_editar_nuevo").attr("maxlength", "8");
      $("#txtnro_editar_nuevo").attr("onkeypress", "return soloNumeros(event)");


    } else if (tipdoc == "RUC") {
      $("#txtnro_editar_nuevo").attr("maxlength", "11");
      $("#txtnro_editar_nuevo").attr("onkeypress", "return soloNumeros(event)");
    } else {
      $("#txtnro_editar_nuevo").attr("maxlength", "12");
      $("#txtnro_editar_nuevo").attr("onkeypress", "");
    }
  })

  $("#cbm_rol").change(function() {
    var tiporol = $('#cbm_rol option:selected').html();
    if (tiporol == "ASESOR COMERCIAL") {
      $("#persresponsable").css("display", "block");
    } else {
      $("#persresponsable").css("display", "none");
      Listar_personaCargo_combo();
    }
  })
</script>