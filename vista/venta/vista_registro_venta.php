<?php
session_start();

?>
<div class="row">
    <div class="col-md-12">
        <div class="ibox ibox-default">
            <div class="ibox-head">
                <div class="ibox-title">NUEVA DE VENTA</div>
                <div class="ibox-tools">
                    <button class="btn btn-danger" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                </div>
            </div>
            <div class="ibox-body">
                <p class="statusMsg"></p>
                <form autocomplete="false" onsubmit="return false" id="form-sample-1">

                    <div class="col-lg-11">
                        <div class="form-group row">
                            <label class="col-3" for="inputName">MOVIL DE LLAMADA</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" name="name" class="form-control " id="txt_nrollamada" onkeypress="return soloNumeros(event)" required />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <?php
                            date_default_timezone_set('America/Lima');
                            $fecha_actual = date("Y-m-d");
                            $hora_actual = date("H:i:s");
                            ?>
                            <label class="col-3" for="inputName">FECHA DE LLAMADA</label>
                            <div class="col-6">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="date" class="form-control" id="txt_fechallamada" value="<?= $fecha_actual ?>" min="<?= $fecha_actual ?>" max="<?= $fecha_actual ?>" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">HORA DE LLAMADA</label>
                            <div class="col-6">
                                <input type="time" class="form-control" id="txt_horallamada" value="<?= $hora_actual ?>" />
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">TIPO DE DOCUMENTO</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="DNI">DNI</option>
                                        <option value="RUC">RUC</option>
                                        <option value="PASAPORTE">CARNET DE EXTRANJERIA</option>
                                    </select>
                                </div>

                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-3" for="inputName">NUMERO DE DOCUMENTO</label>

                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input class="form-control inp" type="text" id="txt_nrodocumento">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">CLIENTE</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" class="form-control" id="txt_nombre" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">FIJO A PORTAR</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_fijoportar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">PRIMER NOMBRE DE PAPA</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_primernombrepapa" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>


                        <div class="form-group row">
                            <label class="col-3" for="inputName">PRIMER NOMBRE DE MAMA</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_primenombremama" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row" id="date_1">
                            <label class="col-3" for="inputName">FECHA DE NACIMIENTO</label>
                            <div class="col-6">
                                <div class="input-group date" style="width: 100%;">
                                    <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                        <input id="txt_fecnacimiento" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
                                    </div>
                                </div>


                            </div>

                        </div>


                        <div class="form-group row">
                            <label class="col-3" for="inputName">SIGNO DEL ZODIACO</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="txt_sodiaco">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="ARIES">ARIES</option>
                                        <option value="TAURO">TAURO</option>
                                        <option value="GEMINIS">GEMINIS</option>
                                        <option value="CANCER">CANCER</option>
                                        <option value="LEO">LEO</option>
                                        <option value="VIRGO">VIRGO</option>
                                        <option value="LIBRA">LIBRA</option>
                                        <option value="ESCORPIO">ESCORPIO</option>
                                        <option value="SAGITARIO">SAGITARIO</option>
                                        <option value="CAPRICORNIO">CAPRICORNIO</option>
                                        <option value="ACUARIO">ACUARIO</option>
                                        <option value="PISCIS">PISCIS</option>
                                    </select>
                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">LUGAR DE NACIMIENTO <br>(Departamento/Provincia/Distrito)</label>
                            <div class="col-6">
                                <div class="input-group">

                                    <select class="form-control inp col-4" id="sel_departamento" style="width:100%">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                    </select>



                                    <select class="form-control inp col-4" id="sel_provincia" style="width:100%">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                    </select>



                                    <select class="form-control inp col-4" id="sel_distrito" style="width:100%">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                    </select>
                                </div>
                            </div>
                        </div>



                        <div class="form-group row ">
                            <label class="col-3" for="">DIRECCION</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_via" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">RESIDENCIA ACTUAL <br>(Departamento/Provincia/Distrito)</label>
                            <div class="col-6">
                                <div class="input-group">

                                    <select class="form-control inp col-4" id="sel_departamento2" style="width:100%">

                                    </select>

                                    <select class="form-control inp col-4" id="sel_provincia2" style="width:100%">
                                    </select>

                                    <select class="form-control inp col-4" id="sel_distrito2" style="width:100%">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">REFERENCIA DE DIRECCION</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_referencia" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">MOVIL TITULAR</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_titular" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">MOVIL CONTACTO</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_contacto" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">MOVIL PARA COORDINAR INSTALACION</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_coordinacion" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputEmail">EMAIL</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="email" class="form-control" id="txt_email" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">PLANO</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_plano" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">PROMOCION ESPECIAL</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_promocion" />
                                </div>
                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-3" for="inputName">TIPO DE VENTA</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="cbm_tipoventa">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="ALTA NUEVA">ALTA NUEVA</option>
                                        <option value="PORTABILIDAD">PORTABILIDAD</option>

                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="">PRODUCTO</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="cmb_producto" style="width:100%">
                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="">PLAY</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="cbm_play" style="width:100%">
                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="">VELOCIDAD</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="cbm_plan" style="width:100%">
                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="">PAQUETE</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="cbm_paquete" style="width:100%">
                                    </select>

                                </div>

                            </div>
                            <div class="col-1" style="margin-right: 8px;">
                                <button class="btn btn-danger" onclick="Boton_decoadicional()" type="button">Paq. Ad</button>

                            </div>
                            <div class="col-1">
                                <button class="btn btn-danger" onclick="Boton_servicioadicional()" type="button">Serv. Ad</button>
                            </div>

                        </div>
                        <!-- <div class="form-group row">
                            <label class="col-3" for="">ADICIONAL</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="cbm_adicional">

                                        <option value="SI">SI</option>
                                        <option value="NO" selected="selected">NO</option>
                                    </select>

                                </div>

                            </div>

                        </div> -->


                        <div class="form-group row">
                            <label class="col-3" for="inputName">CARGO FIJO</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_cargo_fijo" onkeypress="return soloNumeros(event)" disabled/>

                                </div>
                            </div>

                        </div>



                        <div id="adicional_deco" style="display: none;">
                            <div class="form-group row">
                                <label class="col-3" for="inputName"><span>DECO ADICIONAL</span></label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="cbm_decoadd1">

                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>
                        <!-- <div class="form-group row" style="display: none;">
                            <label class="col-3" for="inputName">DECO ADICIONAL 2</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="cbm_decoadd2">

                                    </select>
                                </div>

                            </div>
                        </div> -->
                        <div id="adicional_premium" style="display: none;">
                            <div class="form-group row">
                                <label class="col-3" for="inputName"><span>PAQUETE PREMIUM</span></label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="cbm_paquetepre">

                                        </select>
                                    </div>

                                </div>
                            </div>
                        </div>

                        <div id="adicional_tabladetalle" style="display: none;">
                            <div class="form-group row">
                                <div class="col-lg-12 table-responsive">
                                    <table id="detalle_adicionales" class="table">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>NOMBRE COLUMNA</th>
                                                <th>PAQUETE</th>
                                                <th>PRECIO</th>
                                            </tr>
                                        </thead>
                                        <tbody id="tbody_detalle_adicionales">

                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        
                        <div id="adicional_cargo_add" style="display: none;">
                            <div class="form-group row">
                                <label class="col-3" for="inputName">CARGO ADICIONAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_cargo_adicional" onkeypress="return soloNumeros(event)" disabled/>

                                    </div>
                                </div>

                            </div>
                        </div>



                        <div class="form-group row">
                            <label class="col-3" for="inputName">CARGO TOTAL</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_cargo_total" onkeypress="return soloNumeros(event)" disabled/>

                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-3" for="inputName">COSTO DE INSTALACION</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_costo_intalacion" onkeypress="return soloNumeros(event)" />


                                </div>
                            </div>

                        </div>
                        <div class="form-group row" hidden>
                            <label class="col-3" for="inputName">ASESOR COMERCIAL</label>
                            <?php
                            $nombre = $_SESSION['S_NOMBRECOMPLETO'];
                            $idusuario = $_SESSION['S_IDUSUARIO']
                            ?>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_asesor_comercial" value="<?= $nombre ?>" />

                                </div>
                            </div>

                            <input type="text" class="form-control" id="txt_idusuario" value="<?= $idusuario ?>" hidden />

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">FULL CLARO</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">

                                    <select class="form-control inp" style="width: 100%;" id="cbm_fullclaro">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>

                                    </select>

                                </div>

                            </div>


                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">¿Cuenta o conto con servicio CLARO HOGAR en la misma direccion?</label>
                            <div class="col-6" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_pregunta" />

                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">TIENE BANCA MOVIL</label>
                            <div class="col-6">
                                <div class="input-group" style="width: 100%;">

                                    <select class="form-control inp" style="width: 100%;" id="txt_bancamovil">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>

                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">DIA DE PAGO</label>
                            <div class="col-6">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="date" class="form-control" id="txt_diapago" min="<?= $fecha_actual ?>" max="<?= $fecha_actual ?>" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-3" for="inputName">HORA DE PAGO</label>
                            <div class="col-6">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="time" class="form-control" id="txt_horapago" />
                                </div>
                            </div>

                        </div>



                    </div>
                </form>
            </div>
            <!-- Modal Footer -->
            <div class="ibox-footer" style="text-align: center;">
                <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                <button type="submit" class="btn btn-primary submitBtn" onclick="Registrar_Venta()">Registrar</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript" src="../../js/console_ubigeo.js?rev=<?php echo time(); ?>"></script>
<script type="text/javascript" src="../../js/console_lista_ubigeo.js?rev=<?php echo time(); ?>"></script>

<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        // $("#form-sample-1").validate();
        $('.js-example-basic-single').select2();
        listar_departamento();
        listar_departamento2();
        listar_producto();
        listar_producto_pr();
        listar_play_combo();
        listar_plan_internet_combo();
        listar_paquete_combo();
        listar_decoadd1();
        listar_decoadd2();
        listar_paquetepremium();


    });

    $("#cbm_tdocumento").change(function() {
        var tipdoc = $("#cbm_tdocumento").val();
        if (tipdoc == "DNI") {
            $("#txt_nrodocumento").attr("maxlength", "8");
            $("#txt_nrodocumento").attr("onkeypress", "return soloNumeros(event)");


        } else if (tipdoc == "RUC") {
            $("#txt_nrodocumento").attr("maxlength", "11");
            $("#txt_nrodocumento").attr("onkeypress", "return soloNumeros(event)");
        } else if (tipdoc == "CARNET DE EXTRANJERIA") {
            $("#txt_nrodocumento").attr("maxlength", "12");
            $("#txt_nrodocumento").attr("onkeypress", "");
        } else {
            $("#txt_nrodocumento").attr("maxlength", "12");
            $("#txt_nrodocumento").attr("onkeypress", "");
        }
    })

    $("#sel_departamento").change(function() {
        var iddepartamento = $("#sel_departamento").val();
        listar_pronvincia(iddepartamento);
    })
    $("#sel_departamento2").change(function() {
        var iddepartamento2 = $("#sel_departamento2").val();
        listar_pronvincia2(iddepartamento2);
    })

    $("#sel_provincia").change(function() {
        var idprovincia = $("#sel_provincia").val();
        listar_distrito(idprovincia);
    })
    $("#sel_provincia2").change(function() {
        var idprovincia2 = $("#sel_provincia2").val();
        listar_distrito2(idprovincia2);
    })

    $("#cbm_paquete").change(function() {
        var id = $("#cbm_paquete").val();
        cambiar_precio_fijo(id);
    })

    $("#cbm_plan").change(function() {
        var idproducto = $("#cmb_producto").val();
        var idplay = $("#cbm_play").val();
        var idplan = $("#cbm_plan").val();

        listar_paquete_combo(idproducto, idplay, idplan);
        var id = $("#cbm_paquete").val();
        cambiar_precio_fijo(id);
    })

    // $("#cbm_adicional").change(function() {
    //     var valor = $("#cbm_adicional").val();
    //     if (valor == "SI") {
    //         document.getElementById("adicionales").style.display = "block";
    //     } else {
    //         document.getElementById("adicionales").style.display = "none";
    //     }
    // })
    

    $("#cbm_adicional").change(function() {
        var valor = $("#cbm_adicional").val();
        if (valor == "SI") {
            document.getElementById("adicionales").style.display = "block";


        } else {
            document.getElementById("adicionales").style.display = "none";
            listar_decoadd1();
            listar_decoadd2();
            listar_paquetepremium();
            removerAll();
            $("#txt_cargo_adicional").val("0");
            $("#txt_cargo_total").val($("#txt_cargo_fijo").val());

        }
    })

    $("#cbm_decoadd1").change(function() {
        var id = $("#cbm_decoadd1").val();
        var nombre = $('#cbm_decoadd1 option:selected').html();
        Obtener_precio_decoadd1(id, nombre, "DECO ADICIONAL 1");
    })

    $("#cbm_decoadd2").change(function() {
        var id = $("#cbm_decoadd2").val();
        var nombre2 = $('#cbm_decoadd2 option:selected').html();
        Obtener_precio_decoadd2(id, nombre2, "DECO ADICIONAL 2");
    })
    $("#cbm_paquetepre").change(function() {
        var id = $("#cbm_paquetepre").val();
        var nombrepre = $('#cbm_paquetepre option:selected').html();
        Obtener_precio_paqupremium(id, nombrepre, "PAQUETE PREMIUM");
    })
</script>