<?php
session_start();


require '../../modelo/modelo_conexion_pdo.php';

getPermisos(6);
?>

<div class="row" id="recarga">
    <div class="col-md-12">
        <div class="ibox ibox-default">
            <div class="ibox-head">
                <div class="ibox-title">VENTAS
                    <?php

                    $rol = $_SESSION['S_ROL'];
                    $id_usuario = $_SESSION['S_IDUSUARIO'];
                    ?>
                    <input type="text" id="txt_rol" value="<?= $rol ?>" hidden>
                    <input type="text" id="txt_id_usuario" value="<?= $id_usuario ?>" hidden>
                </div>
                <?php if ($_SESSION['permisosMod']['w']) { ?>
                    <div class="ibox-tools">
                        <button class="btn btn-danger" onclick="cargar_contenido('contenido_principal','venta/vista_registro_venta.php')">Nuevo Registro</button>
                    </div>
                <?php } ?>
            </div>
            <div class="ibox-body">
                <div class="form-group row">
                    <?php
                    date_default_timezone_set('America/Lima');
                    $fecha_actual = date("Y-m-d");
                    $hora_actual = date("H:i:s");
                    ?>

                    <div class="col-lg-3">
                        <label for="inputName"><strong>Fecha de inicio</strong></label>
                        <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                            <input type="date" class="form-control" id="fecha_inicio" value="<?= $fecha_actual ?>" />
                        </div>

                    </div>

                    <div class="col-lg-3">
                        <label for="inputName"><strong>Fecha de fin</strong></label>
                        <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                            <input type="date" class="form-control" id="fecha_fin" value="<?= $fecha_actual ?>" />
                        </div>


                    </div>
                    <div class="col-lg-3">
                        <label for=""><br></label>
                        <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                            <button class="btn btn-danger" onclick="listar_venta_rechazada()">Buscar</button>
                        </div>


                    </div>
                    <div class="col-lg-12">
                        <strong>
                            <hr>
                        </strong>
                    </div>
                </div>
                <table id="tabla_venta" class="display" style="width:100%">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Acci&oacute;n</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Tipo Doc.</th>
                            <th>Nro Doc.</th>
                            <th>Fijo a portar</th>
                            <th>Fecha Nacimiento</th>
                            <th>Lugar nacimiento</th>
                            <th>Primer nombre papa</th>
                            <th>Primer nombre mama</th>
                            <th>Direcci&oacute;n</th>
                            <th>Residencia actual</th>
                            <th>Referencia de direccion</th>
                            <th>Movil titular</th>
                            <th>Movil contacto</th>
                            <th>Movil para coordinar</th>
                            <th>Correo</th>
                            <th>Plano</th>
                            <th>Producto</th>
                            <th>Promocion</th>
                            <th>Plan</th>
                            <th>Cargo fijo</th>
                            <th>Asesor comercial</th>
                            <th>Full claro</th>
                            <th>Pregunta</th>
                            <th>Banca Movil</th>
                        </tr>
                    </thead>
                    <tbody>
                    </tbody>

                    <tfoot>
                        <tr>
                            <th>#</th>
                            <th>Estado</th>
                            <th>Acci&oacute;n</th>
                            <th>Fecha</th>
                            <th>Cliente</th>
                            <th>Tipo Doc.</th>
                            <th>Nro Doc.</th>
                            <th>Fijo a portar</th>
                            <th>Fecha Nacimiento</th>
                            <th>Lugar nacimiento</th>
                            <th>Primer nombre papa</th>
                            <th>Primer nombre mama</th>
                            <th>Direcci&oacute;n</th>
                            <th>Residencia actual</th>
                            <th>Referencia de direccion</th>
                            <th>Movil titular</th>
                            <th>Movil contacto</th>
                            <th>Movil para coordinar</th>
                            <th>Correo</th>
                            <th>Plano</th>
                            <th>Producto</th>
                            <th>Promocion</th>
                            <th>Plan</th>
                            <th>Cargo fijo</th>
                            <th>Asesor comercial</th>
                            <th>Full claro</th>
                            <th>Pregunta</th>
                            <th>Banca Movil</th>
                        </tr>
                    </tfoot>
                </table>
            </div>
        </div>
    </div>
</div>




<!-- Modal editar-->



<div class="modal fade" id="modal_editar" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Editar estados de venta</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form autocomplete="false" onsubmit="return false">
                    <div class="col-lg-12">
                        <?php
                        $rol = $_SESSION['S_ROL']; ?>
                        <div class="form-group col-lg-12" hidden>
                            <input type="text" id="txt_idventa">
                            <label for="inputName">Rol Usuario</label>
                            <input type="text" class="form-control" id="txt_rol" value="<?= $rol ?>" />
                        </div>

                        <div id="cuadro_venta">
                            <div class="form-group row">
                                <label class="col-4" for="inputName">VA</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_va" onchange="ChangeColor(this.value)">
                                            <option value="SIN ELEGIR" selected>SIN ELEGIR</option>
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">

                                <label class="col-4" for="inputName">SOT</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_sot" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-4" for="inputName">SET</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_set" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-4" for="inputName">ESTADO DE VENTA</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_estatus">
                                            <option value="OBSERVADO">OBSERVADO</option>
                                            <option value="APROBADO">APROBADO</option>
                                            <option value="ADJUNTAR DOCUMENTO">ADJUNTAR DOCUMENTO</option>
                                            <option value="PENDIENTE SOT">PENDIENTE SOT</option>
                                            <option value="EJECUCION">EJECUCION</option>
                                            <option value="AGENDADO">AGENDADO</option>
                                            <option value="PENDIENTE REASIGNACION">PENDIENTE REASIGNACION</option>
                                            <option value="PENDIENTE VALIDAR">PENDIENTE VALIDAR</option>
                                            <option value="ATENDIDA">ATENDIDA</option>
                                            <option value="RECHAZADO">RECHAZADO</option>
                                            <option value="ENVIADO">ENVIADO</option>
                                            <option value="TRAMITANDO">TRAMITANDO</option>
                                            <option value="INSTALADO">INSTALADO</option>
                                            <option value="PENDIENTE PAGO">PENDIENTE PAGO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">

                                <label class="col-4" for="inputName">COMENTARIO DE ESTADO INTERNO</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_comentario_estado_venta" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-4" for="inputName">COMENTARIO</label>
                                <div class="col-8">
                                    <div class="input-group" style="width: 100%;">
                                        <textarea id="txt_comentario_estado" cols="30" rows="5" class="form-control"></textarea>

                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-4" for="inputName">FECHA DE INSTALACION</label>
                                <div class="col-8">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fecha_instalacion" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="date_5">
                                <label class="font-normal col-4">HORA DE INSTALACION</label>
                                <div class="input-daterange input-group col-8" id="datepicker">
                                    <input id="hora_inicio_instalacion" class="input-sm form-control" type="time" name="start">
                                    <span class="input-group-addon p-l-10 p-r-10">to</span>
                                    <input id="hora_fin_instalacion" class="input-sm form-control" type="time" name="end">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-12">
                                    <strong>
                                        <hr>
                                    </strong>
                                </div>
                            </div>
                        </div>


                        <div class="form-group row">
                            <input type="text" id="txt_idventa" hidden>
                            <label class="col-4" for="inputName">MOVIL DE LLAMADA</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" class="form-control" id="txt_nrollamada_editar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <input type="text" class="form-control" id="txt_idgrabacion" hidden />
                            <label class="col-4" for="inputName">FECHA DE LLAMADA</label>
                            <div class="col-8">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="date" class="form-control" id="txt_fechallamada_editar" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">HORA DE LLAMADA</label>
                            <div class="col-8">
                                <input type="time" class="form-control" id="txt_horallamada_editar" />
                            </div>

                        </div>

                        <div class="form-group row">
                            <input type="text" id="txt_cliente" hidden>
                            <label class="col-4" for="inputName">TIPO DE DOCUMENTO</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento_editar">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="DNI">DNI</option>
                                        <option value="RUC">RUC</option>
                                        <option value="PASAPORTE">PASAPORTE</option>
                                    </select>
                                </div>

                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-4" for="inputName">NUMERO DE DOCUMENTO</label>

                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" id="txt_nrodocumento_editar_actual" onkeypress="return soloNumeros(event)" hidden>
                                    <input class="form-control" type="text" id="txt_nrodocumento_editar_nuevo">
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">CLIENTE</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <input type="text" class="form-control" id="txt_nombre_editar" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">FIJO A PORTAR</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_fijoportar_editar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">PRIMER NOMBRE DE PAPA</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_primernombrepapa_editar" onkeypress="return sololetras(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">PRIMER NOMBRE DE MAMA</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_primenombremama_editar" onkeypress="return sololetras(event)" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">FECHA DE NACIMIENTO</label>
                            <div class="col-8">
                                <div class="input-group date" style="width: 100%;">
                                    <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                        <input id="txt_fecnacimiento_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
                                    </div>
                                </div>


                            </div>

                        </div>


                        <div class="form-group row">
                            <label class="col-4" for="inputName">SIGNO DEL ZODIACO</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" style="width: 100%;" id="txt_sodiaco_editar">
                                        <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                        <option value="ARIES">ARIES</option>
                                        <option value="TAURO">TAURO</option>
                                        <option value="GEMINIS">GEMINIS</option>
                                        <option value="CANCER">CANCER</option>
                                        <option value="LEO">LEO</option>
                                        <option value="VIRGO">VIRGO</option>
                                        <option value="LIBRA">LIBRA</option>
                                        <option value="ESCORPIO">ESCORPIO</option>
                                        <option value="SAGITARIO">SAGITARIO</option>
                                        <option value="CAPRICORNIO">CAPRICORNIO</option>
                                        <option value="ACUARIO">ACUARIO</option>
                                        <option value="PISCIS">PISCIS</option>
                                    </select>
                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">LUGAR DE NACIMIENTO <br>(Departamento/Provincia/Distrito)</label>
                            <div class="col-8">
                                <div class="input-group">

                                    <select class="form-control inp col-4" id="sel_departamento_editar" style="width:100%">
                                    </select>



                                    <select class="form-control inp col-4" id="sel_provincia_editar" style="width:100%">
                                    </select>



                                    <select class="form-control inp col-4" id="sel_distrito_editar" style="width:100%">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row ">
                            <input type="text" class="form-control" id="txt_iddireccion" hidden />
                            <label class="col-4" for="">VIA</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_via_editar" />
                                </div>
                            </div>

                        </div>




                        <div class="form-group row">
                            <label class="col-4" for="inputName">RESIDENCIA ACTUAL <br>(Departamento/Provincia/Distrito)</label>
                            <div class="col-8">
                                <div class="input-group">

                                    <select class="form-control inp col-4" id="sel_departamento2_editar" style="width:100%">
                                    </select>

                                    <select class="form-control inp col-4" id="sel_provincia2_editar" style="width:100%">
                                    </select>

                                    <select class="form-control inp col-4" id="sel_distrito2_editar" style="width:100%">
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-4" for="inputName">REFERENCIA DE DIRECCION</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_referencia_editar" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputEmail">EMAIL</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="email" class="form-control" id="txt_email_editar" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">MOVIL TITULAR</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_titular_editar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-4" for="inputName">MOVIL CONTACTO</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_contacto_editar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">MOVIL PARA COORDINAR INSTALACION</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_movil_coordinacion_editar" onkeypress="return soloNumeros(event)" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <input type="text" class="form-control" id="txt_iddetallepre" hidden />
                            <label class="col-4" for="inputName">PLANO</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_plano_editar" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="">PRODUCTO</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="producto_editar" style="width:100%">
                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">PROMOCION ESPECIAL</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_promocion_editar" />
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">PLAN</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_plan_editar" />
                                </div>
                            </div>

                        </div>

                        <div class="form-group row">
                            <label class="col-4" for="inputName">CARGO FIJO</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_cargo_fijo_editar" onkeypress="return soloNumeros(event)" />

                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">COSTO DE INSTALACION</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_costo_intalacion_editar" onkeypress="return soloNumeros(event)" />


                                </div>
                            </div>

                        </div>
                        <div class="form-group row" hidden>
                            <label class="col-4" for="inputName">ASESOR COMERCIAL</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_asesor_comercial_editar" />
                                    <input type="text" class="form-control" id="txt_idusuario_editar" hidden />

                                </div>
                            </div>



                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="">ASESOR COMERCIAL</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">
                                    <select class="form-control inp" id="cbm_idusuario_editar" style="width:100%">
                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">FULL CLARO</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">

                                    <select class="form-control inp" style="width: 100%;" id="cbm_fullclaro_editar">
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>

                                    </select>

                                </div>

                            </div>


                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">¿Cuenta o conto con servicio CLARO HOGAR en la misma direccion?</label>
                            <div class="col-8" style="width: 100%;">
                                <div class="input-group" style="width: 100%;">

                                    <input type="text" class="form-control" id="txt_pregunta_editar" />

                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">TIENE BANCA MOVIL</label>
                            <div class="col-8">
                                <div class="input-group" style="width: 100%;">

                                    <select class="form-control inp" style="width: 100%;" id="txt_bancamovil_editar">
                                        <option value="SI">SI</option>
                                        <option value="NO">NO</option>

                                    </select>

                                </div>

                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">DIA DE PAGO</label>
                            <div class="col-8">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="date" class="form-control" id="txt_diapago_editar" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">
                            <label class="col-4" for="inputName">HORA DE PAGO</label>
                            <div class="col-8">
                                <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                    <input type="time" class="form-control" id="txt_horapago_editar" />
                                </div>
                            </div>

                        </div>
                        <div class="form-group row">

                            <div class="col-9">
                                <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;">

                                </div>
                            </div>

                        </div>
                        <div id="comentariosmensaje" style="display: none;">
                            <div class="form-group row">
                                <label class="col-4" for="inputName">COMENTARIOS</label>
                                <div class="col-8" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <textarea id="txt_comentario" cols="30" rows="5" class="form-control"></textarea>

                                    </div>
                                </div>

                            </div>
                        </div>




                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <div class="" id="div_botoneditar">
                    <!-- <button type="button" class="btn btn-primary submitBtn" onclick="Editar_Venta()">Actualizar</button> -->
                </div>

            </div>
        </div>
    </div>
</div>




<script type="text/javascript" src="../../js/console_ubigeo.js?rev=<?php echo time(); ?>"></script>
<script type="text/javascript" src="../../js/console_lista_ubigeo.js?rev=<?php echo time(); ?>"></script>
<script>
    $(document).ready(function() {
        $('.js-example-basic-single').select2();

        listar_venta_rechazada();
        listar_asesores_combo();
        listar_departamento_editar_nac();
        listar_departamento_editar_loc()
        listar_pronvincia_editar_nac();
        listar_pronvincia2_editar_loc();
        listar_distrito_editar_nac();
        listar_distrito_editar_loc();

        listar_producto();

        function listar_venta_rechazada() {
            var rol = $("#txt_rol").val();
            var idusuario = $("#txt_id_usuario").val();
            var fechainicio = $("#fecha_inicio").val();
            var fechafin = $("#fecha_fin").val();
            t_venta = $("#tabla_venta").DataTable({

                ordering: true,
                pageLength: 10,
                destroy: true,
                async: false,
                paging: true,
                bLengthChange: false,
                // responsive: true,
                scrollX: true,
                autoWidth: false,
                ajax: {
                    method: "POST",
                    url: "../controlador/venta/controlador_venta_listar_rechazadas.php",
                    data: {
                        idusuario: idusuario,
                        rol: rol,
                        fechainicio: fechainicio,
                        fechafin: fechafin
                    }
                },
                columns: [{
                        defaultContent: ""
                    },
                    {
                        data: "preventa_status",
                        render: function(data, type, row) {
                            if (data == "ENVIADO") {
                                return (
                                    "<span class='badge badge-info badge-pill m-r-5 m-b-5' style='background-color:#fff; color:black;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "OBSERVADO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f75a5f ;''>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "PENDIENTE PAGO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(201, 181, 116);'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "EJECUCION") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#178ab0;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "AGENDADO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#a4a89d;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "PENDIENTE VALIDAR") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f77212;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "INSTALADO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#2ecc71;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "RECHAZADO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#e74c3c;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "PENDIENTE REASIGNACION") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#efa1f5;'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "PENDIENTE REASIGNACION" || data == "ADJUNTAR DOCUMENTO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(216, 215, 209);'>" +
                                    data +
                                    "</span>"
                                );
                            } else if (data == "TRAMITANDO") {
                                return (
                                    "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#1999c4;'>" +
                                    data +
                                    "</span>"
                                );
                            }
                            // else {
                            //   return (
                            //     "<span class='badge badge-warning badge-pill m-r-5 m-b-5'>" +
                            //     data +
                            //     "</span>"
                            //   );
                            // }

                        },

                    },
                    {
                        data: "preventa_status",
                        render: function(data, type, row) {
                            if (data == "EJECUCION" && rol == "ASESOR COMERCIAL") {
                                return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";

                            } else if ((data == "ENVIADO" || data == "OBSERVADO" || data == "EJECUCION") && rol != "ASESOR COMERCIAL") {
                                return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
                            } else if ((data == "ENVIADO" || data == "OBSERVADO") && rol == "ASESOR COMERCIAL") {
                                return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
                            } else {
                                return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
                            }

                        },
                    },

                    {
                        data: "fechallamada"
                    },
                    {
                        data: "cliente_nombre"
                    },
                    {
                        data: "cliente_tipdocumento"
                    },
                    {
                        data: "cliente_nrodocumento"
                    },
                    {
                        data: "cliente_fijoportar"
                    },
                    {
                        data: "cliente_fenacimiento"
                    },
                    {
                        data: "lugardenacimiento"
                    },
                    {
                        data: "cliente_papa"
                    },
                    {
                        data: "cliente_mama"
                    },
                    {
                        data: "direccion_via"
                    },
                    {
                        data: "residenciaactual"
                    },
                    {
                        data: "cliente_referencia"
                    },
                    {
                        data: "cliente_moviltitular"
                    },
                    {
                        data: "cliente_movilcontacto"
                    },
                    {
                        data: "cliente_movilcoordinacion"
                    },
                    {
                        data: "cliente_correo"
                    },
                    {
                        data: "preventa_plano"
                    },
                    {
                        data: "producto_nombre"
                    },
                    {
                        data: "detalleprev_promocion"
                    },
                    {
                        data: "detalleprev_plan"
                    },
                    {
                        data: "detalleprev_cargofijo"
                    },
                    {
                        data: "detallepre_asesorcomercial"
                    },
                    {
                        data: "detallepre_fullclaro"
                    },
                    {
                        data: "detallepre_pregunta"
                    },
                    {
                        data: "preventapre_bancamovil"
                    },


                ],
                fnRowCallback: function(nRow, aData, iDisplayIndex, iDisplayIndexFull) {
                    // $($(nRow).find("td")[3]).css("text-align", "center");
                    // $($(nRow).find("td")[4]).css("text-align", "center");
                },
                language: idioma_espanol,
                select: true,
            });
            t_venta.on("draw.dt", function() {
                var PageInfo = $("#tabla_venta").DataTable().page.info();
                t_venta
                    .column(0, {
                        page: "current"
                    })
                    .nodes()
                    .each(function(cell, i) {
                        cell.innerHTML = i + 1 + PageInfo.start;
                    });
            });

            // if (rol=="ASESOR COMERCIAL") {
            //   t_venta.column(1).visible(false);
            //   t_venta.column(2).visible(false);
            //   t_venta.column(3).visible(false);
            // }
        }

        listar_venta_rechazada();



    });

    $("#cbm_tdocumento_editar").change(function() {
        var tipdoc = $("#cbm_tdocumento_editar").val();
        if (tipdoc == "DNI") {
            $("#txt_nrodocumento_editar_nuevo").attr("maxlength", "8");
            $("#txt_nrodocumento_editar_nuevo").attr("onkeypress", "return soloNumeros(event)");
        } else if (tipdoc == "RUC") {
            $("#txt_nrodocumento_editar_nuevo").attr("maxlength", "11");
            $("#txt_nrodocumento_editar_nuevo").attr("onkeypress", "return soloNumeros(event)");

        } else {
            $("#txt_nrodocumento_editar_nuevo").attr("maxlength", "12");
            $("#txt_nrodocumento_editar_nuevo").attr("onkeypress", "");
        }
    })



    $("#sel_departamento_editar").change(function() {
        var iddepartamento = $("#sel_departamento_editar").val();
        listar_pronvincia(iddepartamento);
    })
    $("#sel_departamento2_editar").change(function() {
        var iddepartamento2 = $("#sel_departamento2_editar").val();
        listar_pronvincia2(iddepartamento2);
    })

    $("#sel_provincia_editar").change(function() {
        var idprovincia = $("#sel_provincia_editar").val();
        listar_distrito(idprovincia);
    })
    $("#sel_provincia2_editar").change(function() {
        var idprovincia2 = $("#sel_provincia2_editar").val();
        listar_distrito2(idprovincia2);
    })
</script>