<?php
session_start();

?>

<?php if ($_SESSION['S_ROL'] == "ADMINISTRADOR") { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="ibox ibox-default">
                <div class="ibox-head">
                    <div class="ibox-title">EDITAR VENTA</div>
                    <div class="ibox-tools">
                        <button class="btn btn-danger" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                        <!-- <button class="btn btn-danger" onclick="mostrardatoseditar()">Editar</button> -->
                    </div>
                </div>
                <div class="ibox-body">
                    <p class="statusMsg"></p>
                    <form autocomplete="false" onsubmit="return false">
                        <div class="col-lg-11">
                            <?php
                            $rol = $_SESSION['S_ROL']; ?>
                            <div class="form-group col-lg-12" hidden>
                                <input type="text" id="txt_idventa">
                                <label for="inputName">Rol Usuario</label>
                                <input type="text" class="form-control" id="txt_rol" value="<?= $rol ?>" />
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">VA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_va" onchange="ChangeColor(this.value)">
                                            <option value="SIN ELEGIR">SIN ELEGIR</option>
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">SOT</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_sot" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">SET</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_set" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">ESTADO DE VENTA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_estatus">
                                            <option value="OBSERVADO">OBSERVADO</option>
                                            <option value="ADJUNTAR DOC.">ADJUNTAR DOC.</option>
                                            <option value="APROBADO">APROBADO</option>
                                            <option value="P. GENE. SOT">P. GENE. SOT</option>
                                            <option value="EJECUCION">EJECUCION</option>
                                            <option value="AGENDADO">AGENDADO</option>
                                            <option value="REASIGNA. P.">REASIGNA. P.</option>
                                            <option value="P. VALIDAR">P. VALIDAR</option>
                                            <option value="ATENDIDA">ATENDIDA</option>
                                            <option value="RECHAZADO">RECHAZADO</option>
                                            <option value="ENVIADO">ENVIADO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">COMENTARIO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_comentario_estado" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">FECHA DE INSTALACION</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fecha_instalacion" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="date_5">
                                <label class="font-normal col-3">HORA DE INSTALACION</label>
                                <div class="input-daterange input-group col-6" id="datepicker">
                                    <input id="hora_inicio_instalacion" class="input-sm form-control" type="time" name="start">
                                    <span class="input-group-addon p-l-10 p-r-10">to</span>
                                    <input id="hora_fin_instalacion" class="input-sm form-control" type="time" name="end">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-9">
                                    <strong>
                                        <hr>
                                    </strong>
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="text" id="txt_idventa" hidden>
                                <label class="col-3" for="inputName">MOVIL DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nrollamada_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_idgrabacion" hidden />
                                <label class="col-3" for="inputName">FECHA DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fechallamada_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE LLAMADA</label>
                                <div class="col-6">
                                    <input type="time" class="form-control" id="txt_horallamada_editar" />
                                </div>

                            </div>

                            <div class="form-group row">
                                <input type="text" id="txt_cliente" hidden>
                                <label class="col-3" for="inputName">TIPO DE DOCUMENTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="DNI">DNI</option>
                                            <option value="RUC">RUC</option>
                                            <option value="PASAPORTE">PASAPORTE</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">NUMERO DE DOCUMENTO</label>

                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" id="txt_nrodocumento_editar_actual" onkeypress="return soloNumeros(event)" hidden>
                                        <input class="form-control" type="text" id="txt_nrodocumento_editar_nuevo" onkeypress="return soloNumeros(event)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">CLIENTE</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nombre_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FIJO A PORTAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_fijoportar_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE PAPA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primernombrepapa_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE MAMA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primenombremama_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">FECHA DE NACIMIENTO</label>
                                <div class="col-6">
                                    <div class="input-group date" style="width: 100%;">
                                        <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                            <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                            <input id="txt_fecnacimiento_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">SIGNO DEL ZODIACO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="txt_sodiaco_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="ARIES">ARIES</option>
                                            <option value="TAURO">TAURO</option>
                                            <option value="GEMINIS">GEMINIS</option>
                                            <option value="CANCER">CANCER</option>
                                            <option value="LEO">LEO</option>
                                            <option value="VIRGO">VIRGO</option>
                                            <option value="LIBRA">LIBRA</option>
                                            <option value="ESCORPIO">ESCORPIO</option>
                                            <option value="SAGITARIO">SAGITARIO</option>
                                            <option value="CAPRICORNIO">CAPRICORNIO</option>
                                            <option value="ACUARIO">ACUARIO</option>
                                            <option value="PISCIS">PISCIS</option>
                                        </select>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">LUGAR DE NACIMIENTO <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_provincia_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_distrito_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <input type="text" class="form-control" id="txt_iddireccion" hidden />
                                <label class="col-3" for="">DIRECCION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_via_editar" />
                                    </div>
                                </div>

                            </div>




                            <div class="form-group row">
                                <label class="col-3" for="inputName">RESIDENCIA ACTUAL <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_provincia2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_distrito2_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">REFERENCIA DE DIRECCION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_referencia_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputEmail">EMAIL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="email" class="form-control" id="txt_email_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL TITULAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_titular_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL CONTACTO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_contacto_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL PARA COORDINAR INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_coordinacion_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_iddetallepre" hidden />
                                <label class="col-3" for="inputName">PLANO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plano_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="">PRODUCTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" id="producto_editar" style="width:100%">
                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PROMOCION ESPECIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_promocion_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PLAN</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plan_editar" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">CARGO FIJO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_cargo_fijo_editar" onkeypress="return soloNumeros(event)" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">COSTO DE INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_costo_intalacion_editar" onkeypress="return soloNumeros(event)" />


                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">ASESOR COMERCIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_asesor_comercial_editar" />
                                        <input type="text" class="form-control" id="txt_idusuario_editar" hidden />

                                    </div>
                                </div>



                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FULL CLARO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_fullclaro_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">¿Cuenta o conto con servicio CLARO HOGAR en la misma direccion?</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_pregunta_editar" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">TIENE BANCA MOVIL</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_bancamovil_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">DIA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_diapago_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="time" class="form-control" id="txt_horapago_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row" hidden>

                                <div class="col-9">
                                    <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;">

                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">COMENTARIOS</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <textarea id="txt_comentario" cols="30" rows="5" class="form-control"></textarea>

                                    </div>
                                </div>

                            </div>


                            <div class="form-group col-lg-12"><br>
                                <div class="alert alert-danger alert-bordered" id="div_error_editar" style="display: none;"></div>
                            </div>

                        </div>
                    </form>
                </div>
                <!-- Modal Footer -->
                <div class="ibox-footer" style="text-align: center;">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                    <div class="" id="div_botoneditar">
                        <!-- <button type="button" class="btn btn-primary submitBtn" onclick="Editar_Venta()">Actualizar</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>

<?php } ?>
<?php if ($_SESSION['S_ROL'] == "ASESOR COMERCIAL") { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="ibox ibox-default">
                <div class="ibox-head">
                    <div class="ibox-title">EDITAR VENTA</div>
                    <div class="ibox-tools">
                        <button class="btn btn-danger" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                        <!-- <button class="btn btn-danger" onclick="mostrardatoseditar()">Editar</button> -->
                    </div>
                </div>
                <div class="ibox-body">
                    <p class="statusMsg"></p>
                    <form autocomplete="false" onsubmit="return false">
                        <div class="col-lg-11">
                            <?php
                            $rol = $_SESSION['S_ROL']; ?>
                            <div class="form-group col-lg-12" hidden>
                                <input type="text" id="txt_idventa">
                                <label for="inputName">Rol Usuario</label>
                                <input type="text" class="form-control" id="txt_rol" value="<?= $rol ?>" />
                            </div>

                            <div id="cuadro_venta">
                                <div class="form-group row">
                                    <label class="col-3" for="inputName">VA</label>
                                    <div class="col-6">
                                        <div class="input-group" style="width: 100%;">

                                            <select class="form-control inp" style="width: 100%;" id="txt_va" onchange="ChangeColor(this.value)">
                                                <option value="SIN ELEGIR">SIN ELEGIR</option>
                                                <option value="SI">SI</option>
                                                <option value="NO">NO</option>

                                            </select>

                                        </div>

                                    </div>


                                </div>
                                <div class="form-group row">

                                    <label class="col-3" for="inputName">SOT</label>
                                    <div class="col-6">
                                        <div class="input-group" style="width: 100%;">
                                            <input type="text" class="form-control" id="txt_sot" onkeypress="return soloNumeros(event)" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <label class="col-3" for="inputName">SET</label>
                                    <div class="col-6">
                                        <div class="input-group" style="width: 100%;">
                                            <input type="text" class="form-control" id="txt_set" onkeypress="return soloNumeros(event)" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-3" for="inputName">ESTADO DE VENTA</label>
                                    <div class="col-6">
                                        <div class="input-group" style="width: 100%;">

                                            <select class="form-control inp" style="width: 100%;" id="cbm_estatus" disabled>
                                                <option value="OBSERVADO">OBSERVADO</option>
                                                <option value="ADJUNTAR DOC.">ADJUNTAR DOC.</option>
                                                <option value="APROBADO">APROBADO</option>
                                                <option value="P. GENE. SOT">P. GENE. SOT</option>
                                                <option value="EJECUCION">EJECUCION</option>
                                                <option value="AGENDADO">AGENDADO</option>
                                                <option value="REASIGNA. P.">REASIGNA. P.</option>
                                                <option value="P. VALIDAR">P. VALIDAR</option>
                                                <option value="ATENDIDA">ATENDIDA</option>
                                                <option value="RECHAZADO">RECHAZADO</option>
                                                <option value="ENVIADO">ENVIADO</option>

                                            </select>

                                        </div>

                                    </div>

                                </div>
                                <div class="form-group row">

                                    <label class="col-3" for="inputName">COMENTARIO</label>
                                    <div class="col-6">
                                        <div class="input-group" style="width: 100%;">
                                            <input type="text" class="form-control" id="txt_comentario_estado" onkeypress="return soloNumeros(event)" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row">

                                    <label class="col-3" for="inputName">FECHA DE INSTALACION</label>
                                    <div class="col-6">
                                        <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                            <input type="date" class="form-control" id="txt_fecha_instalacion" disabled />
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group row" id="date_5">
                                    <label class="font-normal col-3">HORA DE INSTALACION</label>
                                    <div class="input-daterange input-group col-6" id="datepicker">
                                        <input id="hora_inicio_instalacion" class="input-sm form-control" type="time" name="start" disabled>
                                        <span class="input-group-addon p-l-10 p-r-10">to</span>
                                        <input id="hora_fin_instalacion" class="input-sm form-control" type="time" name="end" disabled>
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <div class="col-9">
                                        <strong>
                                            <hr>
                                        </strong>
                                    </div>
                                </div>
                            </div>


                            <div class="form-group row">
                                <input type="text" id="txt_idventa" hidden>
                                <label class="col-3" for="inputName">MOVIL DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nrollamada_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_idgrabacion" hidden />
                                <label class="col-3" for="inputName">FECHA DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fechallamada_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE LLAMADA</label>
                                <div class="col-6">
                                    <input type="time" class="form-control" id="txt_horallamada_editar" />
                                </div>

                            </div>

                            <div class="form-group row">
                                <input type="text" id="txt_cliente" hidden>
                                <label class="col-3" for="inputName">TIPO DE DOCUMENTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="DNI">DNI</option>
                                            <option value="RUC">RUC</option>
                                            <option value="PASAPORTE">PASAPORTE</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">NUMERO DE DOCUMENTO</label>

                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" id="txt_nrodocumento_editar_actual" onkeypress="return soloNumeros(event)" hidden>
                                        <input class="form-control" type="text" id="txt_nrodocumento_editar_nuevo" onkeypress="return soloNumeros(event)">
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">CLIENTE</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nombre_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FIJO A PORTAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_fijoportar_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE PAPA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primernombrepapa_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE MAMA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primenombremama_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FECHA DE NACIMIENTO</label>
                                <div class="col-6">
                                    <div class="input-group date" style="width: 100%;">
                                        <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                            <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                            <input id="txt_fecnacimiento_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
                                        </div>
                                    </div>


                                </div>

                            </div>


                            <div class="form-group row">
                                <label class="col-3" for="inputName">SIGNO DEL ZODIACO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="txt_sodiaco_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="ARIES">ARIES</option>
                                            <option value="TAURO">TAURO</option>
                                            <option value="GEMINIS">GEMINIS</option>
                                            <option value="CANCER">CANCER</option>
                                            <option value="LEO">LEO</option>
                                            <option value="VIRGO">VIRGO</option>
                                            <option value="LIBRA">LIBRA</option>
                                            <option value="ESCORPIO">ESCORPIO</option>
                                            <option value="SAGITARIO">SAGITARIO</option>
                                            <option value="CAPRICORNIO">CAPRICORNIO</option>
                                            <option value="ACUARIO">ACUARIO</option>
                                            <option value="PISCIS">PISCIS</option>
                                        </select>
                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">LUGAR DE NACIMIENTO <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_provincia_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_distrito_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row ">
                                <input type="text" class="form-control" id="txt_iddireccion" hidden />
                                <label class="col-3" for="">VIA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_via_editar" />
                                    </div>
                                </div>

                            </div>




                            <div class="form-group row">
                                <label class="col-3" for="inputName">RESIDENCIA ACTUAL <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_provincia2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_distrito2_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">REFERENCIA DE DIRECCION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_referencia_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputEmail">EMAIL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="email" class="form-control" id="txt_email_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL TITULAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_titular_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL CONTACTO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_contacto_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL PARA COORDINAR INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_coordinacion_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_iddetallepre" hidden />
                                <label class="col-3" for="inputName">PLANO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plano_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="">PRODUCTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" id="producto_editar" style="width:100%">
                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PROMOCION ESPECIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_promocion_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PLAN</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plan_editar" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">CARGO FIJO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_cargo_fijo_editar" onkeypress="return soloNumeros(event)" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">COSTO DE INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_costo_intalacion_editar" onkeypress="return soloNumeros(event)" />


                                    </div>
                                </div>

                            </div>
                            <div class="form-group row" hidden>
                                <label class="col-3" for="inputName">ASESOR COMERCIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_asesor_comercial_editar" />
                                        <input type="text" class="form-control" id="txt_idusuario_editar" hidden />

                                    </div>
                                </div>



                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FULL CLARO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_fullclaro_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">¿Cuenta o conto con servicio CLARO HOGAR en la misma direccion?</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_pregunta_editar" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">TIENE BANCA MOVIL</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_bancamovil_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">DIA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_diapago_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="time" class="form-control" id="txt_horapago_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">

                                <div class="col-9">
                                    <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;">

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row" hidden>
                                <label class="col-3" for="inputName">COMENTARIOS</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <textarea id="txt_comentario" cols="30" rows="5" class="form-control"></textarea>

                                    </div>
                                </div>

                            </div>




                        </div>
                    </form>
                </div>
                <!-- Modal Footer -->
                <div class="ibox-footer" style="text-align: center;">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                    <div class="" id="div_botoneditar">
                        <!-- <button type="button" class="btn btn-primary submitBtn" onclick="Editar_Venta()">Actualizar</button> -->
                    </div>
                </div>
            </div>
        </div>
    </div>
<?php } ?>

<?php if ($_SESSION['S_ROL'] == "BACK OFFICE") { ?>
    <div class="row">
        <div class="col-md-12">
            <div class="ibox ibox-default">
                <div class="ibox-head">
                    <div class="ibox-title">EDITAR VENTA</div>
                    <div class="ibox-tools">
                        <button class="btn btn-danger" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                        <!-- <button class="btn btn-danger" onclick="mostrardatoseditar()">Editar</button> -->
                    </div>
                </div>
                <div class="ibox-body">
                    <p class="statusMsg"></p>
                    <form autocomplete="false" onsubmit="return false">
                        <div class="col-lg-11">
                            <?php
                            $rol = $_SESSION['S_ROL']; ?>
                            <div class="form-group col-lg-12" hidden>
                                <input type="text" id="txt_idventa">
                                <label for="inputName">Rol Usuario</label>
                                <input type="text" class="form-control" id="txt_rol" value="<?= $rol ?>" />
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">VA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_va" onchange="ChangeColor(this.value)">
                                            <option value="SIN ELEGIR">SIN ELEGIR</option>
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">SOT</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_sot" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">SET</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_set" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">ESTADO DE VENTA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_estatus">
                                            <option value="OBSERVADO">OBSERVADO</option>
                                            <option value="ADJUNTAR DOC.">ADJUNTAR DOC.</option>
                                            <option value="APROBADO">APROBADO</option>
                                            <option value="P. GENE. SOT">P. GENE. SOT</option>
                                            <option value="EJECUCION">EJECUCION</option>
                                            <option value="AGENDADO">AGENDADO</option>
                                            <option value="REASIGNA. P.">REASIGNA. P.</option>
                                            <option value="P. VALIDAR">P. VALIDAR</option>
                                            <option value="ATENDIDA">ATENDIDA</option>
                                            <option value="RECHAZADO">RECHAZADO</option>
                                            <option value="ENVIADO">ENVIADO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">COMENTARIO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_comentario_estado" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">

                                <label class="col-3" for="inputName">FECHA DE INSTALACION</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fecha_instalacion" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row" id="date_5">
                                <label class="font-normal col-3">HORA DE INSTALACION</label>
                                <div class="input-daterange input-group col-6" id="datepicker">
                                    <input id="hora_inicio_instalacion" class="input-sm form-control" type="time" name="start">
                                    <span class="input-group-addon p-l-10 p-r-10">to</span>
                                    <input id="hora_fin_instalacion" class="input-sm form-control" type="time" name="end">
                                </div>
                            </div>

                            <div class="form-group row">
                                <div class="col-9">
                                    <strong>
                                        <hr>
                                    </strong>
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="text" id="txt_idventa" hidden>
                                <label class="col-3" for="inputName">MOVIL DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nrollamada_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_idgrabacion" hidden />
                                <label class="col-3" for="inputName">FECHA DE LLAMADA</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_fechallamada_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE LLAMADA</label>
                                <div class="col-6">
                                    <input type="time" class="form-control" id="txt_horallamada_editar" />
                                </div>

                            </div>

                            <div class="form-group row">
                                <input type="text" id="txt_cliente" hidden>
                                <label class="col-3" for="inputName">TIPO DE DOCUMENTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="cbm_tdocumento_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="DNI">DNI</option>
                                            <option value="RUC">RUC</option>
                                            <option value="PASAPORTE">PASAPORTE</option>
                                        </select>
                                    </div>

                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">NUMERO DE DOCUMENTO</label>

                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" id="txt_nrodocumento_editar_actual" onkeypress="return soloNumeros(event)" hidden>
                                        <input class="form-control" type="text" id="txt_nrodocumento_editar_nuevo" onkeypress="return soloNumeros(event)">
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">CLIENTE</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <input type="text" class="form-control" id="txt_nombre_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FIJO A PORTAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_fijoportar_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE PAPA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primernombrepapa_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PRIMER NOMBRE DE MAMA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_primenombremama_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">FECHA DE NACIMIENTO</label>
                                <div class="col-6">
                                    <div class="input-group date" style="width: 100%;">
                                        <div role="wrapper" class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                            <span class="input-group-addon bg-white"><i class="fa fa-calendar"></i></span>
                                            <input id="txt_fecnacimiento_editar" type="date" required="" class="form-control inp" data-type="datepicker" data-guid="b551be5c-8fa9-3b41-178a-d23e567b5344" data-datepicker="true" role="input">
                                        </div>
                                    </div>


                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">SIGNO DEL ZODIACO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" style="width: 100%;" id="txt_sodiaco_editar">
                                            <option value disabled="disabled" selected="selected">Elegir una opcion</option>
                                            <option value="ARIES">ARIES</option>
                                            <option value="TAURO">TAURO</option>
                                            <option value="GEMINIS">GEMINIS</option>
                                            <option value="CANCER">CANCER</option>
                                            <option value="LEO">LEO</option>
                                            <option value="VIRGO">VIRGO</option>
                                            <option value="LIBRA">LIBRA</option>
                                            <option value="ESCORPIO">ESCORPIO</option>
                                            <option value="SAGITARIO">SAGITARIO</option>
                                            <option value="CAPRICORNIO">CAPRICORNIO</option>
                                            <option value="ACUARIO">ACUARIO</option>
                                            <option value="PISCIS">PISCIS</option>
                                        </select>
                                    </div>

                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">LUGAR DE NACIMIENTO <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_provincia_editar" style="width:100%">
                                        </select>



                                        <select class="form-control inp col-4" id="sel_distrito_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row ">
                                <input type="text" class="form-control" id="txt_iddireccion" hidden />
                                <label class="col-3" for="">VIA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_via_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row ">
                                <label class="col-3" for="">NOMBRE DE VIA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_nombre_via_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row ">
                                <label class="col-3" for="">IDENTIFICACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_identificacion_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row ">
                                <label class="col-3" for="">INTERIOR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_interior_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="">ZONA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_zona_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row ">
                                <label class="col-3" for="">NOMBRE DE ZONA</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_nombre_zona_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">RESIDENCIA ACTUAL <br>(Departamento/Provincia/Distrito)</label>
                                <div class="col-6">
                                    <div class="input-group">

                                        <select class="form-control inp col-4" id="sel_departamento2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_provincia2_editar" style="width:100%">
                                        </select>

                                        <select class="form-control inp col-4" id="sel_distrito2_editar" style="width:100%">
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">REFERENCIA DE DIRECCION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_referencia_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputEmail">EMAIL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="email" class="form-control" id="txt_email_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL TITULAR</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_titular_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL CONTACTO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_contacto_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">MOVIL PARA COORDINAR INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_movil_coordinacion_editar" onkeypress="return soloNumeros(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <input type="text" class="form-control" id="txt_iddetallepre" hidden />
                                <label class="col-3" for="inputName">PLANO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plano_editar" onkeypress="return sololetras(event)" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="">PRODUCTO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">
                                        <select class="form-control inp" id="producto_editar" style="width:100%">
                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PROMOCION ESPECIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_promocion_editar" />
                                    </div>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">PLAN</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_plan_editar" />
                                    </div>
                                </div>

                            </div>

                            <div class="form-group row">
                                <label class="col-3" for="inputName">CARGO FIJO</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_cargo_fijo_editar" onkeypress="return soloNumeros(event)" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">COSTO DE INSTALACION</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_costo_intalacion_editar" onkeypress="return soloNumeros(event)" />


                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">ASESOR COMERCIAL</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_asesor_comercial_editar" />
                                        <input type="text" class="form-control" id="txt_idusuario_editar" hidden />

                                    </div>
                                </div>



                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">FULL CLARO</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="cbm_fullclaro_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>


                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">¿Cuenta o conto con servicio CLARO HOGAR en la misma direccion?</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <input type="text" class="form-control" id="txt_pregunta_editar" />

                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">TIENE BANCA MOVIL</label>
                                <div class="col-6">
                                    <div class="input-group" style="width: 100%;">

                                        <select class="form-control inp" style="width: 100%;" id="txt_bancamovil_editar">
                                            <option value="SI">SI</option>
                                            <option value="NO">NO</option>

                                        </select>

                                    </div>

                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">DIA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="date" class="form-control" id="txt_diapago_editar" />
                                    </div>
                                </div>

                            </div>
                            <div class="form-group row">
                                <label class="col-3" for="inputName">HORA DE PAGO</label>
                                <div class="col-6">
                                    <div class="gj-datepicker gj-datepicker-bootstrap gj-unselectable input-group">
                                        <input type="time" class="form-control" id="txt_horapago_editar" />
                                    </div>
                                </div>

                            </div>


                            <div class="form-group row">
                                <label class="col-3" for="inputName">COMENTARIOS</label>
                                <div class="col-6" style="width: 100%;">
                                    <div class="input-group" style="width: 100%;">

                                        <textarea id="txt_comentario" cols="30" rows="5" class="form-control"></textarea>

                                    </div>
                                </div>

                            </div>
                            

                            <div class="form-group row" hidden>

                                <div class="col-9">
                                    <div class="alert alert-danger alert-bordered" id="div_error" style="display: none;">

                                    </div>
                                </div>

                            </div>


                        </div>
                    </form>
                </div>
                <!-- Modal Footer -->
                <div class="ibox-footer" style="text-align: center;">
                    <button type="button" class="btn btn-default" data-dismiss="modal" onclick="cargar_contenido('contenido_principal','venta/vista_mantenimiento_venta.php')">Regresar</button>
                    <button type="button" class="btn btn-primary submitBtn" onclick="Editar_Venta()">Actualizar</button>
                </div>
            </div>
        </div>
    </div>
<?php } ?>


<script type="text/javascript" src="../../js/console_ubigeo.js?rev=<?php echo time(); ?>"></script>
<script type="text/javascript" src="../../js/console_lista_ubigeo.js?rev=<?php echo time(); ?>"></script>
<script>
    // In your Javascript (external .js resource or <script> tag)
    $(document).ready(function() {
        $('.js-example-basic-single').select2();


        listar_departamento_editar_nac();
        listar_departamento_editar_loc()
        listar_pronvincia_editar_nac();
        listar_pronvincia2_editar_loc();
        listar_distrito_editar_nac();
        listar_distrito_editar_loc();

        listar_producto();


    });

    setTimeout(mostrardatoseditar, 300);

    function cargo() {
        $("#sel_departamento_editar").change(function() {
            var iddepartamento = $("#sel_departamento_editar").val();
            listar_pronvincia(iddepartamento);
        })
        $("#sel_departamento2_editar").change(function() {
            var iddepartamento2 = $("#sel_departamento2_editar").val();
            listar_pronvincia2(iddepartamento2);
        })

        $("#sel_provincia_editar").change(function() {
            var idprovincia = $("#sel_provincia_editar").val();
            listar_distrito(idprovincia);
        })
        $("#sel_provincia2_editar").change(function() {
            var idprovincia2 = $("#sel_provincia2_editar").val();
            listar_distrito2(idprovincia2);
        })
    }

    setTimeout(cargo, 2000);
</script>