<?php
    require '../../modelo/modelo_persona.php';

    $MP = new Modelo_Persona();

    $error="";
    $contador=0;
    $nombre = htmlspecialchars(mb_strtoupper($_POST['n']),ENT_QUOTES,'UTF-8');
    $apepat = htmlspecialchars(mb_strtoupper($_POST['apt']),ENT_QUOTES,'UTF-8');
    $apemat = htmlspecialchars(mb_strtoupper($_POST['apm']),ENT_QUOTES,'UTF-8');
    $nrodocumento = htmlspecialchars(mb_strtoupper($_POST['nro']),ENT_QUOTES,'UTF-8');
    $tipdocumento = htmlspecialchars(mb_strtoupper($_POST['tip']),ENT_QUOTES,'UTF-8');
    $sexo = htmlspecialchars(mb_strtoupper($_POST['sex']),ENT_QUOTES,'UTF-8');
    $telefono = htmlspecialchars(mb_strtoupper($_POST['tel']),ENT_QUOTES,'UTF-8');
    $username = htmlspecialchars($_POST['username'],ENT_QUOTES,'UTF-8');
    $email = htmlspecialchars(mb_strtoupper($_POST['email']),ENT_QUOTES,'UTF-8');
    $idrol = htmlspecialchars($_POST['idrol'],ENT_QUOTES,'UTF-8');
    // $password = password_hash($_POST['password'],PASSWORD_DEFAULT,['cost'=>10]);

    $fechanacimiento = htmlspecialchars(mb_strtoupper($_POST['fechanacimiento']),ENT_QUOTES,'UTF-8');
    $fechacontratacion = htmlspecialchars(mb_strtoupper($_POST['fechacontratacion']),ENT_QUOTES,'UTF-8');
    $fechaingreso = htmlspecialchars(mb_strtoupper($_POST['fechaingreso']),ENT_QUOTES,'UTF-8');
    $banco = htmlspecialchars(mb_strtoupper($_POST['banco']),ENT_QUOTES,'UTF-8');
    $nrocuentaahorros = htmlspecialchars(mb_strtoupper($_POST['nrocuentaahorros']),ENT_QUOTES,'UTF-8');
    $codigointerbancario = htmlspecialchars(mb_strtoupper($_POST['codigointerbancario']),ENT_QUOTES,'UTF-8');
    $tipopago = htmlspecialchars(mb_strtoupper($_POST['tipopago']),ENT_QUOTES,'UTF-8');
    $esquemapago = htmlspecialchars(mb_strtoupper($_POST['esquemapago']),ENT_QUOTES,'UTF-8');
    $ruc = htmlspecialchars(mb_strtoupper($_POST['ruc']),ENT_QUOTES,'UTF-8');
    $clavesol1 = htmlspecialchars($_POST['clavesol1'],ENT_QUOTES,'UTF-8');
    $clavesol2 = htmlspecialchars($_POST['clavesol2'],ENT_QUOTES,'UTF-8');
    $personaresponsable = htmlspecialchars(mb_strtoupper($_POST['personaresponsable']),ENT_QUOTES,'UTF-8');
    $zona = htmlspecialchars(mb_strtoupper($_POST['zona']),ENT_QUOTES,'UTF-8');

    
    if (!preg_match("/^(?!-+)[a-zA-Z-ñáéíóúÁÉÍÓÚ\s]*$/",$nombre)) {
        $contador++;
        $error.="El nombre debe contener solo letras<br>"; 
    }
    if (!preg_match("/^(?!-+)[a-zA-Z-ñáéíóúÁÉÍÓÚ\s]*$/",$apepat)) {
        $contador++;
        $error.="El apellido paterno debe contener solo letras"; 
    }
    if (!preg_match("/^(?!-+)[a-zA-Z-ñáéíóúÁÉÍÓÚ\s]*$/",$apemat)) {
        $contador++;
        $error.="El apellido materno debe contener solo letras"; 
    }
    if (!preg_match("/^[[:digit:]\s]*$/",$nrodocumento)) {
        $contador++;
        $error.="El n&uacute;mero del documento debe contener solo n&uacute;meros.<br>"; 
    }
    if (!preg_match("/^[[:digit:]\s]*$/",$telefono)) {
        $contador++;
        $error.="El n&uacute;mero del telefono debe contener solo n&uacute;meros.<br>";  
    }
    if ($contador>0) {
        echo $error;
    }else{
        $consulta = $MP->Registrar_Persona($nombre,$apepat,$apemat,$nrodocumento,$tipdocumento,$sexo,$telefono,$username,$email,$idrol,$fechanacimiento,$fechacontratacion,$fechaingreso,$banco,$nrocuentaahorros,$codigointerbancario,$tipopago,$esquemapago,$ruc,$clavesol1,$clavesol2,$personaresponsable,$zona);
        echo $consulta;
    }
