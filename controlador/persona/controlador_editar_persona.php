<?php
    require '../../modelo/modelo_persona.php';

    $MP = new Modelo_Persona();

    $error="";
    $contador=0;
    $idpersona = htmlspecialchars(mb_strtoupper($_POST['idpersona']),ENT_QUOTES,'UTF-8');
    $idpersona_pago = htmlspecialchars(mb_strtoupper($_POST['idpersona_pago']),ENT_QUOTES,'UTF-8');
    $idusuario = htmlspecialchars(mb_strtoupper($_POST['idusuario']),ENT_QUOTES,'UTF-8');
    $nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']),ENT_QUOTES,'UTF-8');
    $apepat = htmlspecialchars(mb_strtoupper($_POST['apepat']),ENT_QUOTES,'UTF-8');
    $apemat = htmlspecialchars(mb_strtoupper($_POST['apemat']),ENT_QUOTES,'UTF-8');
    $nrodocumentoactual = htmlspecialchars(mb_strtoupper($_POST['nrodocumentoactual']),ENT_QUOTES,'UTF-8');
    $nrodocumentonuevo = htmlspecialchars(mb_strtoupper($_POST['nrodocumentonuevo']),ENT_QUOTES,'UTF-8');
    $telefono = htmlspecialchars(mb_strtoupper($_POST['telefono']),ENT_QUOTES,'UTF-8');
    $tipdocumento = htmlspecialchars(mb_strtoupper($_POST['tipdocumento']),ENT_QUOTES,'UTF-8');
    $sexo = htmlspecialchars(mb_strtoupper($_POST['sexo']),ENT_QUOTES,'UTF-8');
    $estado = htmlspecialchars(mb_strtoupper($_POST['estado']),ENT_QUOTES,'UTF-8');
    $fecnacimiento = htmlspecialchars(mb_strtoupper($_POST['fecnacimiento']),ENT_QUOTES,'UTF-8');
    $feccontratacion = htmlspecialchars(mb_strtoupper($_POST['feccontratacion']),ENT_QUOTES,'UTF-8');
    $fecingreso = htmlspecialchars(mb_strtoupper($_POST['fecingreso']),ENT_QUOTES,'UTF-8');
    $fecsalida = htmlspecialchars(mb_strtoupper($_POST['fecsalida']),ENT_QUOTES,'UTF-8');
    $motivosalida = htmlspecialchars(mb_strtoupper($_POST['motivosalida']),ENT_QUOTES,'UTF-8');
    $fecfulltime = htmlspecialchars(mb_strtoupper($_POST['fecfulltime']),ENT_QUOTES,'UTF-8');
    $banco = htmlspecialchars(mb_strtoupper($_POST['banco']),ENT_QUOTES,'UTF-8');
    $cuentaahorro = htmlspecialchars(mb_strtoupper($_POST['cuentaahorro']),ENT_QUOTES,'UTF-8');
    $codigointerban = htmlspecialchars(mb_strtoupper($_POST['codigointerban']),ENT_QUOTES,'UTF-8');
    $tipopago = htmlspecialchars(mb_strtoupper($_POST['tipopago']),ENT_QUOTES,'UTF-8');
    $esquemapago = htmlspecialchars(mb_strtoupper($_POST['esquemapago']),ENT_QUOTES,'UTF-8');
    $ruc = htmlspecialchars(mb_strtoupper($_POST['ruc']),ENT_QUOTES,'UTF-8');
    $clavesol1actual = htmlspecialchars($_POST['clavesol1actual'],ENT_QUOTES,'UTF-8');
    $clavesol1nuevo = htmlspecialchars($_POST['clavesol1nuevo'],ENT_QUOTES,'UTF-8');
    $clavesol2actual = htmlspecialchars($_POST['clavesol2actual'],ENT_QUOTES,'UTF-8');
    $clavesol2nuevo = htmlspecialchars($_POST['clavesol2nuevo'],ENT_QUOTES,'UTF-8');
    $emailactual = htmlspecialchars(mb_strtoupper($_POST['emailactual']),ENT_QUOTES,'UTF-8');
    $emailnuevo = htmlspecialchars(mb_strtoupper($_POST['emailnuevo']),ENT_QUOTES,'UTF-8');
    $rol = htmlspecialchars(mb_strtoupper($_POST['rol']),ENT_QUOTES,'UTF-8');
    $responsable = htmlspecialchars(mb_strtoupper($_POST['responsable']),ENT_QUOTES,'UTF-8');
    $zona = htmlspecialchars(mb_strtoupper($_POST['zona']),ENT_QUOTES,'UTF-8');

    if(isset($responsable)){
        $responsable = 0;
    
    }

     $consulta = $MP->Editar_Persona($idpersona,$idpersona_pago,$idusuario,$nombre,$apepat,$apemat,$nrodocumentoactual,$nrodocumentonuevo,$telefono,$tipdocumento,$sexo,$estado,$fecnacimiento,$feccontratacion,$fecingreso,$fecsalida,$motivosalida,$fecfulltime,$banco,$cuentaahorro,$codigointerban,$tipopago,$esquemapago,$ruc,$clavesol1actual,$clavesol1nuevo,$clavesol2actual,$clavesol2nuevo,$emailactual,$emailnuevo,$rol,$responsable,$zona);
        echo $consulta;
    
