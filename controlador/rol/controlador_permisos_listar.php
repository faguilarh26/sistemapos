<?php
    require '../../modelo/modelo_rol.php';

    $MR = new Modelo_Rol();
    $id = htmlspecialchars($_POST['variable'],ENT_QUOTES,'UTF-8');
    $consulta = $MR->Listar_Permisos($id);
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
		    "sEcho": 1,
		    "iTotalRecords": "0",
		    "iTotalDisplayRecords": "0",
		    "aaData": []
		}';
    }
?>