<?php
    require '../../modelo/modelo_producto.php';

    $MP = new Modelo_Producto();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $productoactual = htmlspecialchars(mb_strtoupper($_POST['productoactual']),ENT_QUOTES,'UTF-8');
    $productonuevo = htmlspecialchars(mb_strtoupper($_POST['productonuevo']),ENT_QUOTES,'UTF-8');
    $estatus = htmlspecialchars($_POST['estatus'],ENT_QUOTES,'UTF-8');
    $consulta = $MP->Modificar_Producto($id,$productoactual,$productonuevo,$estatus);
    echo $consulta;
    
?>
