<?php
      require '../../modelo/modelo_ubigeo.php';
    $MU = new Modelo_Ubigeo();
    $va = htmlspecialchars($_POST['va'],ENT_QUOTES,'UTF-8');
    $sot = htmlspecialchars($_POST['sot'],ENT_QUOTES,'UTF-8');
    $set = htmlspecialchars($_POST['set'],ENT_QUOTES,'UTF-8');
    $comentario_estado = htmlspecialchars($_POST['comentario_estado'],ENT_QUOTES,'UTF-8');
    $comentario_estado_venta = htmlspecialchars($_POST['comentario_estado_venta'],ENT_QUOTES,'UTF-8');
    $fecha_instalacion = htmlspecialchars($_POST['fecha_instalacion'],ENT_QUOTES,'UTF-8');
    $hor_iniciopro = htmlspecialchars($_POST['hor_iniciopro'],ENT_QUOTES,'UTF-8');
    $hor_fin_pro = htmlspecialchars($_POST['hor_fin_pro'],ENT_QUOTES,'UTF-8');

    $estado = htmlspecialchars($_POST['estado'],ENT_QUOTES,'UTF-8');
    $idventa = htmlspecialchars(mb_strtoupper($_POST['idventa']),ENT_QUOTES,'UTF-8');
    $idcliente = htmlspecialchars(mb_strtoupper($_POST['idcliente']),ENT_QUOTES,'UTF-8');
    $iddireccion = htmlspecialchars(mb_strtoupper($_POST['iddireccion']),ENT_QUOTES,'UTF-8');
    $iddetallepre = htmlspecialchars(mb_strtoupper($_POST['iddetallepre']),ENT_QUOTES,'UTF-8');
    $idgrabacion = htmlspecialchars(mb_strtoupper($_POST['idgrabacion']),ENT_QUOTES,'UTF-8');
    $movilllamada = htmlspecialchars($_POST['movilllamada'],ENT_QUOTES,'UTF-8');
    $fechallamada = htmlspecialchars($_POST['fechallamada'],ENT_QUOTES,'UTF-8');
    $horallamada = htmlspecialchars($_POST['horallamada'],ENT_QUOTES,'UTF-8');
    $tipodocumento = htmlspecialchars(mb_strtoupper($_POST['tipodocumento']),ENT_QUOTES,'UTF-8');
    $nrodocumentoactual = htmlspecialchars($_POST['nrodocumentoactual'],ENT_QUOTES,'UTF-8');
    $nrodocumentonuevo = htmlspecialchars($_POST['nrodocumentonuevo'],ENT_QUOTES,'UTF-8');
    $nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']),ENT_QUOTES,'UTF-8');
    $fijoportar = htmlspecialchars($_POST['fijoportar'],ENT_QUOTES,'UTF-8');
    $prinompapa = htmlspecialchars(mb_strtoupper($_POST['prinompapa']),ENT_QUOTES,'UTF-8');
    $prinommama = htmlspecialchars(mb_strtoupper($_POST['prinommama']),ENT_QUOTES,'UTF-8');
    $fechanacimiento = htmlspecialchars($_POST['fechanacimiento'],ENT_QUOTES,'UTF-8');
    $signosodiacal = htmlspecialchars(mb_strtoupper($_POST['signosodiacal']),ENT_QUOTES,'UTF-8');
    $distritonac = htmlspecialchars(mb_strtoupper($_POST['distritonac']),ENT_QUOTES,'UTF-8');
    $via = htmlspecialchars(mb_strtoupper($_POST['via']),ENT_QUOTES,'UTF-8');
    $distritoact = htmlspecialchars(mb_strtoupper($_POST['distritoact']),ENT_QUOTES,'UTF-8');
    $referencia = htmlspecialchars(mb_strtoupper($_POST['referencia']),ENT_QUOTES,'UTF-8');
    $email = htmlspecialchars(mb_strtoupper($_POST['email']),ENT_QUOTES,'UTF-8');
    $moviltitular = htmlspecialchars($_POST['moviltitular'],ENT_QUOTES,'UTF-8');
    $movilcontacto = htmlspecialchars($_POST['movilcontacto'],ENT_QUOTES,'UTF-8');
    $movilcoordinacion = htmlspecialchars($_POST['movilcoordinacion'],ENT_QUOTES,'UTF-8');
    $plano = htmlspecialchars(mb_strtoupper($_POST['plano']),ENT_QUOTES,'UTF-8');
    $producto = htmlspecialchars(mb_strtoupper($_POST['producto']),ENT_QUOTES,'UTF-8');
    $plan = htmlspecialchars($_POST['plan'],ENT_QUOTES,'UTF-8');
    $cargofijo = htmlspecialchars($_POST['cargofijo'],ENT_QUOTES,'UTF-8');
    $costoinstalacion = htmlspecialchars($_POST['costoinstalacion'],ENT_QUOTES,'UTF-8');
    $idusuario = htmlspecialchars(mb_strtoupper($_POST['idusuario']),ENT_QUOTES,'UTF-8');
    $fullclaro = htmlspecialchars(mb_strtoupper($_POST['fullclaro']),ENT_QUOTES,'UTF-8');
    $pregunta = htmlspecialchars(mb_strtoupper($_POST['pregunta']),ENT_QUOTES,'UTF-8');
    $bancamovil = htmlspecialchars(mb_strtoupper($_POST['bancamovil']),ENT_QUOTES,'UTF-8');
    $diapago = htmlspecialchars($_POST['diapago'],ENT_QUOTES,'UTF-8');
    $horapago = htmlspecialchars($_POST['horapago'],ENT_QUOTES,'UTF-8');
    $promocion = htmlspecialchars($_POST['promocion'],ENT_QUOTES,'UTF-8');
    $comentario = htmlspecialchars($_POST['comentario'],ENT_QUOTES,'UTF-8');
    $rol = htmlspecialchars($_POST['rol'],ENT_QUOTES,'UTF-8');

    $tipoventa = htmlspecialchars($_POST['tipoventa'],ENT_QUOTES,'UTF-8');
    $play = htmlspecialchars($_POST['play'],ENT_QUOTES,'UTF-8');
    $paquete = htmlspecialchars($_POST['paquete'],ENT_QUOTES,'UTF-8');
//     $adicional = htmlspecialchars($_POST['adicional'],ENT_QUOTES,'UTF-8');
    $decoadd1 = htmlspecialchars($_POST['decoadd1'],ENT_QUOTES,'UTF-8');
//     $decoadd2 = htmlspecialchars($_POST['decoadd2'],ENT_QUOTES,'UTF-8');
    $premium = htmlspecialchars($_POST['premium'],ENT_QUOTES,'UTF-8');
    $cargoadd = htmlspecialchars($_POST['cargoadd'],ENT_QUOTES,'UTF-8');
    $cargototal = htmlspecialchars($_POST['cargototal'],ENT_QUOTES,'UTF-8');



    
    if ((empty($diapago)) && (empty($horapago)) ) {
           $consulta = $MU->Editar_Venta_Con_Pago($idusuario,$estado,$idventa,$idcliente,$iddireccion,$iddetallepre,$idgrabacion,$movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumentoactual,$nrodocumentonuevo,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$fullclaro,$pregunta,$bancamovil,$diapago,$horapago,$promocion,$comentario,$rol,$va,$sot,$set,$comentario_estado,$fecha_instalacion,$hor_iniciopro,$hor_fin_pro,$comentario_estado_venta,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal);  
    }else{
            $consulta = $MU->Editar_Venta_Con_Pago($idusuario,$estado,$idventa,$idcliente,$iddireccion,$iddetallepre,$idgrabacion,$movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumentoactual,$nrodocumentonuevo,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$fullclaro,$pregunta,$bancamovil,$diapago,$horapago,$promocion,$comentario,$rol,$va,$sot,$set,$comentario_estado,$fecha_instalacion,$hor_iniciopro,$hor_fin_pro,$comentario_estado_venta,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal); 
    }

    
        
    
        echo $consulta;
    

  
    
    
?>