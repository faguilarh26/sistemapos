<?php
      require '../../modelo/modelo_ubigeo.php';
    $MU = new Modelo_Ubigeo();
    $movilllamada = htmlspecialchars($_POST['movilllamada'],ENT_QUOTES,'UTF-8');
    $fechallamada = htmlspecialchars($_POST['fechallamada'],ENT_QUOTES,'UTF-8');
    $horallamada = htmlspecialchars($_POST['horallamada'],ENT_QUOTES,'UTF-8');
    $tipodocumento = htmlspecialchars(mb_strtoupper($_POST['tipodocumento']),ENT_QUOTES,'UTF-8');
    $nrodocumento = htmlspecialchars($_POST['nrodocumento'],ENT_QUOTES,'UTF-8');
    $nombre = htmlspecialchars(mb_strtoupper($_POST['nombre']),ENT_QUOTES,'UTF-8');
    $fijoportar = htmlspecialchars($_POST['fijoportar'],ENT_QUOTES,'UTF-8');
    $prinompapa = htmlspecialchars(mb_strtoupper($_POST['prinompapa']),ENT_QUOTES,'UTF-8');
    $prinommama = htmlspecialchars(mb_strtoupper($_POST['prinommama']),ENT_QUOTES,'UTF-8');
    $fechanacimiento = htmlspecialchars($_POST['fechanacimiento'],ENT_QUOTES,'UTF-8');
    $signosodiacal = htmlspecialchars(mb_strtoupper($_POST['signosodiacal']),ENT_QUOTES,'UTF-8');
    $distritonac = htmlspecialchars(mb_strtoupper($_POST['distritonac']),ENT_QUOTES,'UTF-8');
    $via = htmlspecialchars(mb_strtoupper($_POST['via']),ENT_QUOTES,'UTF-8');
    $distritoact = htmlspecialchars(mb_strtoupper($_POST['distritoact']),ENT_QUOTES,'UTF-8');
    $referencia = htmlspecialchars(mb_strtoupper($_POST['referencia']),ENT_QUOTES,'UTF-8');
    $email = htmlspecialchars(mb_strtoupper($_POST['email']),ENT_QUOTES,'UTF-8');
    $moviltitular = htmlspecialchars($_POST['moviltitular'],ENT_QUOTES,'UTF-8');
    $movilcontacto = htmlspecialchars($_POST['movilcontacto'],ENT_QUOTES,'UTF-8');
    $movilcoordinacion = htmlspecialchars($_POST['movilcoordinacion'],ENT_QUOTES,'UTF-8');
    $plano = htmlspecialchars(mb_strtoupper($_POST['plano']),ENT_QUOTES,'UTF-8');
    $producto = htmlspecialchars(mb_strtoupper($_POST['producto']),ENT_QUOTES,'UTF-8');
    $plan = htmlspecialchars(mb_strtoupper($_POST['plan']),ENT_QUOTES,'UTF-8');
    $cargofijo = htmlspecialchars($_POST['cargofijo'],ENT_QUOTES,'UTF-8');
    $costoinstalacion = htmlspecialchars($_POST['costoinstalacion'],ENT_QUOTES,'UTF-8');
    $idusuario = htmlspecialchars(mb_strtoupper($_POST['idusuario']),ENT_QUOTES,'UTF-8');
    $fullclaro = htmlspecialchars(mb_strtoupper($_POST['fullclaro']),ENT_QUOTES,'UTF-8');
    $pregunta = htmlspecialchars(mb_strtoupper($_POST['pregunta']),ENT_QUOTES,'UTF-8');
    $bancamovil = htmlspecialchars(mb_strtoupper($_POST['bancamovil']),ENT_QUOTES,'UTF-8');
    $diapago = htmlspecialchars($_POST['diapago'],ENT_QUOTES,'UTF-8');
    $horapago = htmlspecialchars($_POST['horapago'],ENT_QUOTES,'UTF-8');
    $promocion = htmlspecialchars($_POST['promocion'],ENT_QUOTES,'UTF-8');

    $tipoventa = htmlspecialchars($_POST['tipoventa'],ENT_QUOTES,'UTF-8');
    $play = htmlspecialchars($_POST['play'],ENT_QUOTES,'UTF-8');
    $paquete = htmlspecialchars($_POST['paquete'],ENT_QUOTES,'UTF-8');
//     $adicional = htmlspecialchars($_POST['adicional'],ENT_QUOTES,'UTF-8');
    $decoadd1 = htmlspecialchars($_POST['decoadd1'],ENT_QUOTES,'UTF-8');
//     $decoadd2 = htmlspecialchars($_POST['decoadd2'],ENT_QUOTES,'UTF-8');
    $premium = htmlspecialchars($_POST['premium'],ENT_QUOTES,'UTF-8');
    $cargoadd = htmlspecialchars($_POST['cargoadd'],ENT_QUOTES,'UTF-8');
    $cargototal = htmlspecialchars($_POST['cargototal'],ENT_QUOTES,'UTF-8');


    
    if ((empty($diapago)) && (empty($horapago)) ) {
        
            

            $consulta = $MU->Registrar_Venta_Sin_Pago($movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumento,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$idusuario,$fullclaro,$pregunta,$bancamovil,$promocion,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal);
        
    }else{
            
            
            $consulta = $MU->Registrar_Venta_Con_Pago($movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumento,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$idusuario,$fullclaro,$pregunta,$bancamovil,$diapago,$horapago,$promocion,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal);
    }

    
        
    
        echo $consulta;
    

  
    
    
?>