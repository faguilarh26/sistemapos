<?php
    require '../../modelo/modelo_ubigeo.php';
    $MU = new Modelo_Ubigeo();
    $rol = htmlspecialchars($_POST['rol'],ENT_QUOTES,'UTF-8');
    $idusuario = htmlspecialchars($_POST['idusuario'],ENT_QUOTES,'UTF-8');
    $fecha_inicio = htmlspecialchars($_POST['fechainicio'],ENT_QUOTES,'UTF-8');
    $fecha_fin = htmlspecialchars($_POST['fechafin'],ENT_QUOTES,'UTF-8');
    $consulta = $MU->Listar_Venta($rol,$idusuario,$fecha_inicio,$fecha_fin);
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
		    "sEcho": 1,
		    "iTotalRecords": "0",
		    "iTotalDisplayRecords": "0",
		    "aaData": []
		}';
    }
?>