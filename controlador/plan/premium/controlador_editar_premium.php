<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $premiumactual = htmlspecialchars($_POST['premiumactual'],ENT_QUOTES,'UTF-8');
    $premiumnuevo = htmlspecialchars($_POST['premiumnuevo'],ENT_QUOTES,'UTF-8');
    $precio = htmlspecialchars($_POST['precio'],ENT_QUOTES,'UTF-8');
    
    $consulta = $MP->Modificar_Premium($id,$premiumactual,$premiumnuevo,$precio);
    echo $consulta;
    
?>