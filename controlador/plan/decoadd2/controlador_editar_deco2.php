<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $deco2actual = htmlspecialchars($_POST['deco2actual'],ENT_QUOTES,'UTF-8');
    $deco2nuevo = htmlspecialchars($_POST['deco2nuevo'],ENT_QUOTES,'UTF-8');
    $precio = htmlspecialchars($_POST['precio'],ENT_QUOTES,'UTF-8');
    
    $consulta = $MP->Modificar_Deco2($id,$deco2actual,$deco2nuevo,$precio);
    echo $consulta;
    
?>