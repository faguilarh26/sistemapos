<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $deco1actual = htmlspecialchars($_POST['deco1actual'],ENT_QUOTES,'UTF-8');
    $deco1nuevo = htmlspecialchars($_POST['deco1nuevo'],ENT_QUOTES,'UTF-8');
    $precio = htmlspecialchars($_POST['precio'],ENT_QUOTES,'UTF-8');
    
    $consulta = $MP->Modificar_Deco1($id,$deco1actual,$deco1nuevo,$precio);
    echo $consulta;
    
?>