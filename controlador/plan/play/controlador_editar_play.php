<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $playactual = htmlspecialchars(mb_strtoupper($_POST['playactual']),ENT_QUOTES,'UTF-8');
    $playnuevo = htmlspecialchars(mb_strtoupper($_POST['playnuevo']),ENT_QUOTES,'UTF-8');
    $estatus = htmlspecialchars($_POST['estatus'],ENT_QUOTES,'UTF-8');
    $consulta = $MP->Modificar_Play($id,$playactual,$playnuevo,$estatus);
    echo $consulta;
    
?>
