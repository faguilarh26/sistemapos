<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    
    $producto = htmlspecialchars($_POST['producto'],ENT_QUOTES,'UTF-8');
    $play= htmlspecialchars($_POST['play'],ENT_QUOTES,'UTF-8');
    $plan = htmlspecialchars($_POST['plan'],ENT_QUOTES,'UTF-8');
    $paquete = htmlspecialchars($_POST['paquete'],ENT_QUOTES,'UTF-8');
    $precio = htmlspecialchars($_POST['precio'],ENT_QUOTES,'UTF-8');
    $productonombre = htmlspecialchars($_POST['productonombre'],ENT_QUOTES,'UTF-8');
    $playnombre= htmlspecialchars($_POST['playnombre'],ENT_QUOTES,'UTF-8');
    $plannombre = htmlspecialchars($_POST['plannombre'],ENT_QUOTES,'UTF-8');

    $consulta = $MP->Registar_Paquete($producto,$play,$plan,$paquete,$precio,$productonombre,$playnombre,$plannombre);
    
    echo $consulta;
?>