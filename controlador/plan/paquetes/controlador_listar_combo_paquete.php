<?php
    require '../../../modelo/modelo_plan.php';
    $MP = new Modelo_Plan();
    $idproducto = htmlspecialchars($_POST['idproducto'],ENT_QUOTES,'UTF-8');
    $idplay = htmlspecialchars($_POST['idplay'],ENT_QUOTES,'UTF-8');
    $idplan = htmlspecialchars($_POST['idplan'],ENT_QUOTES,'UTF-8');
    $consulta = $MP->listar_combo_paquete($idproducto,$idplay,$idplan);
    echo json_encode($consulta);
?>