<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $producto = htmlspecialchars($_POST['producto'],ENT_QUOTES,'UTF-8');
    $play = htmlspecialchars($_POST['play'],ENT_QUOTES,'UTF-8');
    $plan = htmlspecialchars($_POST['plan'],ENT_QUOTES,'UTF-8');
    $paqueteactual = htmlspecialchars($_POST['paqueteactual'],ENT_QUOTES,'UTF-8');
    $paquetenuevo = htmlspecialchars($_POST['paquetenuevo'],ENT_QUOTES,'UTF-8');
    $precio = htmlspecialchars($_POST['precio'],ENT_QUOTES,'UTF-8');
    
    $consulta = $MP->Modificar_Paquete($id,$producto,$play,$plan,$paqueteactual,$paquetenuevo,$precio);
    echo $consulta;
    
?>