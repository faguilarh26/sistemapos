<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $play= htmlspecialchars($_POST['play'],ENT_QUOTES,'UTF-8');
    $plan = htmlspecialchars($_POST['plan'],ENT_QUOTES,'UTF-8');
    $producto = htmlspecialchars($_POST['producto'],ENT_QUOTES,'UTF-8');
    $consulta = $MP->Listar_plan_paquetes($play,$plan,$producto);
    if($consulta){
        echo json_encode($consulta);
    }else{
        echo '{
		    "sEcho": 1,
		    "iTotalRecords": "0",
		    "iTotalDisplayRecords": "0",
		    "aaData": []
		}';
    }
?>