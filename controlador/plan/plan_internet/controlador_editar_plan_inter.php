<?php
    require '../../../modelo/modelo_plan.php';

    $MP = new Modelo_Plan();
    $id = htmlspecialchars($_POST['id'],ENT_QUOTES,'UTF-8');
    $planinteractual = htmlspecialchars(mb_strtoupper($_POST['planinteractual']),ENT_QUOTES,'UTF-8');
    $planinternuevo = htmlspecialchars(mb_strtoupper($_POST['planinternuevo']),ENT_QUOTES,'UTF-8');
    $estatus = htmlspecialchars($_POST['estatus'],ENT_QUOTES,'UTF-8');
    $consulta = $MP->Modificar_Plan_Inter($id,$planinteractual,$planinternuevo,$estatus);
    echo $consulta;
    
?>