// funciones para el listado en la vista de registro de venta
function listar_departamento() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_departamento.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_departamento").html(cadena);
            // $("#sel_departamento2").html(cadena);
            var iddepartamento = $("#sel_departamento").val();
            listar_pronvincia(iddepartamento);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_departamento").html(cadena);
        }
    })
}

function listar_departamento2() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_departamento.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_departamento2").html(cadena);
            var iddepartamento = $("#sel_departamento2").val();
            listar_pronvincia2(iddepartamento);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_departamento2").html(cadena);
        }
    })
}

function listar_pronvincia(iddepartamento) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_provincia.php',
        type: 'POST',
        data: {
            iddepartamento: iddepartamento
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_provincia").html(cadena);
            
            var idprovincia = $("#sel_provincia").val();
           
            listar_distrito(idprovincia);
            
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia").html(cadena);
        }
    })
}
// inicio de prueba para nuevo ubigeo

function listar_pronvincia_editar(iddepartamento) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_provincia.php',
        type: 'POST',
        data: {
            iddepartamento: iddepartamento
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }

            $("#sel_provincia_editar").html(cadena);

            var idprovincia2 = $("#sel_provincia_editar").val();

            listar_distrito_editar_nuevo(idprovincia2);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia_editar").html(cadena);
        }
    })
}

// fin de prueba para nuevo ubigeo




function listar_pronvincia2(iddepartamento) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_provincia.php',
        type: 'POST',
        data: {
            iddepartamento: iddepartamento
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_provincia2").html(cadena);
            var idprovincia = $("#sel_provincia2").val();
            
            
            listar_distrito2(idprovincia);
            
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia2").html(cadena);
        }
    })
}

// inicio de prueba para nuevo ubigeo provincia

function listar_pronvincia2_editar(iddepartamento) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_provincia.php',
        type: 'POST',
        data: {
            iddepartamento: iddepartamento
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }

            $("#sel_provincia2_editar").html(cadena);
            var idprovincia2 = $("#sel_provincia2_editar").val();

            listar_distrito2_editar(idprovincia2);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia2_editar").html(cadena);
        }
    })
}

// fin de prueba para nuevo ubigeo provincia

function listar_distrito(idprovincia) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_distrito.php',
        type: 'POST',
        data: {
            idprovincia: idprovincia
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_distrito").html(cadena);
            
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito").html(cadena);
        }
    })
}


// incio de prueba distrito nuevo

function listar_distrito_editar_nuevo(idprovincia) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_distrito.php',
        type: 'POST',
        data: {
            idprovincia: idprovincia
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }

            $("#sel_distrito_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito_editar").html(cadena);
        }
    })
}
// fin de prueba distrito nuevo

function listar_distrito2(idprovincia) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_distrito.php',
        type: 'POST',
        data: {
            idprovincia: idprovincia
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_distrito2").html(cadena);
            
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito2").html(cadena);
        }
    })
}

function listar_distrito2_editar(idprovincia) {
    $.ajax({
        url: '../controlador/venta/controlador_listar_distrito.php',
        type: 'POST',
        data: {
            idprovincia: idprovincia
        }
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }

            $("#sel_distrito2_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito2_editar").html(cadena);
        }
    })
}

// funciones para el listado de datos en la vista de editar
function listar_departamento_editar_nac() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_departamento.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_departamento_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_departamento_editar").html(cadena);
        }
    })
}

function listar_departamento_editar_loc() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_departamento.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_departamento2_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_departamento2_editar").html(cadena);
        }
    })
}

function listar_pronvincia_editar_nac() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_solo_provincia.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_provincia_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia_editar").html(cadena);
        }
    })
}
function listar_pronvincia2_editar_loc() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_solo_provincia.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_provincia2_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_provincia2_editar").html(cadena);
        }
    })
}

function listar_distrito_editar_nac() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_solo_distrito.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_distrito_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito_editar").html(cadena);
        }
    })
}

function listar_distrito_editar_loc() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_solo_distrito.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#sel_distrito2_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#sel_distrito2_editar").html(cadena);
        }
    })
}

function listar_asesores_combo() {
    $.ajax({
        url: "../controlador/usuario/controlador_asesores_combo_listar.php",
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "";
        if (data.length > 0) {
            for (var index = 0; index < data.length; index++) {
                cadena += "<option value='" + data[index][0] + "'>" + data[index][1] + "</option>";

            }
            $("#cbm_idusuario_editar").html(cadena);

        } else {
            cadena += "<option value=''>No se encontraron datos</option>";
            $("#cbm_idusuario_editar").html(cadena);

        }
    })
}

function listar_producto_pr() {
    $.ajax({
        url: '../controlador/venta/controlador_listar_producto.php',
        type: 'POST'
    }).done(function (resp) {
        var data = JSON.parse(resp);
        var cadena = "<option value disabled='disabled' selected='selected'>Elegir una opcion</option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#cmb_producto").html(cadena);
            $("#cmb_producto_editar").html(cadena);
            // $("#producto_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cmb_producto").html(cadena);
            $("#cmb_producto_editar").html(cadena);
        }
    })
}

function listar_play_combo() {
    $.ajax({
        url: '../controlador/plan/play/controlador_listar_combo_play.php',
        type: 'POST'
    }).done(function (resp) {

        var data = JSON.parse(resp);
        var cadena = "<option value disabled='disabled' selected='selected'>Elegir una opcion</option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#cbm_play").html(cadena);
            $("#cbm_play_editar").html(cadena);
            // $("#producto_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cbm_play").html(cadena);
            $("#cbm_play_editar").html(cadena);
        }
    })
}
function listar_plan_internet_combo() {
    $.ajax({
        url: '../controlador/plan/plan_internet/controlador_listar_combo_plan.php',
        type: 'POST'
    }).done(function (resp) {

        var data = JSON.parse(resp);
        var cadena = "<option value disabled='disabled' selected='selected'>Elegir una opcion</option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

            }
            $("#cbm_plan").html(cadena);
            $("#cbm_plan_editar").html(cadena);
            // $("#producto_editar").html(cadena);
        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cbm_plan").html(cadena);
            $("#cbm_plan_editar").html(cadena);
        }
    })
}


function listar_paquete_combo(idproducto, idplay, idplan) {
    $.ajax({
        url: '../controlador/plan/paquetes/controlador_listar_combo_paquete.php',
        type: 'POST',
        data: {
            idproducto: idproducto,
            idplay: idplay,
            idplan: idplan
        }
    }).done(function (resp) {
        console.log("listar_paquete_combo");
        var data = JSON.parse(resp);
        var cadena = "<option value disabled='disabled' selected='selected'>Elegir una opcion</option>";
        
        if (data === null) {
            console.log("data es null");
        } else {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cadena += "<option value='" + data[i][0] + "'>" + data[i][4] + "</option>";

                }
                $("#cbm_paquete").html(cadena);
                $("#cbm_paquete_editar").html(cadena);
                // $("#producto_editar").html(cadena);
            } else {
                cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
                $("#cbm_paquete").html(cadena);
                $("#cbm_paquete_editar").html(cadena);
            }
        }
    })
}

function listar_paquete_combo_editar() {
    
    $.ajax({
        url: '../controlador/plan/paquetes/controlador_listar_paquete_general.php',
        type: 'POST'
        
    }).done(function (resp) {
        console.log("funcion listar_paquete_combo_editar");
        var data = JSON.parse(resp);
        var cadena = "<option value disabled='disabled' selected='selected'>Elegir una opcion</option>";
        if (data === null) {

        } else {
            if (data.length > 0) {
                for (var i = 0; i < data.length; i++) {
                    cadena += "<option value='" + data[i][0] + "'>" + data[i][4] + "</option>";

                }

                $("#cbm_paquete_editar").html(cadena);
                // $("#producto_editar").html(cadena);
            } else {
                cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";

                $("#cbm_paquete_editar").html(cadena);
            }
        }

    })
}

function cambiar_precio_fijo(id) {
    $.ajax({
        url: '../controlador/plan/paquetes/controlador_cambiar_preciofijo.php',
        type: 'POST',
        data: { id: id }
    }).done(function (resp) {
        $("#txt_cargo_fijo").val(resp);
        $("#txt_cargo_total").val(resp);
        $("#txt_cargo_fijo_editar").val(resp);
        var cargoadd= parseInt($("#txt_cargo_adicional_editar").val());
        if(isNaN(cargoadd)){
            cargoadd=0;
        }
        $("#txt_cargo_total_editar").val(parseInt(resp)+cargoadd);

    })
}
var iddeco1;
var idedeco2;
var idpremium;
function Obtener_precio_decoadd1(id, nombre, nombrecolumna) {
    if(id==null && nombre===undefined){
    
}else{
    if (id != "0") {
        iddeco1 = id;
        // precioadicional = precioadicional - parseInt(preciodeco1);
        $.ajax({
            url: '../controlador/plan/decoadd1/controlador_cambiar_obtenerpreciodeco1.php',
            type: 'POST',
            data: { id: id }
        }).done(function (resp) {
            // preciodeco1 = resp;
            // precioadicional = precioadicional + parseInt(resp);
            // console.log(precioadicional);
            // $("#txt_cargo_adicional").val(precioadicional);
            agregar_adicionale(id, resp, nombre, nombrecolumna);
            sumarNeto();
        })

    } 
}



}
function Obtener_precio_decoadd2(id, nombre, nombrecolumna) {
    if(id==null && nombre===undefined){
        
    }else{
        if (id != "0") {
        $.ajax({
            url: '../controlador/plan/decoadd2/controlador_cambiar_obtenerpreciodeco2.php',
            type: 'POST',
            data: { id: id }
        }).done(function (resp) {

            // $("#txt_cargo_adicional").val(precioadicional);
            agregar_adicionale(id, resp, nombre, nombrecolumna);
            sumarNeto();
        });


    }
    }
    



}
function Obtener_precio_paqupremium(id, nombre, nombrecolumna) {
    if(id==null && nombre===undefined){
}else{
    if (id != "0") {
        $.ajax({
            url: '../controlador/plan/premium/controlador_cambiar_obtenerpreciopremium.php',
            type: 'POST',
            data: { id: id }
        }).done(function (resp) {

            // $("#txt_cargo_adicional").val(precioadicional);
            agregar_adicionale(id, resp, nombre, nombrecolumna);
            sumarNeto();
        });


    }
}

}


function listar_decoadd1() {
    $.ajax({
        url: '../controlador/plan/decoadd1/controlador_listar_combo_deco1.php',
        type: 'POST'
    }).done(function (resp) {

        var data = JSON.parse(resp);

        var cadena = "<option value='0' selected='selected'><span>No</span></option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";
            }

            $("#cbm_decoadd1").html(cadena);
            $("#cbm_decoadd1_editar").html(cadena);
            // $("#producto_editar").html(cadena);

        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cbm_decoadd1").html(cadena);
            $("#cbm_decoadd1_editar").html(cadena);
        }
    })
}

function listar_decoadd2() {
    $.ajax({
        url: '../controlador/plan/decoadd2/controlador_listar_combo_deco2.php',
        type: 'POST'
    }).done(function (resp) {

        var data = JSON.parse(resp);

        var cadena = "<option value='0' selected='selected'><span>No</span></option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";
            }

            $("#cbm_decoadd2").html(cadena);
            $("#cbm_decoadd2_editar").html(cadena);
            // $("#producto_editar").html(cadena);

        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cbm_decoadd2").html(cadena);
            $("#cbm_decoadd2_editar").html(cadena);
        }
    })
}

function listar_paquetepremium() {
    $.ajax({
        url: '../controlador/plan/premium/controlador_listar_combo_premium.php',
        type: 'POST'
    }).done(function (resp) {

        var data = JSON.parse(resp);

        var cadena = "<option value='0' selected='selected'><span>No</span></option>";
        if (data.length > 0) {
            for (var i = 0; i < data.length; i++) {
                cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";
            }

            $("#cbm_paquetepre").html(cadena);
            $("#cbm_paquetepre_editar").html(cadena);

        } else {
            cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
            $("#cbm_paquetepre").html(cadena);
            $("#cbm_paquetepre_editar").html(cadena);
        }
    })
}



function agregar_adicionale(id, precio, nombre, nombrefila) {
    if (verificarid(id)) {
        return console.log("ya existe");
    }

    let datos_agregar = "<tr>";
    datos_agregar += "<td for='id'>" + id + "</td>";
    datos_agregar += "<td>" + nombrefila + "</td>";
    datos_agregar += "<td>" + nombre + "</td>";
    datos_agregar += "<td>" + precio + "</td>";
    datos_agregar += "<td><button type='button' class='btn btn-danger' onclick='remove(this)'><i class='fa fa-trash'></i></button></td>";
    datos_agregar += "</tr>"
    $("#tbody_detalle_adicionales").append(datos_agregar);
}

function remove(t) {

    var td = t.parentNode;
    var tr = td.parentNode;
    var table = tr.parentNode;
    table.removeChild(tr);
    sumarNeto();
}

function removerAll(){
    $("#detalle_adicionales tbody#tbody_detalle_adicionales tr").remove();
}

function sumarNeto() {
    var cargofijo = $("#txt_cargo_fijo").val();
    console.log(cargofijo);
    if (cargofijo === undefined) {
        cargofijo = 0;

    } else {
        if (cargofijo.length == 0) {
            cargofijo = 0;
        }
    }

    var cargofijo2 = $("#txt_cargo_fijo_editar").val();
    if (cargofijo2 === undefined) {
        cargofijo2 = 0;
    } else {
        if (cargofijo2.length == 0) {
            cargofijo2 = 0;
        }
    }

    var arreglo = new Array();
    var count = 0;
    var total = 0;
    $("#detalle_adicionales tbody#tbody_detalle_adicionales tr").each(function () {
        arreglo.push($(this).find("td").eq(3).text());
        count++;
    })
    for (var i = 0; i < count; i++) {

        total += parseInt(arreglo[i]);
    }
    if (isNaN(total)) {
        total = 0;
    }
    var neto = parseInt(cargofijo) + total;
    var neto2 = parseInt(cargofijo2) + total;

    $("#txt_cargo_adicional").val(total);
    $("#txt_cargo_total").val(neto);
    $("#txt_cargo_adicional_editar").val(total);
    console.log(neto2);
    setTimeout(function () {
        $("#txt_cargo_total_editar").val(neto2);
    }, 2000);
}

function verificarid(id) {
    let idverificar = document.querySelectorAll("#detalle_adicionales td[for='id']");

    return [].filter.call(idverificar, td => td.textContent === id).length === 1;
}


function Boton_decoadicional() {
    var x = document.getElementById("adicional_deco");
    var y = document.getElementById("adicional_premium");
    if (x.style.display === "none") {
        x.style.display = "block";
        document.getElementById("adicional_tabladetalle").style.display = "block";
        document.getElementById("adicional_cargo_add").style.display = "block";
    } else {
        x.style.display = "none";
        listar_decoadd1();
        if(x.style.display === "none" && y.style.display === "none"){
            document.getElementById("adicional_tabladetalle").style.display = "none";
            document.getElementById("adicional_cargo_add").style.display = "none";
            removerAll();
            $("#txt_cargo_adicional").val("0");
            $("#txt_cargo_total").val($("#txt_cargo_fijo").val());
            $("#txt_cargo_adicional_editar").val("0");
            $("#txt_cargo_total_editar").val($("#txt_cargo_fijo_editar").val());
        }
        
        }
    }

    


function Boton_servicioadicional() {
    var x = document.getElementById("adicional_premium");
    var y = document.getElementById("adicional_deco");
    if (x.style.display === "none") {
        x.style.display = "block";
    document.getElementById("adicional_tabladetalle").style.display = "block";
    document.getElementById("adicional_cargo_add").style.display = "block";
    } else {
        
        x.style.display = "none";
        listar_paquetepremium();
        if(x.style.display === "none" && y.style.display === "none"){
            document.getElementById("adicional_tabladetalle").style.display = "none";
            document.getElementById("adicional_cargo_add").style.display = "none";
            removerAll();
            $("#txt_cargo_adicional").val("0");
            $("#txt_cargo_total").val($("#txt_cargo_fijo").val());
            $("#txt_cargo_adicional_editar").val("0");
            $("#txt_cargo_total_editar").val($("#txt_cargo_fijo_editar").val());
        }
    }
}


