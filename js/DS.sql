CREATE DEFINER=`root`@`localhost` PROCEDURE `SP_EDITAR_VENTA_CON_PAGO`(
IN IDUSUARIO INT,
		IN ESTATUS VARCHAR(25),
IN IDPREVENTA INT,
IN IDCLIENTE INT,
IN IDDIRECCION INT,
IN IDDETALLEPREVENTA INT,
IN IDGRABACION INT,
IN MOVILLLAMADA VARCHAR(12),
IN FECHALLAMADA VARCHAR(12),
IN HORALLAMADA VARCHAR(12),
IN TIPDOCUMENTO VARCHAR(10),
IN NRODOCUMENTOACTUAL VARCHAR(20),
IN NRODOCUMENTONUEVO VARCHAR(20),
IN NOMBRE VARCHAR(100),
IN FIJOPORTAR VARCHAR(12),
IN PRIMERNOMPAPA VARCHAR(10),
IN PRIMERNOMMAMA VARCHAR(10),
IN FECHANACIMIENTO VARCHAR(12),
IN SIGNOSODIACO VARCHAR(12),
IN IDDISTRINAC INT,
IN VIA VARCHAR(100),
IN LOCALIDAD INT,
IN REFERENCIA VARCHAR(255),
IN EMAIL VARCHAR(100),
IN MOVILTITULAR VARCHAR(12),
IN MOVILCONTACTO VARCHAR(12),
IN MOVILCOORDINACIO VARCHAR(12),
IN PLANO VARCHAR(15),
IN IDPRODUCTO INT,
IN PLAN VARCHAR(50),
IN CARGOFIJO VARCHAR(10),
IN COSTOINSTALACION VARCHAR(10),
IN FULLCLARO VARCHAR(10),
IN PREGUNTA TEXT,
IN BANCAMOVIL VARCHAR(5),
IN FECHAPAGO VARCHAR(12),
IN HORAPAGO VARCHAR(12),
IN PROMOCION VARCHAR(255),
IN COMENTARIO VARCHAR(255),
IN ROL VARCHAR(20),
		IN VA VARCHAR(20),
		IN CODSOT VARCHAR(15),
		IN CODSET VARCHAR(15),
		IN COMENTARIO_ESTADO VARCHAR(255),
		IN FECHAINSTALACION VARCHAR(12),
		IN HORAINICIO VARCHAR(12),
		IN HORAFIN VARCHAR(12),
		IN COMENTARIO_ESTADO_VENTA VARCHAR(100)
)
BEGIN
DECLARE CANTIDADCLI INT;

IF ESTATUS = 'ENVIADO' OR ESTATUS = 'OBSERVADO' OR ESTATUS = 'TRAMITANDO' THEN
	IF NRODOCUMENTOACTUAL = NRODOCUMENTONUEVO  THEN
		IF ROL=  'ASESOR COMERCIAL' THEN
				UPDATE direccion SET 
			direccion_via = VIA,
			
			localidad_id = LOCALIDAD
			WHERE direccion_id = IDDIRECCION;
			
			UPDATE cliente SET 
			cliente_nombre = NOMBRE,
			cliente_papa= PRIMERNOMPAPA,
			cliente_mama = PRIMERNOMMAMA,
			cliente_lugarnacimiento = IDDISTRINAC,
			cliente_fenacimiento = STR_TO_DATE(FECHANACIMIENTO, '%Y-%m-%d'),
			cliente_correo = EMAIL,
			cliente_tipdocumento = TIPDOCUMENTO,
			cliente_nrodocumento = NRODOCUMENTOACTUAL,
			cliente_fijoportar = FIJOPORTAR,
			cliente_moviltitular = MOVILTITULAR,
			cliente_movilcontacto = MOVILCONTACTO,
			cliente_movilcoordinacion = MOVILCOORDINACIO,
			cliente_signosodiaco = SIGNOSODIACO,
			cliente_referencia = REFERENCIA
			WHERE cliente_id = IDCLIENTE;
					
					IF FECHAINSTALACION='VACIO'AND HORAINICIO='VACIO'AND HORAFIN='VACIO' THEN
					UPDATE preventa SET
					preventa_plano = PLANO,
					preventa_status = 'ENVIADO',
					preventa.usuario_id = IDUSUARIO,
					preventa_comentario = COMENTARIO,
					preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
					preventa_horapago = HORAPAGO,
					preventa_fecinstalacion= NULL,
					preventa_hrainicio_ist=NULL,
					preventa_hrafin_ist = NULL,
					preventa_va=VA,
					preventa_sot=CODSOT,
					preventa_set= CODSET,
					preventa_comentario_venta= COMENTARIO_ESTADO,
					preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
					WHERE preventa_id = IDPREVENTA;
				ELSE
					UPDATE preventa SET
					preventa_plano = PLANO,
					preventa_status = 'ENVIADO',
					preventa.usuario_id = IDUSUARIO,
					preventa_comentario = COMENTARIO,
					preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
					preventa_horapago = HORAPAGO,
					preventa_fecinstalacion= STR_TO_DATE(FECHAINSTALACION, '%Y-%m-%d'),
					preventa_hrainicio_ist=HORAINICIO,
					preventa_hrafin_ist = HORAFIN,
					preventa_va=VA,
					preventa_sot=CODSOT,
					preventa_set= CODSET,
					preventa_comentario_venta= COMENTARIO_ESTADO,
					preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
					WHERE preventa_id = IDPREVENTA;
					END IF;
					
					
					UPDATE detalle_preventa SET  
					detalleprev_promocion = PROMOCION,
					detalleprev_plan = PLAN,
					detalleprev_cargofijo = CARGOFIJO,
					detallepre_fullclaro = FULLCLARO,
					detallepre_pregunta = PREGUNTA,
					preventapre_bancamovil = BANCAMOVIL,
					detallepre_costoistal = COSTOINSTALACION
					WHERE detalleprev_id = IDDETALLEPREVENTA;
					
					UPDATE grabacion_preventa SET  
					grabacionpre_nrollamada = MOVILLLAMADA,
					grabacionpre_fechallamada = STR_TO_DATE(FECHALLAMADA, '%Y-%m-%d'),
					grabacionpre_horllamada = HORALLAMADA
					WHERE grabacionpre_id = IDGRABACION;
	
	SELECT 1;
		ELSE
		UPDATE direccion SET 
	direccion_via = VIA,
	
	localidad_id = LOCALIDAD
	WHERE direccion_id = IDDIRECCION;
	
	UPDATE cliente SET 
	cliente_nombre = NOMBRE,
	cliente_papa= PRIMERNOMPAPA,
	cliente_mama = PRIMERNOMMAMA,
	cliente_lugarnacimiento = IDDISTRINAC,
	cliente_fenacimiento = STR_TO_DATE(FECHANACIMIENTO, '%Y-%m-%d'),
	cliente_correo = EMAIL,
	cliente_tipdocumento = TIPDOCUMENTO,
	cliente_nrodocumento = NRODOCUMENTOACTUAL,
	cliente_fijoportar = FIJOPORTAR,
	cliente_moviltitular = MOVILTITULAR,
	cliente_movilcontacto = MOVILCONTACTO,
	cliente_movilcoordinacion = MOVILCOORDINACIO,
	cliente_signosodiaco = SIGNOSODIACO,
	cliente_referencia = REFERENCIA
	WHERE cliente_id = IDCLIENTE;
	
	IF  FECHAINSTALACION='VACIO' AND HORAINICIO='VACIO' AND HORAFIN='VACIO' THEN
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= NULL,
	preventa_hrainicio_ist=NULL,
	preventa_hrafin_ist = NULL,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
ELSE
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= STR_TO_DATE(FECHAINSTALACION, '%Y-%m-%d'),
	preventa_hrainicio_ist=HORAINICIO,
	preventa_hrafin_ist = HORAFIN,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
	END IF;
	
	
	UPDATE detalle_preventa SET  
	detalleprev_promocion = PROMOCION,
	detalleprev_plan = PLAN,
	detalleprev_cargofijo = CARGOFIJO,
	detallepre_fullclaro = FULLCLARO,
	detallepre_pregunta = PREGUNTA,
	preventapre_bancamovil = BANCAMOVIL,
	detallepre_costoistal = COSTOINSTALACION
	WHERE detalleprev_id = IDDETALLEPREVENTA;
	
	UPDATE grabacion_preventa SET  
	grabacionpre_nrollamada = MOVILLLAMADA,
	grabacionpre_fechallamada = STR_TO_DATE(FECHALLAMADA, '%Y-%m-%d'),
	grabacionpre_horllamada = HORALLAMADA
	WHERE grabacionpre_id = IDGRABACION;
	
	SELECT 1;
		END IF;
	ELSE
		SET @CANTIDADCLI :=(
        SELECT COUNT(*)
        FROM cliente
        WHERE cliente_nrodocumento = NRODOCUMENTONUEVO
    );
		IF @CANTIDADCLI = 0 THEN
			IF ROL =  'ASESOR COMERCIAL' THEN 
			UPDATE direccion SET 
			direccion_via = VIA,
			
			localidad_id = LOCALIDAD
			WHERE direccion_id = IDDIRECCION;
			
			UPDATE cliente SET 
			cliente_nombre = NOMBRE,
			cliente_papa= PRIMERNOMPAPA,
			cliente_mama = PRIMERNOMMAMA,
			cliente_lugarnacimiento = IDDISTRINAC,
			cliente_fenacimiento = STR_TO_DATE(FECHANACIMIENTO, '%Y-%m-%d'),
			cliente_correo = EMAIL,
			cliente_tipdocumento = TIPDOCUMENTO,
			cliente_nrodocumento = NRODOCUMENTONUEVO,
			cliente_fijoportar = FIJOPORTAR,
			cliente_moviltitular = MOVILTITULAR,
			cliente_movilcontacto = MOVILCONTACTO,
			cliente_movilcoordinacion = MOVILCOORDINACIO,
			cliente_signosodiaco = SIGNOSODIACO,
			cliente_referencia = REFERENCIA
			WHERE cliente_id = IDCLIENTE;
		
			IF  FECHAINSTALACION='VACIO' AND HORAINICIO='VACIO' AND HORAFIN='VACIO' THEN
			UPDATE preventa SET
			preventa_plano = PLANO,
			preventa_status = 'ENVIADO',
			preventa.usuario_id = IDUSUARIO,
			preventa_comentario = COMENTARIO,
			preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
			preventa_horapago = HORAPAGO,
			preventa_fecinstalacion= NULL,
			preventa_hrainicio_ist=NULL,
			preventa_hrafin_ist = NULL,
			preventa_va=VA,
			preventa_sot=CODSOT,
			preventa_set= CODSET,
			preventa_comentario_venta= COMENTARIO_ESTADO,
			preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
			WHERE preventa_id = IDPREVENTA;
		ELSE
			UPDATE preventa SET
			preventa_plano = PLANO,
			preventa_status = 'ENVIADO',
			preventa.usuario_id = IDUSUARIO,
			preventa_comentario = COMENTARIO,
			preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
			preventa_horapago = HORAPAGO,
			preventa_fecinstalacion= STR_TO_DATE(FECHAINSTALACION, '%Y-%m-%d'),
			preventa_hrainicio_ist=HORAINICIO,
			preventa_hrafin_ist = HORAFIN,
			preventa_va=VA,
			preventa_sot=CODSOT,
			preventa_set= CODSET,
			preventa_comentario_venta= COMENTARIO_ESTADO,
			preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
			WHERE preventa_id = IDPREVENTA;
			END IF;
	
			
	
			UPDATE detalle_preventa SET  
			detalleprev_promocion = PROMOCION,
			detalleprev_plan = PLAN,
			detalleprev_cargofijo = CARGOFIJO,
			detallepre_fullclaro = FULLCLARO,
			detallepre_pregunta = PREGUNTA,
			preventapre_bancamovil = BANCAMOVIL,
			detallepre_costoistal = COSTOINSTALACION
			WHERE detalleprev_id = IDDETALLEPREVENTA;
	
			UPDATE grabacion_preventa SET  
			grabacionpre_nrollamada = MOVILLLAMADA,
			grabacionpre_fechallamada = STR_TO_DATE(FECHALLAMADA, '%Y-%m-%d'),
			grabacionpre_horllamada = HORALLAMADA
			WHERE grabacionpre_id = IDGRABACION;
	
			SELECT 1;
			ELSE
			UPDATE direccion SET 
			direccion_via = VIA,
		
			localidad_id = LOCALIDAD
			WHERE direccion_id = IDDIRECCION;
			
			UPDATE cliente SET 
			cliente_nombre = NOMBRE,
			cliente_papa= PRIMERNOMPAPA,
			cliente_mama = PRIMERNOMMAMA,
			cliente_lugarnacimiento = IDDISTRINAC,
			cliente_fenacimiento = STR_TO_DATE(FECHANACIMIENTO, '%Y-%m-%d'),
			cliente_correo = EMAIL,
			cliente_tipdocumento = TIPDOCUMENTO,
			cliente_nrodocumento = NRODOCUMENTONUEVO,
			cliente_fijoportar = FIJOPORTAR,
			cliente_moviltitular = MOVILTITULAR,
			cliente_movilcontacto = MOVILCONTACTO,
			cliente_movilcoordinacion = MOVILCOORDINACIO,
			cliente_signosodiaco = SIGNOSODIACO,
			cliente_referencia = REFERENCIA
			WHERE cliente_id = IDCLIENTE;
	
			IF  FECHAINSTALACION='VACIO' AND HORAINICIO='VACIO' AND HORAFIN='VACIO' THEN
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= NULL,
	preventa_hrainicio_ist=NULL,
	preventa_hrafin_ist = NULL,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
ELSE
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= STR_TO_DATE(FECHAINSTALACION, '%Y-%m-%d'),
	preventa_hrainicio_ist=HORAINICIO,
	preventa_hrafin_ist = HORAFIN,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
	END IF;
	
	
			UPDATE detalle_preventa SET  
			detalleprev_promocion = PROMOCION,
			detalleprev_plan = PLAN,
			detalleprev_cargofijo = CARGOFIJO,
			detallepre_fullclaro = FULLCLARO,
			detallepre_pregunta = PREGUNTA,
			preventapre_bancamovil = BANCAMOVIL,
			detallepre_costoistal = COSTOINSTALACION
			WHERE detalleprev_id = IDDETALLEPREVENTA;
	
			UPDATE grabacion_preventa SET  
			grabacionpre_nrollamada = MOVILLLAMADA,
			grabacionpre_fechallamada = STR_TO_DATE(FECHALLAMADA, '%Y-%m-%d'),
			grabacionpre_horllamada = HORALLAMADA
			WHERE grabacionpre_id = IDGRABACION;
	
			SELECT 1;
			END IF;
		ELSE
			SELECT 2;
		END IF;

		
		
	END IF;
ELSE
	IF ESTATUS <> 'ENVIADO' OR ESTATUS <>' OBSERVADO' OR ESTATUS<>'TRAMITANDO' THEN
			IF  FECHAINSTALACION='VACIO' AND HORAINICIO='VACIO' AND HORAFIN='VACIO' THEN
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= NULL,
	preventa_hrainicio_ist=NULL,
	preventa_hrafin_ist = NULL,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
ELSE
	UPDATE preventa SET
	preventa_plano = PLANO,
	preventa_status = ESTATUS,
	preventa.usuario_id = IDUSUARIO,
	preventa_comentario = COMENTARIO,
	preventa_diapago = STR_TO_DATE(FECHAPAGO, '%Y-%m-%d'),
	preventa_horapago = HORAPAGO,
	preventa_fecinstalacion= STR_TO_DATE(FECHAINSTALACION, '%Y-%m-%d'),
	preventa_hrainicio_ist=HORAINICIO,
	preventa_hrafin_ist = HORAFIN,
	preventa_va=VA,
	preventa_sot=CODSOT,
	preventa_set= CODSET,
	preventa_comentario_venta= COMENTARIO_ESTADO,
	preventa_comentario_venta_estado=COMENTARIO_ESTADO_VENTA
	WHERE preventa_id = IDPREVENTA;
	END IF;
			SELECT 1;
		ELSE
		SELECT 2;
		END IF;
END IF;




END