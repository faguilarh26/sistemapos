var t_venta;

function listar_venta() {
  var rol = $("#txt_rol").val();
  var idusuario = $("#txt_id_usuario").val();
  var fechainicio = $("#fecha_inicio").val();
  var fechafin = $("#fecha_fin").val();
  t_venta = $("#tabla_venta").DataTable({

    ordering: true,
    pageLength: 10,
    destroy: true,
    async: false,
    paging: true,
    bLengthChange: false,
    // responsive: true,
    scrollX: true,
    autoWidth: false,
    ajax: {
      method: "POST",
      url: "../controlador/venta/controlador_venta_listar.php",
      data: {
        idusuario: idusuario,
        rol: rol,
        fechainicio: fechainicio,
        fechafin: fechafin
      }
    },
    columns: [
      { defaultContent: "" },
      {
        data: "preventa_status",
        render: function (data, type, row) {
          if (data == "ENVIADO") {
            return (
              "<span class='badge badge-info badge-pill m-r-5 m-b-5' style='background-color:#fff; color:black;'>" +
              data +
              "</span>"
            );
          } else if (data == "OBSERVADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f75a5f ;''>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE PAGO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(201, 181, 116);'>" +
              data +
              "</span>"
            );
          } else if (data == "EJECUCION") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#178ab0;'>" +
              data +
              "</span>"
            );
          } else if (data == "AGENDADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#a4a89d;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE VALIDAR") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f77212;'>" +
              data +
              "</span>"
            );
          } else if (data == "INSTALADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#2ecc71;'>" +
              data +
              "</span>"
            );
          } else if (data == "RECHAZADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#e74c3c;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE REASIGNACION") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#efa1f5;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE REASIGNACION" || data == "ADJUNTAR DOCUMENTO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(216, 215, 209);'>" +
              data +
              "</span>"
            );
          } else if (data == "TRAMITANDO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#1999c4;'>" +
              data +
              "</span>"
            );
          }
          // else {
          //   return (
          //     "<span class='badge badge-warning badge-pill m-r-5 m-b-5'>" +
          //     data +
          //     "</span>"
          //   );
          // }

        },

      },
      {
        data: "preventa_status",
        render: function (data, type, row) {
          if (data == "EJECUCION" && rol == "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";

          } else if ((data == "ENVIADO" || data == "OBSERVADO" || data == "EJECUCION") && rol != "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          } else if ((data == "ENVIADO" || data == "OBSERVADO") && rol == "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          }
          else {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          }

        },
      },

      { data: "fechallamada" },
      { data: "cliente_nombre" },
      { data: "cliente_tipdocumento" },
      { data: "cliente_nrodocumento" },
      { data: "cliente_fijoportar" },
      { data: "cliente_fenacimiento" },
      { data: "lugardenacimiento" },
      { data: "cliente_papa" },
      { data: "cliente_mama" },
      { data: "direccion_via" },
      { data: "residenciaactual" },
      { data: "cliente_referencia" },
      { data: "cliente_moviltitular" },
      { data: "cliente_movilcontacto" },
      { data: "cliente_movilcoordinacion" },
      { data: "cliente_correo" },
      { data: "preventa_plano" },
      { data: "producto_nombre" },
      { data: "detalleprev_promocion" },
      { data: "detalleprev_plan" },
      { data: "detalleprev_cargofijo" },
      { data: "detallepre_asesorcomercial" },
      { data: "detallepre_fullclaro" },
      { data: "detallepre_pregunta" },
      { data: "preventapre_bancamovil" },


    ],
    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      // $($(nRow).find("td")[3]).css("text-align", "center");
      // $($(nRow).find("td")[4]).css("text-align", "center");
    },
    language: idioma_espanol,
    select: true,
  });
  t_venta.on("draw.dt", function () {
    var PageInfo = $("#tabla_venta").DataTable().page.info();
    t_venta
      .column(0, { page: "current" })
      .nodes()
      .each(function (cell, i) {
        cell.innerHTML = i + 1 + PageInfo.start;
      });
  });

  // if (rol=="ASESOR COMERCIAL") {
  //   t_venta.column(1).visible(false);
  //   t_venta.column(2).visible(false);
  //   t_venta.column(3).visible(false);
  // }
}

function listar_venta_rechazada() {
  var rol = $("#txt_rol").val();
  var idusuario = $("#txt_id_usuario").val();
  var fechainicio = $("#fecha_inicio").val();
  var fechafin = $("#fecha_fin").val();
  t_venta = $("#tabla_venta").DataTable({

    ordering: true,
    pageLength: 10,
    destroy: true,
    async: false,
    paging: true,
    bLengthChange: false,
    // responsive: true,
    scrollX: true,
    autoWidth: false,
    ajax: {
      method: "POST",
      url: "../controlador/venta/controlador_venta_listar_rechazadas.php",
      data: {
        idusuario: idusuario,
        rol: rol,
        fechainicio: fechainicio,
        fechafin: fechafin
      }
    },
    columns: [
      { defaultContent: "" },
      {
        data: "preventa_status",
        render: function (data, type, row) {
          if (data == "ENVIADO") {
            return (
              "<span class='badge badge-info badge-pill m-r-5 m-b-5' style='background-color:#fff; color:black;'>" +
              data +
              "</span>"
            );
          } else if (data == "OBSERVADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f75a5f ;''>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE PAGO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(201, 181, 116);'>" +
              data +
              "</span>"
            );
          } else if (data == "EJECUCION") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#178ab0;'>" +
              data +
              "</span>"
            );
          } else if (data == "AGENDADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#a4a89d;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE VALIDAR") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#f77212;'>" +
              data +
              "</span>"
            );
          } else if (data == "INSTALADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#2ecc71;'>" +
              data +
              "</span>"
            );
          } else if (data == "RECHAZADO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#e74c3c;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE REASIGNACION") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#efa1f5;'>" +
              data +
              "</span>"
            );
          } else if (data == "PENDIENTE REASIGNACION" || data == "ADJUNTAR DOCUMENTO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:rgb(216, 215, 209);'>" +
              data +
              "</span>"
            );
          } else if (data == "TRAMITANDO") {
            return (
              "<span class='badge  badge-pill m-r-5 m-b-5' style='background-color:#1999c4;'>" +
              data +
              "</span>"
            );
          }
          // else {
          //   return (
          //     "<span class='badge badge-warning badge-pill m-r-5 m-b-5'>" +
          //     data +
          //     "</span>"
          //   );
          // }

        },

      },
      {
        data: "preventa_status",
        render: function (data, type, row) {
          if (data == "EJECUCION" && rol == "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";

          } else if ((data == "ENVIADO" || data == "OBSERVADO" || data == "EJECUCION") && rol != "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          } else if ((data == "ENVIADO" || data == "OBSERVADO") && rol == "ASESOR COMERCIAL") {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          }
          else {
            return "<button class='editar_general btn btn-warning' ><i class='fa fa-eye'></i></button>";
          }

        },
      },

      { data: "fechallamada" },
      { data: "cliente_nombre" },
      { data: "cliente_tipdocumento" },
      { data: "cliente_nrodocumento" },
      { data: "cliente_fijoportar" },
      { data: "cliente_fenacimiento" },
      { data: "lugardenacimiento" },
      { data: "cliente_papa" },
      { data: "cliente_mama" },
      { data: "direccion_via" },
      { data: "residenciaactual" },
      { data: "cliente_referencia" },
      { data: "cliente_moviltitular" },
      { data: "cliente_movilcontacto" },
      { data: "cliente_movilcoordinacion" },
      { data: "cliente_correo" },
      { data: "preventa_plano" },
      { data: "producto_nombre" },
      { data: "detalleprev_promocion" },
      { data: "detalleprev_plan" },
      { data: "detalleprev_cargofijo" },
      { data: "detallepre_asesorcomercial" },
      { data: "detallepre_fullclaro" },
      { data: "detallepre_pregunta" },
      { data: "preventapre_bancamovil" },


    ],
    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      // $($(nRow).find("td")[3]).css("text-align", "center");
      // $($(nRow).find("td")[4]).css("text-align", "center");
    },
    language: idioma_espanol,
    select: true,
  });
  t_venta.on("draw.dt", function () {
    var PageInfo = $("#tabla_venta").DataTable().page.info();
    t_venta
      .column(0, { page: "current" })
      .nodes()
      .each(function (cell, i) {
        cell.innerHTML = i + 1 + PageInfo.start;
      });
  });

  // if (rol=="ASESOR COMERCIAL") {
  //   t_venta.column(1).visible(false);
  //   t_venta.column(2).visible(false);
  //   t_venta.column(3).visible(false);
  // }
}




var dataparaeditar;

$('#tabla_venta').on('click', '.editar', function () {
  var data = t_venta.row($(this).parents('tr')).data();
  if (t_venta.row(this).child.isShown()) {
    var data = t_venta.row(this).data();
  }

  dataparaeditar = data;


  $("#contenido_principal").load("venta/vista_editar_venta.php");



})
$('#tabla_venta').on('click', '.editar_general', function () {
  var data = t_venta.row($(this).parents('tr')).data();
  if (t_venta.row(this).child.isShown()) {
    var data = t_venta.row(this).data();
  }
  $("#modal_editar").modal({ backdrop: 'static', Keyboard: false });
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txt_idventa").val(data.preventa_id);
  $("#txt_cliente").val(data.cliente_id);
  $("#txt_iddireccion").val(data.direccion_id);
  $("#txt_iddetallepre").val(data.detalleprev_id);
  $("#txt_idgrabacion").val(data.grabacionpre_id);
  $("#txt_va").val(data.preventa_va).trigger("change");
  $("#txt_sot").val(data.preventa_sot);
  $("#txt_set").val(data.preventa_set);
  $("#txt_comentario_estado").val(data.preventa_comentario_venta);
  $("#txt_comentario_estado_venta").val(data.preventa_comentario_venta_estado);
  $("#txt_fecha_instalacion").val(data.preventa_fecinstalacion);
  $("#hora_inicio_instalacion").val(data.preventa_hrainicio_ist);
  $("#hora_fin_instalacion").val(data.preventa_hrafin_ist);
  $("#cbm_estatus").val(data.preventa_status).trigger("change");
  $("#txt_nrollamada_editar").val(data.grabacionpre_nrollamada);
  $("#txt_fechallamada_editar").val(data.grabacionpre_fechallamada);
  $("#txt_horallamada_editar").val(data.grabacionpre_horllamada);
  $("#cbm_tdocumento_editar").val(data.cliente_tipdocumento).trigger("change");
  $("#txt_nrodocumento_editar_actual").val(data.cliente_nrodocumento).prop("hidden", true);
  $("#txt_nrodocumento_editar_nuevo").val(data.cliente_nrodocumento);
  $("#txt_nombre_editar").val(data.cliente_nombre);
  $("#txt_fijoportar_editar").val(data.cliente_fijoportar);
  $("#txt_primernombrepapa_editar").val(data.cliente_papa);
  $("#txt_primenombremama_editar").val(data.cliente_mama);
  $("#txt_fecnacimiento_editar").val(data.cliente_fenacimiento);
  $("#txt_sodiaco_editar").val(data.cliente_signosodiaco);
  $("#sel_departamento_editar").val(data.naciiddepartamento).trigger("change");
  $("#sel_provincia_editar").val(data.naciidprovincia).trigger("change");
  $("#sel_distrito_editar").val(data.naciiddistrito).trigger("change");
  $("#txt_via_editar").val(data.direccion_via);
  $("#txt_nombre_via_editar").val(data.direccion_nombrevia);
  $("#txt_identificacion_editar").val(data.direccion_identificacion);
  $("#txt_interior_editar").val(data.direccion_interior);
  $("#txt_zona_editar").val(data.direccion_zona);
  $("#txt_nombre_zona_editar").val(data.direccion_nombrezona);
  $("#sel_departamento2_editar").val(data.localiiddepartamento).trigger("change");
  $("#sel_provincia2_editar").val(data.localiidprovincia).trigger("change");
  $("#sel_distrito2_editar").val(data.locaiddistrito).trigger("change");
  $("#txt_referencia_editar").val(data.cliente_referencia);
  $("#txt_email_editar").val(data.cliente_correo);
  $("#txt_movil_titular_editar").val(data.cliente_moviltitular);
  $("#txt_movil_contacto_editar").val(data.cliente_movilcontacto);
  $("#txt_movil_coordinacion_editar").val(data.cliente_movilcoordinacion);
  $("#txt_plano_editar").val(data.preventa_plano);
  $("#cmb_producto_editar").val(data.producto_id).trigger("change");
  $("#cbm_play_editar").val(data.detallepre_play).trigger("change");
  $("#cbm_plan_editar").val(data.detallepre_plan).trigger("change");

  $("#txt_promocion_editar").val(data.detalleprev_promocion);

  $("#txt_costo_intalacion_editar").val(data.detallepre_costoistal);
  $("#txt_asesor_comercial_editar").val(data.detallepre_asesorcomercial);
  $("#cbm_fullclaro_editar").val(data.detallepre_fullclaro).trigger("change");

  $("#cbm_tipoventa_editar").val(data.detallepre_tipoventa).trigger("change");

  $("#cbm_paquete_editar").val(data.detallepre_paquete).trigger("change");
  $("#txt_cargo_fijo_editar").val(data.detalleprev_cargofijo);
  $("#cbm_adicional_editar").val(data.adicional).trigger("change");

  $("#cbm_decoadd1_editar").val(data.detallepre_deco1).trigger("change");
  if ($("#cbm_decoadd1_editar").val() != "0") {
    document.getElementById("adicional_deco").style.display = "block";
  }
  $("#cbm_decoadd2_editar").val(data.detallepre_deco2).trigger("change");
  $("#cbm_paquetepre_editar").val(data.detallepre_premium).trigger("change");
  if ($("#cbm_paquetepre_editar").val() != "0") {
    document.getElementById("adicional_premium").style.display = "block";
  }
  if ($("#cbm_decoadd1_editar").val() != "0" && $("#cbm_paquetepre_editar").val() != "0") {
    document.getElementById("adicional_tabladetalle").style.display = "none";
    document.getElementById("txt_cargo_adicional_editar").style.display = "none";
  }
  $("#txt_cargo_adicional_editar").val(data.detallepre_cargoadicional);
  $("#txt_cargo_total_editar").val(data.detallepre_cargototal);

  $("#txt_pregunta_editar").val(data.detallepre_pregunta);
  $("#txt_bancamovil_editar").val(data.preventapre_bancamovil);
  $("#txt_diapago_editar").val(data.preventa_diapago);
  $("#txt_horapago_editar").val(data.preventa_horapago);
  $("#txt_comentario").val(data.preventa_comentario);
  $("#txt_comentario").val(data.preventa_comentario);

  var estado = $("#cbm_estatus").val();
  var rolusu = $("#txt_rol").val();

  if (rolusu == "ASESOR COMERCIAL") {
    $("#cbm_idusuario_editar").val(data.usuario_id).trigger("change").prop("disabled", true);
    document.getElementById("comentariosmensaje").style.display = "none";
    if (estado == "ENVIADO") {
      document.getElementById("div_error").style.display = "none";
      document.getElementById("cuadro_venta").style.display = "none";
      document.getElementById("div_botoneditar").innerHTML =
        "<button type='button' class='btn btn-primary submitBtn' onclick='Editar_Venta()'>Actualizar</button>";

    } else if (estado == "OBSERVADO") {
      document.getElementById("div_error").style.display = "block";
      document.getElementById("cuadro_venta").style.display = "none";
      document.getElementById("div_error").innerHTML =
        "<strong>Revise los siguientes campos:</strong><br>" + data.preventa_comentario;
      document.getElementById("div_botoneditar").innerHTML =
        "<button type='button' class='btn btn-primary submitBtn' onclick='Editar_Venta()'>Actualizar</button>";
    } else if (estado != "OBSERVADO" || estado != "ENVIADO") {

      document.getElementById("cuadro_venta").style.display = "block";
      document.getElementById("div_error").style.display = "none";
      document.getElementById("div_botoneditar").innerHTML =
        "<button type='button' class='btn btn-primary submitBtn' '>Actualizar</button>";
    } else {
      document.getElementById("div_botoneditar").innerHTML =
        "<button type='button' class='btn btn-primary submitBtn' hidden>Actualizar</button>";
    }
  } else {
    $("#cbm_idusuario_editar").val(data.usuario_id).trigger("change").prop("disabled", false);
    document.getElementById("div_botoneditar").innerHTML =
      "<button type='button' class='btn btn-primary submitBtn' onclick='Editar_Venta()' >Actualizar</button>";
    document.getElementById("comentariosmensaje").style.display = "block";

  }
  setTimeout(function () {
    $("#cbm_paquete_editar").val(data.detallepre_paquete).trigger("change");

  }, 1000);


  setTimeout(sumarNeto, 2000);



})
function removerAll() {
  $("#detalle_adicionales tbody#tbody_detalle_adicionales tr").remove();
}
function sumarNeto() {


  var cargofijo2 = $("#txt_cargo_fijo_editar").val();
  if (cargofijo2 === undefined) {
    cargofijo2 = 0;
  } else {
    if (cargofijo2.length == 0) {
      cargofijo2 = 0;
    }
  }

  var arreglo = new Array();
  var count = 0;
  var total = 0;
  $("#detalle_adicionales tbody#tbody_detalle_adicionales tr").each(function () {
    arreglo.push($(this).find("td").eq(3).text());
    count++;
  })
  for (var i = 0; i < count; i++) {

    total += parseInt(arreglo[i]);
  }
  if (isNaN(total)) {
    total = 0;
  }

  var neto2 = parseInt(cargofijo2) + total;

  $("#txt_cargo_adicional_editar").val(total);
  console.log(neto2);

  $("#txt_cargo_total_editar").val(neto2);

}





var ejecucion = true;



function listar_producto() {
  $.ajax({
    url: '../controlador/venta/controlador_listar_producto.php',
    type: 'POST'
  }).done(function (resp) {
    var data = JSON.parse(resp);
    var cadena = "";
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

      }
      $("#producto").html(cadena);
      $("#producto_editar").html(cadena);
    } else {
      cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
      $("#producto_editar").html(cadena);
    }
  })
}



function Registrar_Venta() {

  var movilllamada = $("#txt_nrollamada").val();
  var fechallamada = $("#txt_fechallamada").val();
  var horallamada = $("#txt_horallamada").val();
  var tipodocumento = $("#cbm_tdocumento").val();
  var nrodocumento = $("#txt_nrodocumento").val();
  var nombre = $("#txt_nombre").val();
  var fijoportar = $("#txt_fijoportar").val();
  var prinompapa = $("#txt_primernombrepapa").val();
  var prinommama = $("#txt_primenombremama").val();
  var fechanacimiento = $("#txt_fecnacimiento").val();
  var signosodiacal = $("#txt_sodiaco").val();
  var distritonac = $("#sel_distrito").val();
  var via = $("#txt_via").val();
  var distritoact = $("#sel_distrito2").val();
  var referencia = $("#txt_referencia").val();
  var email = $("#txt_email").val();
  var moviltitular = $("#txt_movil_titular").val();
  var movilcontacto = $("#txt_movil_contacto").val();
  var movilcoordinacion = $("#txt_movil_coordinacion").val();
  var plano = $("#txt_plano").val();
  var producto = $("#cmb_producto").val();
  var plan = $("#cbm_plan").val();
  var cargofijo = $("#txt_cargo_fijo").val();
  var costoinstalacion = $("#txt_costo_intalacion").val();
  var idusuario = $("#txt_idusuario").val();
  var fullclaro = $("#cbm_fullclaro").val();
  var pregunta = $("#txt_pregunta").val();
  var bancamovil = $("#txt_bancamovil").val();
  var diapago = $("#txt_diapago").val();
  var horapago = $("#txt_horapago").val();
  var promocion = $("#txt_promocion").val();

  var tipoventa = $("#cbm_tipoventa").val();
  var play = $("#cbm_play").val();
  var paquete = $("#cbm_paquete").val();
  // var adicional = $("#cbm_adicional").val();
  var decoadd1 = $("#cbm_decoadd1").val();
  // var decoadd2 = $("#cbm_decoadd2").val();
  var premium = $("#cbm_paquetepre").val();
  var cargoadd = $("#txt_cargo_adicional").val();
  var cargototal = $("#txt_cargo_total").val();
  console.log(decoadd1);
  console.log(premium);



  if (movilllamada.length == 0 || tipodocumento == null || nrodocumento.length == 0 || nombre.length == 0 || fijoportar.length == 0 || prinompapa.length == 0 || prinommama.length == 0 || fechanacimiento.length == 0 || signosodiacal == null || distritonac.length == 0 || via.length == 0 || distritoact.length == 0 || referencia.length == 0 || email.length == 0 || moviltitular.length == 0 || movilcontacto.length == 0 || movilcoordinacion.length == 0 || plano.length == 0 || producto.length == 0 || plan.length == 0 || cargofijo.length == 0 || costoinstalacion.length == 0 || idusuario.length == 0 || fullclaro.length == 0 || pregunta.length == 0 || bancamovil.length == 0 || promocion.length == 0 || tipoventa == null || play == null || paquete == null || cargototal.length == 0) {


    if (movilllamada.length == 0) {
      $("#txt_nrollamada").css("border-color", "red");
    } else {
      $("#txt_nrollamada").css("border-color", "green");
    }
    if (nrodocumento.length == 0) {
      $("#txt_nrodocumento").css("border-color", "red");
    } else {
      $("#txt_nrodocumento").css("border-color", "green");
    }
    if (nombre.length == 0) {
      $("#txt_nombre").css("border-color", "red");
    } else {
      $("#txt_nombre").css("border-color", "green");
    }
    if (tipodocumento == null) {
      $("#cbm_tdocumento").css("border-color", "red");
    } else {
      $("#cbm_tdocumento").css("border-color", "green");

    }
    if (fijoportar.length == 0) {
      $("#txt_fijoportar").css("border-color", "red");
    } else {
      $("#txt_fijoportar").css("border-color", "green");

    }
    if (prinompapa.length == 0) {
      $("#txt_primernombrepapa").css("border-color", "red");
    } else {
      $("#txt_primernombrepapa").css("border-color", "green");

    }
    if (prinommama.length == 0) {
      $("#txt_primenombremama").css("border-color", "red");
    } else {
      $("#txt_primenombremama").css("border-color", "green");

    }

    if (fechanacimiento.length == 0) {
      $("#txt_fecnacimiento").css("border-color", "red");
    } else {
      $("#txt_fecnacimiento").css("border-color", "green");

    }
    if (signosodiacal == null) {
      $("#txt_sodiaco").css("border-color", "red");
    } else {
      $("#txt_sodiaco").css("border-color", "green");

    }
    if (via.length == 0) {
      $("#txt_via").css("border-color", "red");
    } else {
      $("#txt_via").css("border-color", "green");

    }
    if (referencia.length == 0) {
      $("#txt_referencia").css("border-color", "red");
    } else {
      $("#txt_referencia").css("border-color", "green");

    }
    if (email.length == 0) {
      $("#txt_email").css("border-color", "red");
    } else {
      $("#txt_email").css("border-color", "green");

    }
    if (moviltitular.length == 0) {
      $("#txt_movil_titular").css("border-color", "red");
    } else {
      $("#txt_movil_titular").css("border-color", "green");

    }
    if (movilcontacto.length == 0) {
      $("#txt_movil_contacto").css("border-color", "red");
    } else {
      $("#txt_movil_contacto").css("border-color", "green");

    }
    if (movilcoordinacion.length == 0) {
      $("#txt_movil_coordinacion").css("border-color", "red");
    } else {
      $("#txt_movil_coordinacion").css("border-color", "green");

    }
    if (plano.length == 0) {
      $("#txt_plano").css("border-color", "red");
    } else {
      $("#txt_plano").css("border-color", "green");

    }
    if (producto == null) {
      $("#cmb_producto").css("border-color", "red");
    } else {
      $("#cmb_producto").css("border-color", "green");

    }
    if (plan == null) {
      $("#cbm_plan").css("border-color", "red");
    } else {
      $("#cbm_plan").css("border-color", "green");

    }
    if (cargofijo.length == 0) {
      $("#txt_cargo_fijo").css("border-color", "red");
    } else {
      $("#txt_cargo_fijo").css("border-color", "green");

    }
    if (fullclaro == null) {
      $("#cbm_fullclaro").css("border-color", "red");
    } else {
      $("#cbm_fullclaro").css("border-color", "green");

    }
    if (pregunta.length == 0) {
      $("#txt_pregunta").css("border-color", "red");
    } else {
      $("#txt_pregunta").css("border-color", "green");
    }
    if (bancamovil == null) {
      $("#txt_bancamovil").css("border-color", "red");
    } else {
      $("#txt_bancamovil").css("border-color", "green");

    }
    if (tipoventa == null) {
      $("#cbm_tipoventa").css("border-color", "red");
    } else {
      $("#cbm_tipoventa").css("border-color", "green");

    }
    if (play == null) {
      $("#cbm_play").css("border-color", "red");
    } else {
      $("#cbm_play").css("border-color", "green");
    }
    if (paquete == null) {
      $("#cbm_paquete").css("border-color", "red");
    } else {
      $("#cbm_paquete").css("border-color", "green");
    }
    if (cargototal.length == 0) {
      $("#txt_cargo_total").css("border-color", "red");
    } else {
      $("#txt_cargo_total").css("border-color", "green");
    }
    if (costoinstalacion.length == 0) {
      $("#txt_costo_intalacion").css("border-color", "red");
    } else {
      $("#txt_costo_intalacion").css("border-color", "green");
    }





    return Swal.fire("Mensaje de advertencia", "Llene los campos vacios, excepto Dia de pago y Hora de pago que son opcionales", "warning");
  }


  if (validad_email(email)) {

  } else {
    return Swal.fire("Mensaje de advertencia", "El formato de email es incorrecto", "warning");
  }
  $.ajax({
    url: "../controlador/venta/controlador_venta_registro.php",
    type: 'POST',
    data: {
      movilllamada: movilllamada,
      fechallamada: fechallamada,
      horallamada: horallamada,
      tipodocumento: tipodocumento,
      nrodocumento: nrodocumento,
      nombre: nombre,
      fijoportar: fijoportar,
      prinompapa: prinompapa,
      prinommama: prinommama,
      fechanacimiento: fechanacimiento,
      signosodiacal: signosodiacal,
      distritonac: distritonac,
      via: via,
      distritoact: distritoact,
      referencia: referencia,
      email: email,
      moviltitular: moviltitular,
      movilcontacto: movilcontacto,
      movilcoordinacion: movilcoordinacion,
      plano: plano,
      producto: producto,
      plan: plan,
      cargofijo: cargofijo,
      costoinstalacion: costoinstalacion,
      idusuario: idusuario,
      fullclaro: fullclaro,
      pregunta: pregunta,
      bancamovil: bancamovil,
      diapago: diapago,
      horapago: horapago,
      promocion: promocion,

      tipoventa: tipoventa,
      play: play,
      paquete: paquete,

      decoadd1: decoadd1,

      premium: premium,
      cargoadd: cargoadd,
      cargototal: cargototal
    },

  }).done(function (resp) {
    console.log(resp);
    if (resp != 0) {
      if (resp == 1) {

        return Swal.fire({
          icon: `success`,
          title: `Mensaje de confirmacion`,
          text: `Datos guardados`,
          confirmButtonText: `Ok`
        }).then(function () {


          $("#contenido_principal").load("venta/vista_mantenimiento_venta.php");

        });


      } else {
        return Swal.fire("Mensaje de advertencia", "El usuario o email ya se encuentra en la BD", "warning");
      }
    } else {
      Swal.fire(
        "Mensaje De Error",
        "Lo sentimos, no se pudo completar el registro",
        "error"
      );
    }

  });

}

function Editar_Venta() {
  var rol = $("#txt_rol").val();
  var estado = $("#cbm_estatus").val();
  var idventa = $("#txt_idventa").val();
  var idcliente = $("#txt_cliente").val();
  var iddireccion = $("#txt_iddireccion").val();
  var iddetallepre = $("#txt_iddetallepre").val();
  var idgrabacion = $("#txt_iddetallepre").val();
  var movilllamada = $("#txt_nrollamada_editar").val();
  var fechallamada = $("#txt_fechallamada_editar").val();
  var horallamada = $("#txt_horallamada_editar").val();
  var tipodocumento = $("#cbm_tdocumento_editar").val();
  var nrodocumentoactual = $("#txt_nrodocumento_editar_actual").val();
  var nrodocumentonuevo = $("#txt_nrodocumento_editar_nuevo").val();
  var nombre = $("#txt_nombre_editar").val();
  var fijoportar = $("#txt_fijoportar_editar").val();
  var prinompapa = $("#txt_primernombrepapa_editar").val();
  var prinommama = $("#txt_primenombremama_editar").val();
  var fechanacimiento = $("#txt_fecnacimiento_editar").val();
  var signosodiacal = $("#txt_sodiaco_editar").val();
  var distritonac = $("#sel_distrito_editar").val();
  var via = $("#txt_via_editar").val();

  var distritoact = $("#sel_distrito2_editar").val();
  var referencia = $("#txt_referencia_editar").val();
  var email = $("#txt_email_editar").val();
  var moviltitular = $("#txt_movil_titular_editar").val();
  var movilcontacto = $("#txt_movil_contacto_editar").val();
  var movilcoordinacion = $("#txt_movil_coordinacion_editar").val();
  var plano = $("#txt_plano_editar").val();
  var producto = $("#cmb_producto_editar").val();
  var plan = $("#cbm_plan_editar").val();
  var cargofijo = $("#txt_cargo_fijo_editar").val();
  var costoinstalacion = $("#txt_costo_intalacion_editar").val();
  var idusuario = $("#cbm_idusuario_editar").val();
  var fullclaro = $("#cbm_fullclaro_editar").val();
  var pregunta = $("#txt_pregunta_editar").val();
  var bancamovil = $("#txt_bancamovil_editar").val();
  var diapago = $("#txt_diapago_editar").val();
  var horapago = $("#txt_horapago_editar").val();
  var promocion = $("#txt_promocion_editar").val();
  var comentario = $("#txt_comentario").val();

  var tipoventa = $("#cbm_tipoventa_editar").val();
  var play = $("#cbm_play_editar").val();
  var paquete = $("#cbm_paquete_editar").val();
  // var adicional = $("#cbm_adicional_editar").val();
  var decoadd1 = $("#cbm_decoadd1_editar").val();
  // var decoadd2 = $("#cbm_decoadd2_editar").val();
  var premium = $("#cbm_paquetepre_editar").val();
  var cargoadd = $("#txt_cargo_adicional_editar").val();
  var cargototal = $("#txt_cargo_total_editar").val();

  var va = $("#txt_va").val();
  var sot = $("#txt_sot").val();
  var set = $("#txt_sot").val();
  var comentario_estado = $("#txt_comentario_estado").val();
  var comentario_estado_venta = $("#txt_comentario_estado_venta").val();
  var fecha_instalacion = $("#txt_fecha_instalacion").val();
  var hor_iniciopro = $("#hora_inicio_instalacion").val();
  var hor_fin_pro = $("#hora_fin_instalacion").val();



  if (validad_email(email)) {

  } else {

    return Swal.fire("Mensaje de advertencia", "El formato de email es incorrecto", "warning");
  }


  $.ajax({
    url: "../controlador/venta/controlador_editar_venta.php",
    type: "POST",
    data: {
      va: va,
      sot: sot,
      set: set,
      comentario_estado: comentario_estado,
      comentario_estado_venta: comentario_estado_venta,
      fecha_instalacion: fecha_instalacion,
      hor_iniciopro: hor_iniciopro,
      hor_fin_pro: hor_fin_pro,

      rol: rol,
      estado: estado,
      idventa: idventa,
      idcliente: idcliente,
      iddireccion: iddireccion,
      iddetallepre: iddetallepre,
      idgrabacion: idgrabacion,
      movilllamada: movilllamada,
      fechallamada: fechallamada,
      horallamada: horallamada,
      tipodocumento: tipodocumento,
      nrodocumentoactual: nrodocumentoactual,
      nrodocumentonuevo: nrodocumentonuevo,
      nombre: nombre,
      fijoportar: fijoportar,
      prinompapa: prinompapa,
      prinommama: prinommama,
      fechanacimiento: fechanacimiento,
      signosodiacal: signosodiacal,
      distritonac: distritonac,
      via: via,
      distritoact: distritoact,
      referencia: referencia,
      email: email,
      moviltitular: moviltitular,
      movilcontacto: movilcontacto,
      movilcoordinacion: movilcoordinacion,
      plano: plano,
      producto: producto,
      plan: plan,
      cargofijo: cargofijo,
      costoinstalacion: costoinstalacion,
      idusuario: idusuario,
      fullclaro: fullclaro,
      pregunta: pregunta,
      bancamovil: bancamovil,
      diapago: diapago,
      horapago: horapago,
      promocion: promocion,
      comentario: comentario,

      tipoventa: tipoventa,
      play: play,
      paquete: paquete,
      // adicional: adicional,
      decoadd1: decoadd1,
      // decoadd2: decoadd2,
      premium: premium,
      cargoadd: cargoadd,
      cargototal: cargototal
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_venta.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire({
          icon: `success`,
          title: `Mensaje de confirmacion`,
          text: `Datos guardados`,
          confirmButtonText: `Ok`
        });
      } else {
        Swal.fire("Mensaje de advertencia", "El cliente ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      Swal.fire("Mensaje de error", "La actualizacion no se pudo completar", "error");
    }
  });
}

function AbrirModal() {
  $("#modal_registro").modal({ backdrop: "static", keyboard: false });
  $("#modal_registro").find(".modal-header").css("background", "#374f65");
  $("#modal_registro").find(".modal-header").css("color", "white");
  $("#modal_registro").modal("show");
  // document.getElementById("div_error").style.display = "none";
  // LimpiarModal();
}

function validad_email(email) {
  var regex = /^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  return regex.test(email) ? true : false;
}

function ChangeColor(val) {

  if (val == "SI") {
    document.getElementById("txt_va").style.backgroundColor = "#2ecc71";
    document.getElementById("txt_va").style.color = "#ffffff";
  } else if (val == "NO") {
    document.getElementById("txt_va").style.backgroundColor = "#f75959";
    document.getElementById("txt_va").style.color = "#ffffff";
  } else {
    document.getElementById("txt_va").style.backgroundColor = "#fafafa";
    document.getElementById("txt_va").style.color = "#5e5e5e";

  }
}

function ver() {
  var count = 0;
  var idusuariogeneral = $("#txt_idusuario_general").val();
  $.ajax({ //se inicia la petición ajax al archivo que consulta los mensajes en la base de datos
    type: 'POST', //consulta mediante get
    url: '../controlador/usuario/controlador_ver_mensaje.php', //url del archivo a consultar
    data: { idusuariogeneral: idusuariogeneral }, //consulta el id del propietario //se espera retornar un json
    success: function (data) {
      var datos = JSON.parse(data); //si fue satisfactorio la petición ajax retorna la variable data con la información
      console.log(datos);
      for (var index = 0; index < datos.length; index++) {
        
        if(datos[index]['notificacion_edicion_nueva'] != datos[index]['notificacion_edicion_vieja']){
          count++;
        }
        
      }
      if(count>0){
        console.log("tienes una actualizacion");
          Push.create("Hello world!", {
          body: "How's it hangin'?",
          icon: '../vista/img/logo2.png',
          timeout: 4000,
          onClick: function () {
            window.focus();
            this.close();
          }
        });
      }else{
        console.log("no tienes actualizaciones");
      }
      
        
      
    },
  });

}