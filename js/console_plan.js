var t_play;
function listar_play() {

  t_play = $("#tabla_play").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/play/controlador_play_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "play_nombre" },
      { "data": "play_fregistro" },
      {
        "data": "play_estatus",
        render: function (data, type, row) {
          if (data == 'ACTIVO') {
            return "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + data + "</span>";
          } else {
            return "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" + data + "</span>";
          }
        }
      },
      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar_play btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_play.on('draw.dt', function () {
    var PageInfo = $('#tabla_play').DataTable().page.info();
    t_play.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

var t_plan_internet;
function listar_plan_internet() {

  t_plan_internet = $("#tabla_plan_internet").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/plan_internet/controlador_plan_internet_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "plan_internet" },
      { "data": "plan_fregistro" },
      {
        "data": "plan_estatus",
        render: function (data, type, row) {
          if (data == 'ACTIVO') {
            return "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + data + "</span>";
          } else {
            return "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" + data + "</span>";
          }
        }
      },
      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar_plan btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_plan_internet.on('draw.dt', function () {
    var PageInfo = $('#tabla_plan_internet').DataTable().page.info();
    t_plan_internet.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

var t_plan_paquetes;
function listar_paquetes() {
  var producto = $('#cmb_producto_listar option:selected').html();
                  
  var play = $('#cbm_play_listar option:selected').html();
              
  var plan = $('#cbm_plan_listar option:selected').html();
  

  t_plan_paquetes = $("#tabla_plan_paquetes").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/paquetes/controlador_plan_paquetes_listar.php",
      "data": {
        producto: producto,
        play: play,
        plan: plan
      }
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "producto_nombre" },
      { "data": "play_nombre" },
      { "data": "plan_internet" },
      { "data": "paquete_nombre" },
      { "data": "paquete_precio" },

      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar_paquete btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      // $($(nRow).find("td")[4]).css('text-align', 'left' );
    },
    "language": idioma_espanol,
    select: true
  });
  t_plan_paquetes.on('draw.dt', function () {
    var PageInfo = $('#tabla_plan_paquetes').DataTable().page.info();
    t_plan_paquetes.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

var t_decoadd1;
function listar_add1() {

  t_decoadd1 = $("#tabla_decoadd1").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/decoadd1/controlador_decoadd1_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "decoadd1_nombre" },
      { "data": "decoadd1_precio" },
      {
        "data": "decoadd1_fregistro"
      },
      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_decoadd1.on('draw.dt', function () {
    var PageInfo = $('#tabla_decoadd1').DataTable().page.info();
    t_decoadd1.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

$('#tabla_decoadd1').on('click', '.eliminar', function () {
  var data = t_decoadd1.row($(this).parents('tr')).data();
  if (t_decoadd1.row(this).child.isShown()) {
    var data = t_decoadd1.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/plan/decoadd1/controlador_deco1_eliminar.php",
        data: {
          id: data.decoadd1_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_decoadd1.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

var t_decoadd2;
function listar_add2() {

  t_decoadd2 = $("#tabla_decoadd2").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/decoadd2/controlador_decoadd2_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "decoadd2_nombre" },
      { "data": "decoadd2_precio" },
      {
        "data": "decoadd2_fregistro"
      },
      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_decoadd2.on('draw.dt', function () {
    var PageInfo = $('#tabla_decoadd2').DataTable().page.info();
    t_decoadd2.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

var t_paquete_premium;
function listar_premium() {

  t_paquete_premium = $("#tabla_paquete_premium").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/plan/premium/controlador_premium_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "paqpremium_nombre" },
      { "data": "paqpremium_precio" },
      {
        "data": "paqpremium_fregistro"
      },
      { "defaultContent": "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_paquete_premium.on('draw.dt', function () {
    var PageInfo = $('#tabla_paquete_premium').DataTable().page.info();
    t_paquete_premium.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

function Registrar_Premium() {
  var paqpremium = $("#txt_paquete_premium").val();
  var precio = $("#txt_precio").val();


  if (paqpremium.length == 0 || precio.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/premium/controlador_premium_registro.php',
    type: 'POST',
    data: {
      paqpremium: paqpremium,
      precio: precio
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_paquete_premium.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El paquete premium ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}

$('#tabla_paquete_premium').on('click','.editar',function(){
  var data = t_paquete_premium.row($(this).parents('tr')).data();
  if(t_paquete_premium.row(this).child.isShown()){
      var data = t_paquete_premium.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txt_idpremium").val(data.paqpremium_id);
  $("#txt_premium_actual").val(data.paqpremium_nombre);
  $("#txt_premium_nuevo").val(data.paqpremium_nombre);
  $("#txt_precio_premium_editar").val(data.paqpremium_precio);
  
})

function Editar_Paquete_Premium(){
  var id = $("#txt_idpremium").val();
  var premiumactual = $("#txt_premium_actual").val();
  var premiumnuevo = $("#txt_premium_nuevo").val();
  var precio = $("#txt_precio_premium_editar").val();

  if (id.length==0 || premiumactual.length==0 || premiumnuevo.length==0 || precio.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/premium/controlador_editar_premium.php',
    type: 'POST',
    data:{
      id:id,
      premiumactual:premiumactual,
      premiumnuevo:premiumnuevo,
      precio:precio
    }
  }).done(function(resp){
    if (resp>0) {
      if (resp==1) {
        t_paquete_premium.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El paquete ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}
$('#tabla_paquete_premium').on('click', '.eliminar', function () {
  var data = t_paquete_premium.row($(this).parents('tr')).data();
  if (t_paquete_premium.row(this).child.isShown()) {
    var data = t_paquete_premium.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/plan/premium/controlador_premium_eliminar.php",
        data: {
          id: data.paqpremium_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_paquete_premium.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

// inicio funciones deco1
function Registrar_Deco_Add() {
  var deco = $("#txt_adicional").val();
  var precio = $("#txt_precio").val();


  if (deco.length == 0 || precio.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/decoadd1/controlador_decoadd1_registro.php',
    type: 'POST',
    data: {
      deco: deco,
      precio: precio
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_decoadd1.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El deco ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}
$('#tabla_decoadd1').on('click','.editar',function(){
  var data = t_decoadd1.row($(this).parents('tr')).data();
  if(t_decoadd1.row(this).child.isShown()){
      var data = t_decoadd1.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txt_iddeco1").val(data.decoadd1_id);
  $("#txt_adicional_actual").val(data.decoadd1_nombre);
  $("#txt_adicional_nuevo").val(data.decoadd1_nombre);
  $("#txt_precio_editar").val(data.decoadd1_precio);
  
})

function Editar_Deco1(){
  var id = $("#txt_iddeco1").val();
  var deco1actual = $("#txt_adicional_actual").val();
  var deco1nuevo = $("#txt_adicional_nuevo").val();
  var precio = $("#txt_precio_editar").val();

  if (id.length==0 || deco1actual.length==0 || deco1nuevo.length==0 || precio.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/decoadd1/controlador_editar_deco1.php',
    type: 'POST',
    data:{
      id:id,
      deco1actual:deco1actual,
      deco1nuevo:deco1nuevo,
      precio:precio
    }
  }).done(function(resp){
    if (resp>0) {
      if (resp==1) {
        t_decoadd1.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El deco ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}
// fin funciones deco1

// inicio funciones deco2
function Registrar_Deco_Add2() {
  var deco = $("#txt_adicional").val();
  var precio = $("#txt_precio").val();


  if (deco.length == 0 || precio.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/decoadd2/controlador_decoadd2_registro.php',
    type: 'POST',
    data: {
      deco: deco,
      precio: precio
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_decoadd2.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El deco ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}
$('#tabla_decoadd2').on('click','.editar',function(){
  var data = t_decoadd2.row($(this).parents('tr')).data();
  if(t_decoadd2.row(this).child.isShown()){
      var data = t_decoadd2.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txt_iddeco2").val(data.decoadd2_id);
  $("#txt_adicional2_actual").val(data.decoadd2_nombre);
  $("#txt_adicional2_nuevo").val(data.decoadd2_nombre);
  $("#txt_precio2_editar").val(data.decoadd2_precio);
  
})

function Editar_Deco2(){
  var id = $("#txt_iddeco2").val();
  var deco2actual = $("#txt_adicional2_actual").val();
  var deco2nuevo = $("#txt_adicional2_nuevo").val();
  var precio = $("#txt_precio2_editar").val();

  if (id.length==0 || deco2actual.length==0 || deco2nuevo.length==0 || precio.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/decoadd2/controlador_editar_deco2.php',
    type: 'POST',
    data:{
      id:id,
      deco2actual:deco2actual,
      deco2nuevo:deco2nuevo,
      precio:precio
    }
  }).done(function(resp){
    if (resp>0) {
      if (resp==1) {
        t_decoadd2.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El deco ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}
// fin funciones deco2

// funciones de play
function Registrar_Play() {
  var play = $("#txt_play").val();


  if (play.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/play/controlador_play_registro.php',
    type: 'POST',
    data: {
      play: play
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_play.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El play ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}

$('#tabla_play').on('click','.editar',function(){
  var data = t_play.row($(this).parents('tr')).data();
  if(t_play.row(this).child.isShown()){
      var data = t_play.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txtidplay").val(data.play_id);
  $("#txt_play_actual_editar").val(data.play_nombre);
  $("#txt_play_nuevo_editar").val(data.play_nombre);
  $("#cbm_estatus").val(data.play_estatus).trigger("change");
  
})

$('#tabla_play').on('click', '.eliminar_play', function () {
  var data = t_play.row($(this).parents('tr')).data();
  if (t_play.row(this).child.isShown()) {
    var data = t_play.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/plan/play/controlador_play_eliminar.php",
        data: {
          id: data.play_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_play.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error, intente de nuevo',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

function Editar_Play(){
  var id = $("#txtidplay").val();
  var playactual = $("#txt_play_actual_editar").val();
  var playnuevo = $("#txt_play_nuevo_editar").val();
  var estatus = $("#cbm_estatus").val();

  if (id.length==0 || playactual.length==0 || playnuevo.length==0 || estatus.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/play/controlador_editar_play.php',
    type: 'POST',
    data:{
      id:id,
      playactual:playactual,
      playnuevo:playnuevo,
      estatus:estatus
    }
  }).done(function(resp){  
    console.log(resp);
    if (resp>0) {
      if (resp==1) {
        t_play.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El producto ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}

//fin de funciones de play

// inicio funciones plan internet
function Registrar_Plan_Internet() {
  var plan = $("#txt_plan_internet").val();


  if (plan.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/plan_internet/controlador_plan_intenet_registro.php',
    type: 'POST',
    data: {
      plan: plan
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_plan_internet.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El plan ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}
$('#tabla_plan_internet').on('click','.editar',function(){
  var data = t_plan_internet.row($(this).parents('tr')).data();
  if(t_plan_internet.row(this).child.isShown()){
      var data = t_plan_internet.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txtidplaninternet").val(data.plan_internetid);
  $("#txt_planinter_actual_editar").val(data.plan_internet);
  $("#txt_planinter_nuevo_editar").val(data.plan_internet);
  $("#cbm_estatus").val(data.plan_estatus).trigger("change");
  
})

function Editar_Plan_Internet(){
  var id = $("#txtidplaninternet").val();
  var planinteractual = $("#txt_planinter_actual_editar").val();
  var planinternuevo = $("#txt_planinter_nuevo_editar").val();
  var estatus = $("#cbm_estatus").val();

  if (id.length==0 || planinteractual.length==0 || planinternuevo.length==0 || estatus.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/plan_internet/controlador_editar_plan_inter.php',
    type: 'POST',
    data:{
      id:id,
      planinteractual:planinteractual,
      planinternuevo:planinternuevo,
      estatus:estatus
    }
  }).done(function(resp){
    if (resp>0) {
      if (resp==1) {
        t_plan_internet.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El plan ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}

$('#tabla_plan_internet').on('click', '.eliminar_plan', function () {
  var data = t_plan_internet.row($(this).parents('tr')).data();
  if (t_plan_internet.row(this).child.isShown()) {
    var data = t_plan_internet.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/plan/plan_internet/controlador_plan_eliminar.php",
        data: {
          id: data.plan_internetid
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_play.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error, intente de nuevo',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

// fin funciones plan internet



// inicio funciones de paquetes
function Registrar_Paquete() {
  var producto = $("#cmb_producto").val();
  var play = $("#cbm_play").val();
  var plan = $("#cbm_plan").val();
  var paquete = $("#txt_paquete").val();
  var precio = $("#txt_precio").val();

  var productonombre = $('#cmb_producto option:selected').html();
  var playnombre = $('#cbm_play option:selected').html();
  var plannombre = $('#cbm_plan option:selected').html();


  if (producto.length == 0 || play.length == 0 || plan.length == 0 || paquete.length == 0 || precio.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/plan/paquetes/controlador_paquete_registro.php',
    type: 'POST',
    data: {
      producto: producto,
      play: play,
      plan: plan,
      paquete: paquete,
      precio: precio,
      productonombre: productonombre,
      playnombre: playnombre,
      plannombre: plannombre
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_plan_paquetes.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El paquete ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}
$('#tabla_plan_paquetes').on('click','.editar',function(){
  var data = t_plan_paquetes.row($(this).parents('tr')).data();
  if(t_plan_paquetes.row(this).child.isShown()){
      var data = t_plan_paquetes.row(this).data();
  }
  $("#modal_editar").modal({backdrop: 'static' , Keyboard:false});
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txtidpaquete").val(data.paquete_id);
  $('#cmb_producto_editar option:selected').html(data.producto).trigger("change");
  // $("#cmb_producto_editar").val(data.producto_id).trigger("change");
  // $("#cbm_play_editar").val(data.play_id).trigger("change");
  $('#cbm_play_editar option:selected').html(data.play).trigger("change");
  // $("#cbm_plan_editar").val(data.plan_internetid).trigger("change");
  $('#cbm_plan_editar option:selected').html(data.plan).trigger("change");
  $("#txt_paquete_actual_editar").val(data.paquete_nombre);
  $("#txt_paquete_nuevo_editar").val(data.paquete_nombre);
  $("#txt_precio_editar").val(data.paquete_precio);
  
})

$('#tabla_plan_paquetes').on('click', '.eliminar_paquete', function () {
  var data = t_plan_paquetes.row($(this).parents('tr')).data();
  if (t_plan_paquetes.row(this).child.isShown()) {
    var data = t_plan_paquetes.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/plan/paquetes/controlador_paquete_eliminar.php",
        data: {
          id: data.paquete_id 
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_plan_paquetes.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error, intente de nuevo',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

function Editar_Paquetes(){
  var id = $("#txtidpaquete").val();
  var producto = $("#cmb_producto_editar").val();
  var play = $("#cbm_play_editar").val();
  var plan = $("#cbm_plan_editar").val();
  var paqueteactual = $("#txt_paquete_actual_editar").val();
  var paquetenuevo = $("#txt_paquete_nuevo_editar").val();
  var precio = $("#txt_precio_editar").val();

  if (id.length==0 || producto.length==0 || play.length==0 || plan.length==0 || paqueteactual.length==0 || paquetenuevo.length==0 || precio.length==0) {
    Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning")
  }

  $.ajax({
    url: '../controlador/plan/paquetes/controlador_editar_paquetes.php',
    type: 'POST',
    data:{
      id:id,
      producto:producto,
      play:play,
      plan:plan,
      paqueteactual:paqueteactual,
      paquetenuevo:paquetenuevo,
      precio:precio
    }
  }).done(function(resp){
    if (resp>0) {
      if (resp==1) {
        t_plan_paquetes.ajax.reload();
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion","Datos actualizados","success");
    }else{
      Swal.fire("Mensaje de advertencia","El plan ingresado ya se encuentra en la base de datos","warning");
    }
    }else{
      Swal.fire("Mensaje de error","La actulizacion no se pudo completar","error");
    }
  
  })
}
// fin funciones de paquetes

function AbrirModal() {
  $("#modal_registro").modal({ backdrop: 'static', keyboard: false });
  $("#modal_registro").find(".modal-header").css("background", "#854c35");
  $("#modal_registro").find(".modal-header").css("color", "white");
  $("#modal_registro").modal('show');
  LimpiarModal_Play();
  LimpiarModal_Paquetes();
  LimpiarModal_Plan();
  LimpiarModal_Decoadd();
  LimpiarModal_Premium();

}

function LimpiarModal_Play() {
  $("#txt_play").val("");
}
function LimpiarModal_Paquetes() {
  $("#txt_paquete").val("");
  $("#txt_precio").val("");
}
function LimpiarModal_Plan() {
  $("#txt_plan_internet").val("");
}
function LimpiarModal_Decoadd() {
  $("#txt_adicional").val("");
  $("#txt_precio").val("");
}
function LimpiarModal_Premium() {
  $("#txt_paquete_premium").val("");
}


function listar_producto() {
  $.ajax({
    url: '../controlador/venta/controlador_listar_producto.php',
    type: 'POST'
  }).done(function (resp) {
    var data = JSON.parse(resp);
    var cadena = "";
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

      }
      $("#cmb_producto").html(cadena);
      $("#cmb_producto_listar").html(cadena);
      $("#cmb_producto_editar").html(cadena);
    } else {
      cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
      $("#cmb_producto").html(cadena);
      $("#cmb_producto_listar").html(cadena);
      $("#cmb_producto_editar").html(cadena);
    }
  })
}


function listar_play_combo() {
  $.ajax({
    url: '../controlador/plan/play/controlador_listar_combo_play.php',
    type: 'POST'
  }).done(function (resp) {

    var data = JSON.parse(resp);
    var cadena = "";
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

      }
      $("#cbm_play").html(cadena);
      $("#cbm_play_listar").html(cadena);
      $("#cbm_play_editar").html(cadena);
    } else {
      cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
      $("#cbm_play").html(cadena);
      $("#cbm_play_listar").html(cadena);
      $("#cbm_play_editar").html(cadena);
    }
  })
}
function listar_plan_internet_combo() {
  $.ajax({
    url: '../controlador/plan/plan_internet/controlador_listar_combo_plan.php',
    type: 'POST'
  }).done(function (resp) {

    var data = JSON.parse(resp);
    var cadena = "";
    if (data.length > 0) {
      for (var i = 0; i < data.length; i++) {
        cadena += "<option value='" + data[i][0] + "'>" + data[i][1] + "</option>";

      }
      $("#cbm_plan").html(cadena);
      $("#cbm_plan_listar").html(cadena);
      $("#cbm_plan_editar").html(cadena);
    } else {
      cadena += "<option value=''>'NO SE ENCONTRARON REGISTROS'</option>";
      $("#cbm_plan").html(cadena);
      $("#cbm_plan_listar").html(cadena);
      $("#cbm_plan_editar").html(cadena);
    }
  })
}