var t_rol;
function listar_rol() {

  t_rol = $("#tabla_rol").DataTable({
    "ordering": true,
    "pageLength": 10,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/rol/controlador_rol_listar.php",
    },
    "columns": [
      { "defaultContent": "" },
      { "data": "rol_nombre" },
      { "data": "rol_feregistro" },
      {
        "data": "rol_estatus",
        render: function (data, type, row) {
          if (data == 'ACTIVO') {
            return "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" + data + "</span>";
          } else {
            return "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" + data + "</span>";
          }
        }
      },
      { "defaultContent": "<button style='font-size:13px;' type='button' class='permisos btn btn-secondary '><i class='fa fa-key'> </i></button>&nbsp<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button>" }

    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[4]).css('text-align', 'left');
    },
    "language": idioma_espanol,
    select: true
  });
  t_rol.on('draw.dt', function () {
    var PageInfo = $('#tabla_rol').DataTable().page.info();
    t_rol.column(0, { page: 'current' }).nodes().each(function (cell, i) {
      cell.innerHTML = i + 1 + PageInfo.start;
    });
  });

}

$('#tabla_rol').on('click', '.editar', function () {
  var data = t_rol.row($(this).parents('tr')).data();
  if (t_rol.row(this).child.isShown()) {
    var data = t_rol.row(this).data();
  }
  $("#modal_editar").modal({ backdrop: 'static', Keyboard: false });
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txtidrol").val(data.rol_id);
  $("#txt_rol_actual_editar").val(data.rol_nombre);
  $("#txt_rol_nuevo_editar").val(data.rol_nombre);
  $("#cbm_estatus").val(data.rol_estatus).trigger("change");

})

$('#tabla_rol').on('click', '.eliminar', function () {
  var data = t_rol.row($(this).parents('tr')).data();
  if (t_rol.row(this).child.isShown()) {
    var data = t_rol.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/rol/controlador_rol_eliminar.php",
        data: {
          id: data.rol_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_rol.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})



$('#tabla_rol').on('click', '.permisos', function () {
  var data = t_rol.row($(this).parents('tr')).data();
  if (t_rol.row(this).child.isShown()) {
    var data = t_rol.row(this).data();
  }
  $("#modal_permisos").modal({ backdrop: 'static', Keyboard: false });
  $("#modal_permisos").find(".modal-header").css("background", "#854c35");
  $("#modal_permisos").find(".modal-header").css("color", "white");
  $("#modal_permisos").modal("show");
  $("#txtidrol_permiso").val(data.rol_id);
  var idrol_permiso = $("#txtidrol_permiso").val();
  listar_permisos(idrol_permiso);

})

function AbrirModal() {
  $("#modal_registro").modal({ backdrop: 'static', keyboard: false });
  $("#modal_registro").find(".modal-header").css("background", "#854c35");
  $("#modal_registro").find(".modal-header").css("color", "white");
  $("#modal_registro").modal('show');
}

function Registrar_Rol() {
  var rol = $("#txt_rol").val();


  if (rol.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene el campo vacio", "warning")
  }

  $.ajax({
    url: '../controlador/rol/controlador_registro.php',
    type: 'POST',
    data: {
      rol: rol
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_rol.ajax.reload();
        $("#modal_registro").modal('hide');
        return Swal.fire("Mensaje de confirmacion", "Datos guardados", "success");
      } else {
        return Swal.fire("Mensaje de advertencia", "El rol ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      return Swal.fire("Mensaje de error", "El registro no se pudo completar", "error");
    }

  })
}

function Editar_Rol() {
  var id = $("#txtidrol").val();
  var rolactual = $("#txt_rol_actual_editar").val();
  var rolnuevo = $("#txt_rol_nuevo_editar").val();
  var estatus = $("#cbm_estatus").val();

  if (id.length == 0 || rolactual.length == 0 || rolnuevo.length == 0 || estatus.length == 0) {
    return Swal.fire("Mensaje de advertencia", "Llene los campos vacios", "warning")
  }

  $.ajax({
    url: '../controlador/rol/controlador_editar_rol.php',
    type: 'POST',
    data: {
      id: id,
      rolactual: rolactual,
      rolnuevo: rolnuevo,
      estatus: estatus
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        
        $("#modal_editar").modal('hide');
        Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
      } else {
        Swal.fire("Mensaje de advertencia", "El rol ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      Swal.fire("Mensaje de error", "La actulizacion no se pudo completar", "error");
    }

  })
}
var t_permisos;
function listar_permisos(variable) {

  t_permisos = $("#tabla_permisos").DataTable({
    "ordering": false,
    "pageLength": 10,
    "bPaginate": false,
    bLengthChange: false,
    searching: false,
    tfoot: false,
    paging: false,
    bInfo: false,
    "destroy": true,
    "async": false,
    "responsive": true,
    "autoWidth": false,
    "ajax": {
      "method": "POST",
      "url": "../controlador/rol/controlador_permisos_listar.php",
      "data": {
        variable: variable
      }
    },
    "columns": [
      {
        "data": "idmodulo",
        render: function (data, type, row) {


          return "<div class='toggle-flip'><label >" + data + "<input id='idmodulo' type='hidden' name='permisos[" + row['idmodulo'] + "][idmodulo]' value='" + data + "' required=''>" + "</label></div>";

        }
      },
      {
        "data": "idpermiso",
        render: function (data, type, row) {


          return "<div class='toggle-flip'><label >" + data + "<input id='idmodulo' type='hidden' name='permisos[" + row['idmodulo'] + "][idpermiso]' value='" + data + "' required=''>" + "</label></div>";

        }
      },
      {
        "data": "titulo",
        render: function (data, type, row) {

          return "<div class='toggle-flip'><label >" + data + "<input type='hidden' id='nombremodulo' name='permisos[" + row['idmodulo'] + "][titulo]'  value='" + data + "' required=''>" + "</label></div>";

        }
      },
      {
        "data": "r",
        render: function (data, type, row) {
          var checked = "";

          if (data == 1) {
            checked = "checked";

          } else {
            checked = "";

          }
          return "<div class='form-check form-switch'><input id='v' name='permisos[" + row['idmodulo'] + "][r]'  type='checkbox' " + checked + "  data-toggle='toggle' data-onstyle='secondary'></div>";




        }
      },
      {
        "data": "w",
        render: function (data, type, row) {

          var checked = "";

          if (data == 1) {
            checked = "checked";

          } else {
            checked = "";

          }
          return "<input id='w' name='permisos[" + row['idmodulo'] + "][w]'  type='checkbox' " + checked + " ><span class='flip-indecator' data-toggle-on='ON' data-toggle-off='OFF'></span>";

        }
      },
      {
        "data": "u",
        render: function (data, type, row) {

          var checked = "";

          if (data == 1) {
            checked = "checked";

          } else {
            checked = "";

          }
          return "<input id='u' name='permisos[" + row['idmodulo'] + "][u]'  type='checkbox' " + checked + " ><span class='flip-indecator' data-toggle-on='ON' data-toggle-off='OFF'></span>";


        }
      },
      {
        "data": "d",
        render: function (data, type, row) {

          var checked = "";

          if (data == 1) {
            checked = "checked";

          } else {
            checked = "";

          }
          return "<input id='d' name='permisos[" + row['idmodulo'] + "][d]'  type='checkbox' " + checked + " ><span class='flip-indecator' data-toggle-on='ON' data-toggle-off='OFF'></span>";


        }
      },
    ],
    "fnRowCallback": function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {

    },
    "language": idioma_espanol,
    select: false
  });
  $('#tabla_permisos tfoot').prop("hidden", true);
  // t_rol.on( 'draw.dt', function () {
  //       var PageInfo = $('#tabla_rol').DataTable().page.info();
  //       t_rol.column(0, { page: 'current' }).nodes().each( function (cell, i) {
  //               cell.innerHTML = i + 1 + PageInfo.start;
  //           } );
  //       } );
  // t_permisos.column(0).visible(false);
  t_permisos.column(1).visible(false);



}

function Editar_Permisos() {

  $.ajax({
    url: '../controlador/rol/controlador_editar_permisos.php',
    type: 'POST',
    data: {
      id: id,
      modulo: modulo,
      ver: ver,
      crear: crear,
      actualizar: actualizar,
      eliminar: eliminar
    }
  }).done(function (resp) {
    console.log(resp);
    if (resp > 0) {
      if (resp == 1) {
        t_rol.ajax.reload();
        $("#modal_permisos").modal('hide');
        Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
      } else {
        Swal.fire("Mensaje de advertencia", "El rol ingresado ya se encuentra en la base de datos", "warning");
      }
    } else {
      Swal.fire("Mensaje de error", "La actulizacion no se pudo completar", "error");
    }

  })
}


function obtener_datos_datatable() {
  $("#tabla_permisos").each(function () {
    let dt = $('#tabla_permisos').DataTable();
    let checkeds = dt.data().toArray();
    console.log(checkeds);

    var check = $(this).find("input[type='checkbox']").is(':checked');

    console.log(check);
  })

}

function registrarPermisos(e) {
  e.preventDefault();
  
  var formData = new FormData(document.getElementById("formPermisos"));
  $.ajax({
    url: "../controlador/rol/controlador_editar_permisos.php",
    type: 'POST',
    data: formData,
    contentType: false,
    processData: false,
    success: function (resp) {
      console.log(resp);
      if (resp > 0) {
        if (resp == 1) {
          t_rol.ajax.reload();
          t_permisos.ajax.reload();
          
          $("modal_permisos").modal('hide');
          Swal.fire("Mensaje de confirmacion", "Datos actualizados", "success");
        } 
      } else {
        Swal.fire("Mensaje de error", "La actulizacion no se pudo completar", "error");
      }
    }
  });
  return false;
}

