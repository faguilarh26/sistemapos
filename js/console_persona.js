var t_persona;
function listar_persona() {
  t_persona = $("#tabla_persona").DataTable({
    ordering: true,
    pageLength: 10,
    destroy: true,
    async: false,
    responsive: true,
    autoWidth: false,
    ajax: {
      method: "POST",
      url: "../controlador/persona/controlador_persona_listar.php",
    },
    columns: [
      { defaultContent: "" },
      { data: "persona" },
      { data: "persona_nrodocumento" },
      { data: "persona_tipodocumento" },
      {
        data: "persona_sexo",
        render: function (data, type, row) {
          if (data == "MASCULINO") {
            return "<i class='fa fa-male' aria-hidden='true'></i>";
          } else {
            return "<i class='fa fa-female' aria-hidden='true'></i>";
          }
        },
      },
      { data: "persona_telefono" },
      {
        data: "persona_estatus",
        render: function (data, type, row) {
          if (data == "ACTIVO") {
            return (
              "<span class='badge badge-success badge-pill m-r-5 m-b-5'>" +
              data +
              "</span>"
            );
          } else {
            return (
              "<span class='badge badge-danger badge-pill m-r-5 m-b-5'>" +
              data +
              "</span>"
            );
          }
        },
      },
      {
        defaultContent:
          "<button class='editar btn btn-primary'><i class='fa fa-edit'></i></button>&nbsp<button class='editar_password btn btn-warning'><i class='fa fa-unlock'></i></button>&nbsp<button class='resetear_password btn btn-danger'><i class='fa fa-unlock-alt'></i></button>&nbsp<button class='eliminar btn btn-danger'><i class='fa fa-trash'></i></button> ",
      },
    ],
    fnRowCallback: function (nRow, aData, iDisplayIndex, iDisplayIndexFull) {
      $($(nRow).find("td")[3]).css("text-align", "center");
      $($(nRow).find("td")[4]).css("text-align", "center");
    },
    language: idioma_espanol,
    select: true,
  });
  t_persona.on("draw.dt", function () {
    var PageInfo = $("#tabla_persona").DataTable().page.info();
    t_persona
      .column(0, { page: "current" })
      .nodes()
      .each(function (cell, i) {
        cell.innerHTML = i + 1 + PageInfo.start;
      });
  });
}

$('#tabla_persona').on('click', '.editar', function () {
  var data = t_persona.row($(this).parents('tr')).data();
  if (t_persona.row(this).child.isShown()) {
    var data = t_persona.row(this).data();
  }
  $("#modal_editar").modal({ backdrop: 'static', Keyboard: false });
  $("#modal_editar").find(".modal-header").css("background", "#854c35");
  $("#modal_editar").find(".modal-header").css("color", "white");
  $("#modal_editar").modal("show");
  $("#txtidpersona").val(data.persona_id);
  $("#txtidpersona_pago").val(data.pagopers_id);
  $("#txtidusuario").val(data.usuario_id);
  $("#txtnombre_editar").val(data.persona_nombre);
  $("#txtapepat_editar").val(data.persona_apepat);
  $("#txtapemat_editar").val(data.persona_apemat);
  $("#txtnro_editar_actual").val(data.persona_nrodocumento);
  $("#txtnro_editar_nuevo").val(data.persona_nrodocumento);
  $("#txttelefono_editar").val(data.persona_telefono);
  $("#cbm_tdocumento_editar").val(data.persona_tipodocumento).trigger("change");
  $("#cbm_sexo_editar").val(data.persona_sexo).trigger("change");
  $("#cbm_estatus").val(data.persona_estatus).trigger("change");
  $("#txt_fecnacimiento_editar").val(data.persona_fenacimiento);
  $("#txt_fecontratacion_editar").val(data.persona_fecontratacion);
  $("#txt_fecingreso_editar").val(data.persona_feingreso);
  $("#txt_fecsalida_editar").val(data.persona_fesalida);
  $("#txtmotivosalida_editar").val(data.persona_motivosalida);
  $("#txt_fecfulltime_editar").val(data.persona_feiniciofulltime);
  $("#cbm_banco_editar").val(data.pagopers_banco).trigger("change");
  $("#txtnrocuentaahorro_editar").val(data.pagopers_nrocuentaaho);
  $("#txtinterbancario_editar").val(data.pagopers_codinterbancario);
  $("#cbm_tipopago_editar").val(data.pagopers_tippago).trigger("change");
  $("#cbm_esquemapago_editar").val(data.pagopers_esquemapago).trigger("change");
  $("#txtruc_editar").val(data.pagopers_ruc);
  $("#txt_clavesol1_editar_actual").val(data.pagopers_clavesol1);
  $("#txt_clavesol1_editar_nuevo").val(data.pagopers_clavesol1);
  $("#txt_clavesol2_editar_actual").val(data.pagopers_clavesol2);
  $("#txt_clavesol2_editar_nuevo").val(data.pagopers_clavesol2);
  $("#txt_email_editar_actual").val(data.usuario_email);
  $("#txt_email_editar_nuevo").val(data.usuario_email);
  $("#cbm_rol_editar").val(data.rol_id).trigger("change");
  $("#cbm_responsable_editar").val(data.persona_responsable).trigger("change");
  $("#cbm_zona_editar").val(data.persona_zona).trigger("change");
})

$('#tabla_persona').on('click', '.eliminar', function () {
  var data = t_persona.row($(this).parents('tr')).data();
  if (t_persona.row(this).child.isShown()) {
    var data = t_persona.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de eliminar el registro?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, eliminar!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/persona/controlador_persona_eliminar.php",
        data: {
          id: data.persona_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Registro eliminado',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_persona.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})

$('#tabla_persona').on('click', '.resetear_password', function () {
  var data = t_persona.row($(this).parents('tr')).data();
  if (t_persona.row(this).child.isShown()) {
    var data = t_persona.row(this).data();
  }
  Swal.fire({
    title: '¿Esta seguro de restablecer a la contrase\u00f1a por default?',
    text: "No podrás revertir esto!",
    type: 'warning',
    showCancelButton: true,
    confirmButtonColor: '#3085d6',
    cancelButtonColor: '#d33',
    confirmButtonText: 'Si, restablecer!'
  }).then((result) => {
    if (result.value) {
      $.ajax({
        type: "POST",
        url: "../controlador/persona/controlador_persona_restablecer_contra.php",
        data: {
          id: data.usuario_id
        },
        }).done(function (resp) {
          if (resp == 1) {
            Swal.fire({
              icon: 'success',
              title: 'Contrase\u001a restablecida',
              showConfirmButton: false,
              timer: 1500
            })
            
            t_persona.ajax.reload();
          } else {
            Swal.fire({
              icon: 'error',
              title: 'Ocurrio un error',
              showConfirmButton: false,
              timer: 1500
            })
          }
        }
      )}
  })

})


$('#tabla_persona').on('click', '.editar_password', function () {
  var data = t_persona.row($(this).parents('tr')).data();
  if (t_persona.row(this).child.isShown()) {
    var data = t_persona.row(this).data();
  }
  $("#modal_editar_contra").modal({ backdrop: 'static', Keyboard: false });
  $("#modal_editar_contra").find(".modal-header").css("background", "#854c35");
  $("#modal_editar_contra").find(".modal-header").css("color", "white");
  $("#modal_editar_contra").modal("show");
  $("#txtidpersona_contra").val(data.persona_id);
  $("#txtcontrbd").val(data.usuario_password);
  $("#txtusuarioid").val(data.usuario_id);
  
})

function editarContra() {
  var idusuario = $("#txtusuarioid").val();
  var contrabd = $("#txtcontrbd").val();
  var contraactual = $("#txt_contra_actual").val();
  var contranueva = $("#txt_contra_nueva").val();
  var repetircontra = $("#txt_repetir_contra").val();

  

  if(contraactual.length==0 || contranueva.length==0 || repetircontra.length==0){
    return Swal.fire("Mensaje de advertencia","Llene los campos vacios","warning");
  }
  if(contranueva != repetircontra){
    return Swal.fire("Mensaje de advertencia","No coicide la nueva clave con la repetida","warning");
  }

  $.ajax({
    url: "../controlador/persona/controlador_editar_contra_usuario.php",
    type: "POST",
    data: {
      idusuario: idusuario,
      contrabd: contrabd,
      contraactual: contraactual,
      contranueva: contranueva,
      repetircontra: repetircontra
    },
  }).done(function (resp) {
    console.log(resp);
      if (resp > 0) {
        
        if (resp == 1) {
          t_persona.ajax.reload();
          $("#modal_editar_contra").modal("hide");
          return Swal.fire(
            {
              icon: `success`,
              title: `Mensaje de confirmacion`,
              text: `Datos actualizados`
            }
          ).then( (value) => {
            listar_persona();
          });
          LimpiarModalEditarContra();
        } else {
          return Swal.fire(
            "Mensaje de advertencia",
            "La contrase\u00f1a no coicide con la que tenemos en la base de datos",
            "warning"
          );
        }
      } else {
        Swal.fire(
          "Mensaje De Error",
          "Lo sentimos, no se pudo actualizar la contrase\u00f1a",
          "error"
        );
      }
    
  });

  
}


function Registrar_Persona() {
  var nombre = $("#txtnombre").val();
  var apepat = $("#txtapepat").val();
  var apemat = $("#txtapemat").val();
  var nrodocumento = $("#txtnro").val();
  var tipdocumento = $("#cbm_tdocumento").val();
  var sexo = $("#cbm_sexo").val();
  var telefono = $("#txttelefono").val();

  if (
    nombre.length == 0 ||
    apepat.length == 0 ||
    apemat.length == 0 ||
    nrodocumento.length == 0 ||
    tipdocumento.length == 0 ||
    sexo.length == 0 ||
    telefono.length == 0
  ) {
    MensajeError(
      nombre,
      apepat,
      apemat,
      nrodocumento,
      tipdocumento,
      sexo,
      telefono,
      'div_error'
    );
    return Swal.fire(
      "Mensaje de advertencia",
      "Llene el campo vacio",
      "warning"
    );
  }

  $.ajax({

    url: "../controlador/persona/controlador_username.php",
    type: "POST",
    data: {
      n: nombre,
      apt: apepat,
      apm: apemat,
      nro: nrodocumento,
      tip: tipdocumento,
      sex: sexo,
      tel: telefono,
    },
  }).done(function (resp) {
    console.log(resp);
    if (isNaN(resp)) {
      document.getElementById("div_error").style.display = "block";
      document.getElementById("div_error").innerHTML =
        "<strong>Revise los siguientes campos:</strong><br>" + resp;
    } else {
      if (resp > 0) {
        document.getElementById("div_error").style.display = "none";
        document.getElementById("div_error").innerHTML =
          "";
        if (resp == 1) {
          LimpiarModal();
          t_persona.ajax.reload();
          $("#modal_registro").modal("hide");
          return Swal.fire(
            "Mensaje de confirmacion",
            "Datos guardados",
            "success"
          );
        } else {
          return Swal.fire(
            "Mensaje de advertencia",
            "El usuario ingresado ya se encuentra en la base de datos",
            "warning"
          );
        }
      } else {
        Swal.fire(
          "Mensaje De Error",
          "Lo sentimos, no se pudo completar el registro",
          "error"
        );
      }
    }
  });
}
function Registrar_Persona_Prueba() {
  var nombre = $("#txtnombre").val();
  var apepat = $("#txtapepat").val();
  var apemat = $("#txtapemat").val();
  var nrodocumento = $("#txtnro").val();
  var tipdocumento = $("#cbm_tdocumento").val();
  var sexo = $("#cbm_sexo").val();
  var telefono = $("#txttelefono").val();
  var email = $("#txt_email").val();
  
  var idrol = $("#cbm_rol").val();

  var fechanacimiento = $("#txt_fecnacimiento").val();
  var fechacontratacion = $("#txt_fecontratacion").val();
  var fechaingreso = $("#txt_fecingreso").val();
  var banco = $("#cbm_banco").val();
  var nrocuentaahorros = $("#txtnrocuentaahorro").val();
  var codigointerbancario = $("#txtinterbancario").val();
  var tipopago = $("#cbm_tipopago").val();
  var esquemapago = $("#cbm_esquemapago").val();
  var ruc = $("#txtruc").val();
  var clavesol1 = $("#txt_clavesol1").val();
  var clavesol2 = $("#txt_clavesol2").val();
  var personaresponsable = $("#cbm_responsable").val();
  var zona = $("#cbm_zona").val();

  if (
    nombre.length == 0 || apepat.length == 0 || apemat.length == 0 ||
    nrodocumento.length == 0 || tipdocumento.length == 0 ||
    sexo.length == 0 || telefono.length == 0 || email.length == 0 ||
    idrol.length == 0 || fechanacimiento.length == 0 ||
    fechacontratacion.length == 0 || fechaingreso.length == 0 ||
    banco.length == 0 || nrocuentaahorros.length == 0 || tipopago.length == 0 ||
    esquemapago.length == 0 || ruc.length == 0 || clavesol1.length == 0 ||
    clavesol2.length == 0 || personaresponsable.length == 0 ||
    zona.length == 0
  ) {
    if (nombre.length == 0) {
      $("#txtnombre").css("border-color", "red");
    } else {
      $("#txtnombre").css("border-color", "none");
    }
    if (apepat.length == 0) {
      $("#txtapepat").css("border-color", "red");
    } else {
      $("#txtapepat").css("border-color", "none");
    }

    if (apemat.length == 0) {
      $("#txtapemat").css("border-color", "red");
    } else {
      $("#txtapemat").css("border-color", "none");
    }

    if (nrodocumento.length == 0) {
      $("#txtnro").css("border-color", "red");
    } else {
      $("#txtnro").css("border-color", "none");
    }
    if (tipdocumento.length == 0) {
      $("#cbm_tdocumento").css("border-color", "red");
    } else {
      $("#cbm_tdocumento").css("border-color", "none");
    }
    if (sexo.length == 0) {
      $("#cbm_sexo").css("border-color", "red");
    } else {
      $("#cbm_sexo").css("border-color", "none");
    }
    if (telefono.length == 0) {
      $("#txttelefono").css("border-color", "red");
    } else {
      $("#txttelefono").css("border-color", "none");
    }
    if (email.length == 0) {
      $("#txt_email").css("border-color", "red");
    } else {
      $("#txt_email").css("border-color", "none");
    }
    if (fechanacimiento.length == 0) {
      $("#txt_fecnacimiento").css("border-color", "red");
    } else {
      $("#txt_fecnacimiento").css("border-color", "none");
    }
    if (fechacontratacion.length == 0) {
      $("#txt_fecontratacion").css("border-color", "red");
    } else {
      $("#txt_fecontratacion").css("border-color", "none");
    }
    if (fechaingreso.length == 0) {
      $("#txt_fecingreso").css("border-color", "red");
    } else {
      $("#txt_fecingreso").css("border-color", "none");
    }

    if (banco.length == 0) {
      $("#cbm_banco").css("border-color", "red");
    } else {
      $("#cbm_banco").css("border-color", "none");
    }

    if (nrocuentaahorros.length == 0) {
      $("#txtnrocuentaahorro").css("border-color", "red");
    } else {
      $("#txtnrocuentaahorro").css("border-color", "none");
    }

    if (codigointerbancario.length == 0) {
      $("#txtinterbancario").css("border-color", "red");
    } else {
      $("#txtinterbancario").css("border-color", "none");
    }

    if (tipopago.length == 0) {
      $("#cbm_tipopago").css("border-color", "red");
    } else {
      $("#cbm_tipopago").css("border-color", "none");
    }

    if (esquemapago.length == 0) {
      $("#cbm_esquemapago").css("border-color", "red");
    } else {
      $("#cbm_esquemapago").css("border-color", "none");
    }
    
    if (ruc.length == 0) {
      $("#txtruc").css("border-color", "red");
    } else {
      $("#txtruc").css("border-color", "none");
    }

    if (clavesol1.length == 0) {
      $("#txt_clavesol1").css("border-color", "red");
    } else {
      $("#txt_clavesol1").css("border-color", "none");
    }

    if (clavesol2.length == 0) {
      $("#txt_clavesol2").css("border-color", "red");
    } else {
      $("#txt_clavesol2").css("border-color", "none");
    }

    if (personaresponsable.length == 0) {
      $("#cbm_responsable").css("border-color", "red");
    } else {
      $("#cbm_responsable").css("border-color", "none");
    }

    if (zona.length == 0) {
      $("#cbm_zona").css("border-color", "red");
    } else {
      $("#cbm_zona").css("border-color", "none");
    }





    return Swal.fire(
      "Mensaje de advertencia",
      "Llene el/lo campos vacios",
      "warning"
    );
  }

  $.ajax({

    url: "../controlador/persona/controlador_username.php",
    type: "POST",
    data: {
      n: nombre,
      apt: apepat,
      apm: apemat,

    },
  }).done(function (resp) {
    if (resp.length == 0) {
      return Swal.fire(
        "Mensaje de advertencia",
        "No se devolvio ningun dato",
        "warning");
    } else {
      console.log(resp);
      var username = resp;
      $.ajax({

        url: "../controlador/persona/controlador_registro_persona.php",
        type: "POST",
        data: {
          n: nombre,
          apt: apepat,
          apm: apemat,
          nro: nrodocumento,
          tip: tipdocumento,
          sex: sexo,
          tel: telefono,
          username: username,
          email: email,
          idrol: idrol,

          fechanacimiento: fechanacimiento,
          fechacontratacion: fechacontratacion,
          fechaingreso: fechaingreso,
          banco: banco,
          nrocuentaahorros: nrocuentaahorros,
          codigointerbancario: codigointerbancario,
          tipopago: tipopago,
          esquemapago: esquemapago,
          ruc: ruc,
          clavesol1: clavesol1,
          clavesol2: clavesol2,
          personaresponsable: personaresponsable,
          zona: zona
        },
      }).done(function (resp) {
        console.log(resp);
        if (isNaN(resp)) {
          document.getElementById("div_error").style.display = "block";
          document.getElementById("div_error").innerHTML =
            "<strong>Revise los siguientes campos:</strong><br>" + resp;
        } else {
          if (resp > 0) {
            document.getElementById("div_error").style.display = "none";
            document.getElementById("div_error").innerHTML =
              "";
            if (resp == 1) {
              LimpiarModal();
              t_persona.ajax.reload();
              $("#modal_registro").modal("hide");
              return Swal.fire(
                "Mensaje de confirmacion",
                "Datos guardados",
                "success"
              );
            } else {
              return Swal.fire(
                "Mensaje de advertencia",
                "El usuario ingresado ya se encuentra en la base de datos",
                "warning"
              );
            }
          } else {
            Swal.fire(
              "Mensaje De Error",
              "Lo sentimos, no se pudo completar el registro",
              "error"
            );
          }
        }
      });

    }
  });
}

function Editar_Persona() {
  

  var idpersona = $("#txtidpersona").val();
  var idpersona_pago = $("#txtidpersona_pago").val();
  var idusuario = $("#txtidusuario").val();
  var nombre = $("#txtnombre_editar").val();
  var apepat = $("#txtapepat_editar").val();
  var apemat = $("#txtapemat_editar").val();
  var nrodocumentoactual = $("#txtnro_editar_actual").val();
  var nrodocumentonuevo = $("#txtnro_editar_nuevo").val();
  var telefono = $("#txttelefono_editar").val();
  var tipdocumento = $("#cbm_tdocumento_editar").val();
  var sexo = $("#cbm_sexo_editar").val();
  var estado = $("#cbm_estatus").val();
  var fecnacimiento = $("#txt_fecnacimiento_editar").val();
  var feccontratacion = $("#txt_fecontratacion_editar").val();
  var fecingreso = $("#txt_fecingreso_editar").val();
  var fecsalida = $("#txt_fecsalida_editar").val();
  var motivosalida = $("#txtmotivosalida_editar").val();
  var fecfulltime = $("#txt_fecfulltime_editar").val();
  var banco = $("#cbm_banco_editar").val();
  var cuentaahorro = $("#txtnrocuentaahorro_editar").val();
  var codigointerban = $("#txtinterbancario_editar").val();
  var tipopago = $("#cbm_tipopago_editar").val();
  var esquemapago = $("#cbm_esquemapago_editar").val();
  var ruc = $("#txtruc_editar").val();
  var clavesol1actual = $("#txt_clavesol1_editar_actual").val();
  var clavesol1nuevo = $("#txt_clavesol1_editar_nuevo").val();
  var clavesol2actual = $("#txt_clavesol2_editar_actual").val();
  var clavesol2nuevo = $("#txt_clavesol2_editar_nuevo").val();
  var emailactual = $("#txt_email_editar_actual").val();
  var emailnuevo = $("#txt_email_editar_nuevo").val();
  var rol = $("#cbm_rol_editar").val();
  var responsable = $("#cbm_responsable_editar").val();
  var zona = $("#cbm_zona_editar").val();

  if (
    nombre.length == 0 || apepat.length == 0 || apemat.length == 0 ||
    nrodocumentonuevo.length == 0 || telefono.length == 0 ||
    tipdocumento.length == 0 || sexo.length == 0 || estado.length == 0 ||
    fecnacimiento.length == 0 || feccontratacion.length == 0 ||
    fecingreso.length == 0 || fecsalida.length == 0 || motivosalida.length == 0 ||
    fecfulltime.length == 0 || banco.length == 0 || cuentaahorro.length == 0 ||
    tipopago.length == 0 || esquemapago.length == 0 ||
    ruc.length == 0 || clavesol1nuevo.length == 0 || clavesol2nuevo.length == 0 ||
    emailnuevo.length == 0 || rol.length == 0 || responsable.length == 0 ||
    zona.length == 0
  ) {
    
    if (nombre.length == 0) {
      $("#txtnombre_editar").css("border-color", "red");
    } else {
      $("#txtnombre_editar").css("border-color", "none");
    }
    if (apepat.length == 0) {
      $("#txtapepat_editar").css("border-color", "red");
    } else {
      $("#txtapepat_editar").css("border-color", "#ced4da");
    }
    if (apemat.length == 0) {
      $("#txtapemat_editar").css("border-color", "red");
    } else {
      $("#txtapemat_editar").css("border-color", "#ced4da");
    }
    if (nrodocumentonuevo.length == 0) {
      $("#txtnro_editar_nuevo").css("border-color", "red");
    } else {
      $("#txtnro_editar_nuevo").css("border-color", "#ced4da");
    }
    if (telefono.length == 0) {
      $("#txttelefono_editar").css("border-color", "red");
    } else {
      $("#txttelefono_editar").css("border-color", "#ced4da");
    }
    if (tipdocumento.length == 0) {
      $("#cbm_tdocumento_editar").css("border-color", "red");
    } else {
      $("#cbm_tdocumento_editar").css("border-color", "#ced4da");
    }
    if (sexo.length == 0) {
      $("#cbm_sexo_editar").css("border-color", "red");
    } else {
      $("#cbm_sexo_editar").css("border-color", "#ced4da");
    }
    if (estado.length == 0) {
      $("#cbm_estatus").css("border-color", "red");
    } else {
      $("#cbm_estatus").css("border-color", "#ced4da");
    }
    if (fecnacimiento.length == 0) {
      $("#txt_fecnacimiento_editar").css("border-color", "red");
    } else {
      $("#txt_fecnacimiento_editar").css("border-color", "#ced4da");
    }
    if (feccontratacion.length == 0) {
      $("#txt_fecontratacion_editar").css("border-color", "red");
    } else {
      $("#txt_fecontratacion_editar").css("border-color", "#ced4da");
    }
    if (fecingreso.length == 0) {
      $("#txt_fecingreso_editar").css("border-color", "red");
    } else {
      $("#txt_fecingreso_editar").css("border-color", "#ced4da");
    }
    
    
    
    if (banco.length == 0) {
      $("#txtbanco_editar").css("border-color", "red");
    } else {
      $("#txtbanco_editar").css("border-color", "#ced4da");
    }
    if (cuentaahorro.length == 0) {
      $("#txtcuenta_editar").css("border-color", "red");
    } else {
      $("#txtcuenta_editar").css("border-color", "#ced4da");
    }
    if (tipopago.length == 0) {
      $("#cbm_tipopago_editar").css("border-color", "red");
    } else {
      $("#cbm_tipopago_editar").css("border-color", "#ced4da");
    }
    if (esquemapago.length == 0) {
      $("#cbm_esquemapago_editar").css("border-color", "red");
    } else {
      $("#cbm_esquemapago_editar").css("border-color", "#ced4da");
    }
    if (ruc.length == 0) {
      $("#txtruc_editar").css("border-color", "red");
    } else {
      $("#txtruc_editar").css("border-color", "#ced4da");
    }
    if (clavesol1nuevo.length == 0) {
      $("#txtclavesol1_editar").css("border-color", "red");
    } else {
      $("#txtclavesol1_editar").css("border-color", "#ced4da");
    }
    if (clavesol2nuevo.length == 0) {
      $("#txtclavesol2_editar").css("border-color", "red");
    } else {
      $("#txtclavesol2_editar").css("border-color", "#ced4da");
    }
    if (emailnuevo.length == 0) {
      $("#txtemail_editar").css("border-color", "red");
    } else {
      $("#txtemail_editar").css("border-color", "#ced4da");
    }
    if(rol.length == 0){
      $("#cbm_rol_editar").css("border-color", "red");
    }else{
      $("#cbm_rol_editar").css("border-color", "#ced4da");
    }
    if(responsable.length == 0){
      $("#cbm_responsable_editar").css("border-color", "red");
    }else{
      $("#cbm_responsable_editar").css("border-color", "#ced4da");
    }
    if(zona.length == 0){
      $("#cbm_zona_editar").css("border-color", "red");
    }else{
      $("#cbm_zona_editar").css("border-color", "#ced4da");
    }


    
    return Swal.fire(
      "Mensaje de advertencia",
      "Llene el campo vacio",
      "warning"
    );
  }

  $.ajax({
    url: "../controlador/persona/controlador_editar_persona.php",
    type: "POST",
    data: {
      idpersona: idpersona,
      idpersona_pago: idpersona_pago,
      idusuario: idusuario,
      nombre: nombre,
      apepat: apepat,
      apemat: apemat,
      nrodocumentoactual: nrodocumentoactual,
      nrodocumentonuevo: nrodocumentonuevo,
      telefono: telefono,
      tipdocumento: tipdocumento,
      sexo: sexo,
      estado: estado,
      fecnacimiento: fecnacimiento,
      feccontratacion: feccontratacion,
      fecingreso: fecingreso,
      fecsalida: fecsalida,
      motivosalida: motivosalida,
      fecfulltime: fecfulltime,
      banco: banco,
      cuentaahorro: cuentaahorro,
      codigointerban: codigointerban,
      tipopago: tipopago,
      esquemapago: esquemapago,
      ruc: ruc,
      clavesol1actual: clavesol1actual,
      clavesol1nuevo: clavesol1nuevo,
      clavesol2actual: clavesol2actual,
      clavesol2nuevo: clavesol2nuevo,
      emailactual: emailactual,
      emailnuevo: emailnuevo,
      rol: rol,
      responsable: responsable,
      zona: zona

    },
  }).done(function (resp) {
    // if (isNaN(resp)) {
    //   document.getElementById("div_error_editar").style.display = "block";
    //   document.getElementById("div_error_editar").innerHTML =
    //     "<strong>Revise los siguientes campos:</strong><br>" + resp;
    // } else {
      if (resp > 0) {
        // document.getElementById("div_error_editar").style.display = "none";
        // document.getElementById("div_error_editar").innerHTML =
        //   "";
        if (resp == 1) {
          t_persona.ajax.reload();
          $("#modal_editar").modal("hide");
          return Swal.fire(
            {
              icon: `success`,
              title: `Mensaje de confirmacion`,
              text: `Datos guardados`
            }
          );
        } else {
          return Swal.fire(
            "Mensaje de advertencia",
            "El usuario ingresado ya se encuentra en la base de datos",
            "warning"
          );
        }
      } else {
        Swal.fire(
          "Mensaje De Error",
          "Lo sentimos, no se pudo completar la actualizacion",
          "error"
        );
      }
    
  });
}

function AbrirModal() {
  $("#modal_registro").modal({ backdrop: "static", keyboard: false });
  $("#modal_registro").find(".modal-header").css("background", "#854c35");
  $("#modal_registro").find(".modal-header").css("color", "white");
  $("#modal_registro").modal("show");
  document.getElementById("div_error").style.display = "none";
  LimpiarModal();
}

function MensajeError(
  nombre,
  apepat,
  apemat,
  nrodocumento,
  tipdocumento,
  sexo,
  telefono,
  id
) {
  var cadena = "";
  if (nombre.length == 0) {
    cadena += "El campo nombre no debe estar vacio.<br>";
  }
  if (apepat.length == 0) {
    cadena += "El campo apellido paterno no debe estar vacio.<br>";
  }
  if (apemat.length == 0) {
    cadena += "El campo apellido materno no debe estar vacio.<br>";
  }
  if (nrodocumento.length == 0) {
    cadena += "El campo n&uacute;mero de documento no debe estar vacio.<br>";
  }
  if (tipdocumento.length == 0) {
    cadena += "El campo tipo documento no debe estar vacio.<br>";
  }
  if (sexo.length == 0) {
    cadena += "El campo sexo no debe estar vacio.<br>";
  }
  if (telefono.length == 0) {
    cadena += "El campo telefono no debe estar vacio.<br>";
  }

  document.getElementById(id).style.display = "block";
  document.getElementById(id).innerHTML =
    "<strong>Revise los siguientes campos:</strong><br>" + cadena;
}


function LimpiarModal() {

  $("#txtnombre").val("");
  $("#txtapepat").val("");
  $("#txtapemat").val("");
  $("#txtnro").val("");
  $("#txttelefono").val("");
}

function LimpiarModalEditarContra() {
  $("#txt_contra_actual").val("");
  $("#txt_contra_nueva").val("");
  $("#txt_repetir_contra").val("");
  
}


function Listar_rol_combo() {
  $.ajax({
    url: "../controlador/usuario/controlador_rol_combo_listar.php",
    type: 'POST'
  }).done(function (resp) {
    var data = JSON.parse(resp);
    var cadena = "<option value='0' selected='selected'><span>Elegir una opcion</span></option>";
    if (data.length > 0) {
      for (var index = 0; index < data.length; index++) {
        cadena += "<option value='" + data[index][0] + "'>" + data[index][1] + "</option>";

      }
      $("#cbm_rol").html(cadena);
      $("#cbm_rol_editar").html(cadena);
    } else {
      cadena += "<option value=''>No se encontraron datos</option>";
      $("#cbm_rol").html(cadena);
      $("#cbm_rol_editar").html(cadena);
    }
  })
}

function Listar_personaCargo_combo() {
  $.ajax({
    url: "../controlador/persona/controlador_persona_cargo_listar.php",
    type: 'POST'
  }).done(function (resp) {
    var data = JSON.parse(resp);
    var cadena = "<option value='0' selected='selected'><span>Elegir una opcion</span></option>";
    if (data.length > 0) {
      for (var index = 0; index < data.length; index++) {
        cadena += "<option value='" + data[index][0] + "'>" + data[index][1] + "</option>";

      }
      $("#cbm_responsable").html(cadena);
      // $("#cbm_rol_editar").html(cadena);
    } else {
      cadena += "<option value=''>No se encontraron datos</option>";
      $("#cbm_responsable").html(cadena);
      // $("#cbm_rol_editar").html(cadena);
    }
  })
}