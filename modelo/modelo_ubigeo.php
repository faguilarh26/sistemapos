<?php
class Modelo_Ubigeo
{
    private $conexion;
    function __construct()
    {
        require_once 'modelo_conexion.php';
        $this->conexion = new conexion();
        $this->conexion->conectar();
    }

    function Listar_Venta($rol, $idusuario,$fecha_inicio,$fecha_fin)
    {
        $sql = "call SP_LISTAR_VENTA('$rol','$idusuario','$fecha_inicio','$fecha_fin')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function Listar_Venta_Rechazadas($rol, $idusuario,$fecha_inicio,$fecha_fin)
    {
        $sql = "call SP_LISTAR_VENTA_RECHAZADA('$rol','$idusuario','$fecha_inicio','$fecha_fin')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function Editar_venta_general($id, $va, $sotactual, $sotnuevo, $setactual, $setnuevo, $comentario_venta_general, $estatus)
    {
        $sql = "call SP_EDITAR_VENTA_GENERAL('$id','$va','$sotactual','$sotnuevo','$setactual','$setnuevo','$comentario_venta_general','$estatus')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function listar_combo_producto()
    {
        $sql = "call SP_LISTAR_COMBO_PRODUCTO";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }



    function listar_combo_departamento()
    {
        $sql = "call SP_LISTAR_COMBO_DEPARTAMENTO";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function listar_solo_provincia()
    {
        $sql = "call SP_LISTAR_SOLO_PROVINCIA";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function listar_solo_distrito()
    {
        $sql = "call SP_LISTAR_SOLO_DISTRITO()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_provincia($iddepartamento)
    {
        $sql = "call SP_LISTAR_COMBO_PROVINCIA('$iddepartamento')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_distrito($idprovincia)
    {
        $sql = "call SP_LISTAR_COMBO_DISTRITO('$idprovincia')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function Registrar_Venta_Sin_Pago($movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumento,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$idusuario,$fullclaro,$pregunta,$bancamovil,$promocion,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal)
    {


        $sql = "call SP_REGISTRAR_VENTA_SIN_PAGO('$movilllamada','$fechallamada','$horallamada','$tipodocumento','$nrodocumento','$nombre','$fijoportar','$prinompapa','$prinommama','$fechanacimiento','$signosodiacal','$distritonac','$via','$distritoact','$referencia','$email','$moviltitular','$movilcontacto','$movilcoordinacion','$plano','$producto','$plan','$cargofijo','$costoinstalacion','$idusuario','$fullclaro','$pregunta','$bancamovil','$promocion','$tipoventa','$play','$paquete','$decoadd1','$premium','$cargoadd','$cargototal')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Editar_Venta_Sin_Pago($idusuario,$estado, $idventa, $idcliente, $iddireccion, $iddetallepre, $idgrabacion, $movilllamada, $fechallamada, $horallamada, $tipodocumento, $nrodocumentoactual, $nrodocumentonuevo, $nombre, $fijoportar, $prinompapa, $prinommama, $fechanacimiento, $signosodiacal, $distritonac, $via, $distritoact, $referencia, $email, $moviltitular, $movilcontacto, $movilcoordinacion, $plano, $producto, $plan, $cargofijo, $costoinstalacion, $fullclaro, $pregunta, $bancamovil, $promocion, $comentario, $rol,$va,$sot,$set,$comentario_estado,$fecha_instalacion,$hor_iniciopro,$hor_fin_pro,$comentario_estado_venta)
    {


        $sql = "call SP_EDITAR_VENTA_SIN_PAGO('$idusuario','$estado','$idventa','$idcliente','$iddireccion','$iddetallepre','$idgrabacion','$movilllamada','$fechallamada','$horallamada','$tipodocumento','$nrodocumentoactual','$nrodocumentonuevo','$nombre','$fijoportar','$prinompapa','$prinommama','$fechanacimiento','$signosodiacal','$distritonac','$via','$distritoact','$referencia','$email','$moviltitular','$movilcontacto','$movilcoordinacion','$plano','$producto','$plan','$cargofijo','$costoinstalacion','$fullclaro','$pregunta','$bancamovil','$promocion','$comentario','$rol','$va','$sot','$set','$comentario_estado','$fecha_instalacion','$hor_iniciopro','$hor_fin_pro','$comentario_estado_venta')";
        
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }



    function Registrar_Venta_Con_Pago($movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumento,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$idusuario,$fullclaro,$pregunta,$bancamovil,$diapago,$horapago,$promocion,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal)
    {

        $sql = "call SP_REGISTRAR_VENTA_CON_PAGO('$movilllamada','$fechallamada','$horallamada','$tipodocumento','$nrodocumento','$nombre','$fijoportar','$prinompapa','$prinommama','$fechanacimiento','$signosodiacal','$distritonac','$via','$distritoact','$referencia','$email','$moviltitular','$movilcontacto','$movilcoordinacion','$plano','$producto','$plan','$cargofijo','$costoinstalacion','$idusuario','$fullclaro','$pregunta','$bancamovil','$diapago','$horapago','$promocion','$tipoventa','$play','$paquete','$decoadd1','$premium','$cargoadd','$cargototal')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Editar_Venta_Con_Pago($idusuario,$estado,$idventa,$idcliente,$iddireccion,$iddetallepre,$idgrabacion,$movilllamada,$fechallamada,$horallamada,$tipodocumento,$nrodocumentoactual,$nrodocumentonuevo,$nombre,$fijoportar,$prinompapa,$prinommama,$fechanacimiento,$signosodiacal,$distritonac,$via,$distritoact,$referencia,$email,$moviltitular,$movilcontacto,$movilcoordinacion,$plano,$producto,$plan,$cargofijo,$costoinstalacion,$fullclaro,$pregunta,$bancamovil,$diapago,$horapago,$promocion,$comentario,$rol,$va,$sot,$set,$comentario_estado,$fecha_instalacion,$hor_iniciopro,$hor_fin_pro,$comentario_estado_venta,$tipoventa,$play,$paquete,$decoadd1,$premium,$cargoadd,$cargototal)
    {

        $sql = "call SP_EDITAR_VENTA_CON_PAGO('$idusuario','$estado','$idventa','$idcliente','$iddireccion','$iddetallepre','$idgrabacion','$movilllamada','$fechallamada','$horallamada','$tipodocumento','$nrodocumentoactual','$nrodocumentonuevo','$nombre','$fijoportar','$prinompapa','$prinommama','$fechanacimiento','$signosodiacal','$distritonac','$via','$distritoact','$referencia','$email','$moviltitular','$movilcontacto','$movilcoordinacion','$plano','$producto','$plan','$cargofijo','$costoinstalacion','$fullclaro','$pregunta','$bancamovil','$diapago','$horapago','$promocion','$comentario','$rol','$va','$sot','$set','$comentario_estado','$fecha_instalacion','$hor_iniciopro','$hor_fin_pro','$comentario_estado_venta','$tipoventa','$play','$paquete','$decoadd1','$premium','$cargoadd','$cargototal')";

        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
}
