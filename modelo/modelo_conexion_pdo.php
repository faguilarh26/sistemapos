<?php


function deletePermisos($intIdrol)
{
	$conn = new PDO('mysql:host=localhost;dbname=sistemapos', "root", "");
	// $sql = "call SP_ELIMINAR_PERMISOS('$intIdrol')";
	// if ($consulta = $this->conexion->conexion->query($sql)) {

	//     if ($row = mysqli_fetch_array($consulta)) {
	//         return $respuesta = trim($row[0]);
	//     }

	// 	$this->conexion->cerrar();
	// }

	$sql = "call SP_ELIMINAR_PERMISOS('$intIdrol')";


	$result = $conn->prepare($sql);
	$del = $result->execute();



	return $del;
	$sql = null;
	$result = null;
	$del = null;
}

function insertPermisos(int $intIdrol, int $idModulo, int $r, int $w, int $u, int $d)
{
	$conn = new PDO('mysql:host=localhost;dbname=sistemapos', "root", "");
	// $this->intRolid = $idrol;
	// $this->intModuloid = $idmodulo;
	// $this->r = $r;
	// $this->w = $w;
	// $this->u = $u;
	// $this->d = $d;
	// $sql = "call SP_INSERTAR_PERMISOS('$intIdrol','$idpermiso','$idModulo','$r','$w','$u','$d')";
	// if ($consulta = $this->conexion->conexion->query($sql)) {

	//     if ($row = mysqli_fetch_array($consulta)) {
	//         return $respuesta = trim($row[0]);
	//     }

	// 	$this->conexion->cerrar();
	// }
	// $query_insert  = "INSERT INTO permisos(rolid,moduloid,r,w,u,d) VALUES(?,?,?,?,?,?)";
	$query_insert  = "call SP_INSERTAR_PERMISOS('$intIdrol','$idModulo','$r','$w','$u','$d')";
	$arrData = array($intIdrol, $idModulo, $r, $w, $u, $d);



	$insert = $conn->prepare($query_insert);
	$resInsert = $insert->execute();
	if ($resInsert) {
		$lastInsert = $conn->lastInsertId();
	} else {
		$lastInsert = 0;
	}
	return $lastInsert;

	$query_insert = null;
	$arrData = null;
	$insert = null;
	$resInsert = null;
	$lastInsert = null;
}


function getPermisos(int $idmodulo)
{
	$conn = new PDO('mysql:host=localhost;dbname=sistemapos', "root", "");

	$idrol = $_SESSION['S_IDUSUARIO'];
	$sql = "call SP_VERIFICAR_PERMISOS_MODULO('$idrol')";
	$result = $conn->prepare($sql);
	$result->execute();
	$data = $result->fetchall(PDO::FETCH_ASSOC);
	$arrPermisos = array();
	for ($i = 0; $i < count($data); $i++) {
		$arrPermisos[$data[$i]['moduloid']] = $data[$i];
	}
	$permisos = '';
	$permisosMod = '';
	if (count($arrPermisos) > 0) {
		$permisos = $arrPermisos;
		$permisosMod = isset($arrPermisos[$idmodulo]) ? $arrPermisos[$idmodulo] : "";
	}
	// $_SESSION['permisos'] = $permisos;
	$_SESSION['permisosMod'] = $permisosMod;
}
function getPermisosModuloGeneral()
{
	$conn = new PDO('mysql:host=localhost;dbname=sistemapos', "root", "");

	$idrol = $_SESSION['S_IDUSUARIO'];
	$sql = "call SP_VERIFICAR_PERMISOS_MODULO('$idrol')";
	$result = $conn->prepare($sql);
	$result->execute();
	$data = $result->fetchall(PDO::FETCH_ASSOC);
	$arrPermisos = array();
	for ($i = 0; $i < count($data); $i++) {
		$arrPermisos[$data[$i]['moduloid']] = $data[$i];
	}
	$permisos = '';
	$permisosMod = '';
	if (count($arrPermisos) > 0) {
		$permisos = $arrPermisos;
		// $permisosMod = isset($arrPermisos[$idmodulo]) ? $arrPermisos[$idmodulo] : "";
	}
	$_SESSION['permisos'] = $permisos;
	// $_SESSION['permisosMod'] = $permisosMod;
}
