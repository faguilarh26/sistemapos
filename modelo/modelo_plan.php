<?php
class Modelo_Plan
{
    private $conexion;
    function __construct()
    {
        require_once 'modelo_conexion.php';
        $this->conexion = new conexion();
        $this->conexion->conectar();
    }
    function Listar_play()
    {
        $sql = "call SP_LISTAR_PLAY()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function Listar_plan_internet()
    {
        $sql = "call SP_LISTAR_PLAN_INTERNET()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function Listar_plan_paquetes($play, $plan, $producto)
    {
        $sql = "call SP_LISTAR_PLAN_PAQUETES('$play','$plan','$producto')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function Listar_decoadd1()
    {
        $sql = "call SP_LISTAR_DECOADD1()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function Registrar_Decoadd1($deco, $precio)
    {
        $sql = "call SP_REGISTRAR_DECOADD1('$deco','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Modificar_Deco1($id, $deco1actual, $deco1nuevo, $precio)
    {
        $sql = "call SP_EDITAR_DECO1('$id','$deco1actual','$deco1nuevo','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Eliminar_Deco1($id)
    {
        $sql = "call SP_ELIMINAR_DECO1('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {
            return 1;
        } else {
            return 0;
        }
        $this->conexion->cerrar();
    }

    function Listar_decoadd2()
    {
        $sql = "call SP_LISTAR_DECOADD2()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function Registrar_Decoadd2($deco, $precio)
    {
        $sql = "call SP_REGISTRAR_DECOADD2('$deco','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Modificar_Deco2($id, $deco2actual, $deco2nuevo, $precio)
    {
        $sql = "call SP_EDITAR_DECO2('$id','$deco2actual','$deco2nuevo','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }


    function Listar_premium()
    {
        $sql = "call SP_LISTAR_PREMIUM()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_assoc($consulta)) {

                $arreglo["data"][] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }
    function Registrar_Premium($paqpremium, $precio)
    {
        $sql = "call SP_REGISTRAR_PREMIUM('$paqpremium','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Modificar_Premium($id, $premiumactual, $premiumnuevo, $precio)
    {
        $sql = "call SP_EDITAR_PREMIUM('$id','$premiumactual','$premiumnuevo','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Eliminar_Premium($id)
    {
        $sql = "call SP_ELIMINAR_PREMIUM('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {
            return 1;
        } else {
            return 0;
        }
        $this->conexion->cerrar();
    }




    function Registrar_Play($play)
    {
        $sql = "call SP_REGISTRAR_PLAY('$play')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Modificar_Play($id, $playactual, $playnuevo, $estatus)
    {
        $sql = "call SP_EDITAR_PLAY('$id','$playactual','$playnuevo','$estatus')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Eliminar_Play($id)
    {
        $sql = "call SP_ELIMINAR_PLAY('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {
            return 1;
        } else {
            return 0;
        }
        $this->conexion->cerrar();
    }


    function Registrar_Plan_Internet($plan)
    {
        $sql = "call SP_REGISTRAR_PLAN_INTERNET('$plan')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Modificar_Plan_Inter($id, $planinteractual, $planinternuevo, $estatus)
    {
        $sql = "call SP_EDITAR_PLAN_INTER('$id','$planinteractual','$planinternuevo','$estatus')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Eliminar_Plan_internet($id)
    {
        $sql = "call SP_ELIMINAR_PLAN_INTERNET('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {
            return 1;
        } else {
            return 0;
        }
        $this->conexion->cerrar();
    }

    function Registar_Paquete($producto,$play,$plan,$paquete,$precio,$productonombre,$playnombre,$plannombre)
    {
        $sql = "call SP_REGISTRAR_PAQUETE('$producto','$play','$plan','$paquete','$precio','$productonombre','$playnombre','$plannombre')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Modificar_Paquete($id, $producto, $play, $plan, $paqueteactual, $paquetenuevo, $precio)
    {
        $sql = "call SP_EDITAR_PAQUETE('$id','$producto','$play','$plan','$paqueteactual','$paquetenuevo','$precio')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Eliminar_Paquete_internet($id)
    {
        $sql = "call SP_ELIMINAR_PAQUETE_INTERNET('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {
            return 1;
        } else {
            return 0;
        }
        $this->conexion->cerrar();
    }


    function Modificar_Producto($id, $productoactual, $productonuevo, $estatus)
    {
        $sql = "call SP_EDITAR_PRODUCTO('$id','$productoactual','$productonuevo','$estatus')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function listar_combo_play()
    {
        $sql = "call SP_LISTAR_COMBO_PLAY";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_plan_internet()
    {
        $sql = "call SP_LISTAR_COMBO_PLAN_INTERNET";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }


    function listar_combo_paquete($idproducto, $idplay, $idplan)
    {
        $sql = "call SP_LISTAR_COMBO_PAQUETE('$idproducto','$idplay','$idplan')";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_paquete_general()
    {
        $sql = "call SP_LISTAR_COMBO_PAQUETE_GENERAL()";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_decoadd1()
    {
        $sql = "call SP_LISTAR_COMBO_DECOADD1";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_decoadd2()
    {
        $sql = "call SP_LISTAR_COMBO_DECOADD2";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }

    function listar_combo_premium()
    {
        $sql = "call SP_LISTAR_COMBO_PREMIUM";
        $arreglo = array();
        if ($consulta = $this->conexion->conexion->query($sql)) {
            while ($consulta_VU = mysqli_fetch_array($consulta)) {
                $arreglo[] = $consulta_VU;
            }
            return $arreglo;
            $this->conexion->cerrar();
        }
    }



    function Traer_Precio_Fijo($id)
    {
        $sql = "call SP_TRAER_PRECIO_FIJO('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }

    function Obtener_preciodeco1($id)
    {
        $sql = "call SP_OBTENER_PRECIO_DECO1('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Obtener_preciodeco2($id)
    {
        $sql = "call SP_OBTENER_PRECIO_DECO2('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
    function Obtener_premium($id)
    {
        $sql = "call SP_OBTENER_PRECIO_PREMIUM('$id')";
        if ($consulta = $this->conexion->conexion->query($sql)) {

            if ($row = mysqli_fetch_array($consulta)) {
                return $respuesta = trim($row[0]);
            }

            $this->conexion->cerrar();
        }
    }
}
