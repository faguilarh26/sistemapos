<?php
    class Modelo_Persona{
        private $conexion;
        function __construct()
        {
            require_once 'modelo_conexion.php';
            $this->conexion = new conexion();
            $this->conexion->conectar();
        }
        function Listar_Persona(){
            $sql = "call SP_LISTAR_PERSONA()";
            $arreglo = array();
			if ($consulta = $this->conexion->conexion->query($sql)) {
				while ($consulta_VU = mysqli_fetch_assoc($consulta)) {
					
                        $arreglo["data"][] = $consulta_VU;
                    
				}
				return $arreglo;
				$this->conexion->cerrar();
			}
        }

        function Listar_Persona_responsable(){
                $sql = "call SP_LISTAR_PERSONA_RESPONSABLE()";
                $arreglo = array();
        
                if ($consulta = $this->conexion->conexion->query($sql)) {
                    while ($consulta_VU = mysqli_fetch_array($consulta))
                            $arreglo[] = $consulta_VU;
                        
                    }
                    return $arreglo;
        
                    $this->conexion->cerrar() ;
            }

        

        function Registrar_Persona($nombre,$apepat,$apemat,$nrodocumento,$tipdocumento,$sexo,$telefono,$username,$email,$idrol,$fechanacimiento,$fechacontratacion,$fechaingreso,$banco,$nrocuentaahorros,$codigointerbancario,$tipopago,$esquemapago,$ruc,$clavesol1,$clavesol2,$personaresponsable,$zona){
            $sql = "call SP_REGISTRAR_PERSONA('$nombre','$apepat','$apemat','$nrodocumento','$tipdocumento','$sexo','$telefono','$username','$email','$idrol','$fechanacimiento','$fechacontratacion','$fechaingreso','$banco','$nrocuentaahorros','$codigointerbancario','$tipopago','$esquemapago','$ruc','$clavesol1','$clavesol2','$personaresponsable','$zona')";
			if ($consulta = $this->conexion->conexion->query($sql)) {
				
                if ($row = mysqli_fetch_array($consulta)) {
                    return $respuesta = trim($row[0]);
                }
				
				$this->conexion->cerrar();
			}
        }

        function Editar_Persona($idpersona,$idpersona_pago,$idusuario,$nombre,$apepat,$apemat,$nrodocumentoactual,$nrodocumentonuevo,$telefono,$tipdocumento,$sexo,$estado,$fecnacimiento,$feccontratacion,$fecingreso,$fecsalida,$motivosalida,$fecfulltime,$banco,$cuentaahorro,$codigointerban,$tipopago,$esquemapago,$ruc,$clavesol1actual,$clavesol1nuevo,$clavesol2actual,$clavesol2nuevo,$emailactual,$emailnuevo,$rol,$responsable,$zona){
            $sql = "call SP_EDITAR_PERSONA('$idpersona','$idpersona_pago','$idusuario','$nombre','$apepat','$apemat','$nrodocumentoactual','$nrodocumentonuevo','$telefono','$tipdocumento','$sexo','$estado','$fecnacimiento','$feccontratacion','$fecingreso','$fecsalida','$motivosalida','$fecfulltime','$banco','$cuentaahorro','$codigointerban','$tipopago','$esquemapago','$ruc','$clavesol1actual','$clavesol1nuevo','$clavesol2actual','$clavesol2nuevo','$emailactual','$emailnuevo','$rol','$responsable','$zona')";
			if ($consulta = $this->conexion->conexion->query($sql)) {
				
                if ($row = mysqli_fetch_array($consulta)) {
                    return $respuesta = trim($row[0]);
                }
				
				$this->conexion->cerrar();
			}
        }


        function Editar_contra($idusuario,$contranueva){
            $sql = "call SP_EDITAR_CONTRA_USUARIO('$idusuario','$contranueva')";
			if ($consulta = $this->conexion->conexion->query($sql)) {
                return 1;
            }else{
                return 0;
            }
				
                
				
				$this->conexion->cerrar();
        }

        function Eliminar_Persona($id){
            $sql = "call SP_ELIMINAR_PERSONA('$id')";
			if ($consulta = $this->conexion->conexion->query($sql)) {
                return 1;
            }else{
                return 0;
            }
				
                
				
				$this->conexion->cerrar();
			
        }

        function Restablecer_contra_default($id){
            $sql = "call SP_RESTABLECER_CONTRA_DEFAULT('$id')";
			if ($consulta = $this->conexion->conexion->query($sql)) {
                return 1;
            }else{
                return 0;
            }
				
                
				
				$this->conexion->cerrar();
			
        }
        
    }
